(defproject budi "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/clojurescript "1.7.122"]
                 [com.taoensso/timbre "4.7.4"]
                 [org.clojure/tools.cli "0.3.3"]
                 [funcool/promesa "0.7.0"]
                 [metrics-clojure "2.5.1"]
                 [metrics-clojure-health "2.5.1"]
                 [metrics-clojure-ring "2.5.1"]
                 [cheshire "5.4.0"]
                 [ring/ring-core "1.3.2"
                  :exclusions [[org.clojure/tools.reader :extension "jar"]]]
                 [ring/ring-jetty-adapter "1.3.2"]
                 [metosin/ring-swagger "0.20.4"]
                 [metosin/ring-swagger-ui "2.1.5-M2"]
                 [metosin/compojure-api "0.21.1-SNAPSHOT"]
                 [metosin/schema-tools "0.4.2"]
                 [prismatic/schema "1.0.1"]
                 [com.cemerick/url "0.1.1"]
                 [gardendb "0.2.0"]
                 [com.stuartsierra/component "0.2.2"]
                 [slingshot "0.12.2"]
                 [org.clojure/core.cache "0.6.4"]
                 [org.clojure/core.memoize "0.5.6"]
                 [ring/ring-json "0.3.1"]
                 [clj-http "0.9.2"]
                 [figwheel "0.4.0"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [jayq "2.5.2"]
                 [om "0.8.0-rc1"]
                 [om-sync "0.1.1"]
                 [org.apache.httpcomponents/httpcore "4.3.2"]
                 [clj-time "0.11.0"]
                 [com.andrewmcveigh/cljs-time "0.4.0"]
                 [cljs-ajax "0.3.13"]
                 [cljs-hash "0.0.2"]]

  :jvm-opts ^:replace ["-Xmx1g" "-server"]

  :test2junit-output-dir "../shippable/testresults"
  :plugins [[lein-ring "0.8.13"]
            [lein-environ "1.0.0"]
            [com.palletops/uberimage "0.4.1"]
            [lein-cljsbuild "1.0.6"]]

  :resource-paths ["resources"]
  :test-paths ["test/clj" "test/resources"]
  :source-paths ["src/clj"]
  :target-path "target/%s/"
  :cljs-build-path "resources/swagger-ui/js/generated"

  :clean-targets ^{:protect false} [:target-path :cljs-build-path]

  :test-selectors {:default (complement :backend-test)
                   :backend-test :backend-test
                   :ui-test :ui-test
                   :all (constantly true)}

  :aliases {"test-ui" ["do" ["clean"] ["cljsbuild" "once" "prod"] ["midje" "budi.remote-tests.ui-tests.*"]]
            "test-unit" ["do" ["midje" "budi.unit-tests.*"]]
            "test-api" ["do" ["midje" "budi.remote-tests.api-tests.*"]]
            "remote-test-backend" ["with-profile" "dev,remote-tests" "test-backend"]
            "release" ["do" ["test-unit" "test-api" "uberimage"]]
            "server" ["do" ["clean"] ["cljsbuild" "once" "prod"] ["run" "-m" "budi.main"]]
            "server-light" ["do" ["run" "-m" "budi.main"]]}

  :main budi.main
  :profiles {:uberjar {:env {:production true}
                       :aot [budi.main]
                       :hooks [leiningen.cljsbuild]
                       :omit-source true}
             :dev {:dependencies [[clj-webdriver/clj-webdriver "0.6.1"]
                                  [midje "1.8.3"]
                                  [environ "1.0.0"]
                                  [org.apache.httpcomponents/httpclient "4.3.6"]
                                  [xml-apis/xml-apis "1.4.01"]]
                   :ring {:handler budi.handler/app}
                   :source-paths ["dev"]
                   :plugins [[com.cemerick/austin "0.1.6"]
                             [lein-midje "3.1.3"]
                             [lein-figwheel "0.4.0"]]}
             :remote-tests {:env {:conf {:execution :remote
                                         :db {:hosts ["192.168.59.103"]
                                              :type :sqlite}
                                         :port 8080
                                         :host "192.168.59.103"}}}}
  :uberjar-name "budi-main.jar"

  :uberimage {:instructions ["RUN apt-get update && apt-get -y dist-upgrade"]
              :files {"/etc/budi/budi.edn" "resources/external-budi.edn"}
              :tag "thrawn/budi:latest"
              :base-image "java:7"}

  ;; Figwheel settings. We are going to use 'resource/swagger-ui' directory as our root
  ;; directory for serving the files by the figwheel server, and also we specify
  ;; where our CSS styles live, so Figwheel could reload them without reloading the
  ;; page when we change them as well!
  :figwheel {:http-server-root "swagger-ui"
             :port 3449
             :css-dirs ["resources/swagger-ui/css"]}

:cljsbuild {:builds {:dev {:id "budi-dev"
                           :source-paths ["src/cljs"
                                          "dev/brepl"]
                           :figwheel true
                           ;; Google Closure Compiler options
                           :compiler {;; the name of emitted JS script file
                                      :output-to "resources/swagger-ui/js/generated/budi.js"
                                      :output-dir "resources/swagger-ui/js/generated/out"
                                      :optimizations :none

                                      :externs ["swagger-ui/libs/react.0.12.2.js"
                                                "swagger-ui/libs/jquery-2.1.4.js"
                                                "swagger-ui/libs/bootstrap.js"
                                                "swagger-ui/libs/bootstrapValidator.js"
                                                "swagger-ui/libs/jquery.multilevelpushmenu.js"
                                                "swagger-ui/libs/Chart.js"
                                                "swagger-ui/libs/bootbox.js"
                                                "swagger-ui/libs/isotope.pkgd.js"
                                                "swagger-ui/libs/packery-mode.pkgd.js"
                                                "swagger-ui/libs/later.js"
                                                "externs/menu.js"
                                                "externs/charts.js"
                                                "externs/bootbox.js"
                                                "externs/bootstrap-validator.js"]
                                      }}
                     :prod {:id "budi-prod"
                            :source-paths ["src/cljs/budi/charts"
                                           "src/cljs/budi/schemas"
                                           "src/cljs/budi/dal"
                                           "src/cljs/budi/common"
                                           "src/cljs/budi/provider"
                                           "src/cljs/budi/view"]

                            ;; Google Closure Compiler options
                            :compiler {;; the name of emitted JS script file
                                       :output-to "resources/swagger-ui/js/generated/budi.min.js"
                                       :preamble ["swagger-ui/libs/react.0.12.2.min.js"
                                                  "swagger-ui/libs/jquery-2.1.4.min.js"
                                                  "swagger-ui/libs/bootstrap.min.js"
                                                  "swagger-ui/libs/isotope.pkgd.min.js"
                                                  "swagger-ui/libs/packery-mode.pkgd.min.js"
                                                  "swagger-ui/libs/Chart.min.js"
                                                  "swagger-ui/libs/bootstrapValidator.min.js"
                                                  "swagger-ui/libs/register.js"
                                                  "swagger-ui/libs/bootbox.min.js"
                                                  "swagger-ui/libs/later.min.js"
                                                  "swagger-ui/libs/jquery.multilevelpushmenu.min.js"]
                                       :externs ["swagger-ui/libs/react.0.12.2.js"
                                                 "swagger-ui/libs/jquery-2.1.4.js"
                                                 "swagger-ui/libs/bootstrap.js"
                                                 "swagger-ui/libs/bootstrapValidator.js"
                                                 "swagger-ui/libs/jquery.multilevelpushmenu.js"
                                                 "swagger-ui/libs/Chart.js"
                                                 "swagger-ui/libs/bootbox.js"
                                                 "swagger-ui/libs/later.js"
                                                 "swagger-ui/libs/isotope.pkgd.js"
                                                 "swagger-ui/libs/packery-mode.pkgd.js"
                                                 "externs/menu.js"
                                                 "externs/charts.js"
                                                 "externs/bootbox.js"
                                                 "externs/bootstrap-validator.js"]
                                       :closure-warnings {:externs-validation :off
                                                          :non-standard-jsdoc :off}

                                       ;; advanced optimization
                                       :optimizations :advanced

                                       ;; no need prettyfication
                                       :pretty-print false}}}})
