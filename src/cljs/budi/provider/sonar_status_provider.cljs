(ns budi.provider.sonar-status-provider
    (:require [budi.dal.local-data-storage :refer [get-sonar-data]]))

(defn provide-sonar-status-data []
  (->> (get-sonar-data)
       deref
       :sonar-status))

