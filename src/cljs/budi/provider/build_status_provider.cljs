(ns budi.provider.build-status-provider
    (:require [budi.dal.local-data-storage :refer [get-build-data]]))


(defn provide-build-status-data []
  (->> (get-build-data)
       deref
       :build-status))

