(ns budi.provider.service-status-provider
    (:require [budi.dal.local-data-storage :refer [get-service-data]]))

(defn categorize-data [service-data]
  (->> service-data
       (map-indexed (fn [idx item]
                      (if (< idx 6)
                        (assoc item :size :full)
                        (if (< idx 15)
                          (assoc item :size :half)
                          (assoc item :size :third)))))))

(defn provide-service-status-data []
  (->> (get-service-data)
       deref
       :service-status
      (sort-by :service-alive?)
      categorize-data))

