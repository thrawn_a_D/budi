(ns budi.schema.validators
  (:require [schema.core :as s]
            [budi.schemas.budi :refer [BuildStatus ServiceStatus]]))

(def validation-messages {:username "User name is a mandatory field that is max. 20 characters long"})

(defn validate [custom-type value]
  (if (nil? (s/check custom-type value))
    true
    false))
