(ns budi.schemas.config.wallet
  (:require [schema.core :as s]))

(s/defschema WalletConfig
             {:wallet-type (s/enum :bamboo-credentials :jenkins-credentials :sonar-credentials)
              :server-url s/Str
              (s/optional-key :username) s/Str
              (s/optional-key :password) s/Str})
