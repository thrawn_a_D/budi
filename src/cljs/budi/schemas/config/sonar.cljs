(ns budi.schemas.config.sonar
  (:require [schema.core :as s]))

(s/defschema SonarConfig
             {:type (s/enum :sonar-job)
              :name s/Str
              :group-id s/Str
              :artefact-id s/Str})
