(ns budi.schemas.budi
  (:require [schema.core :as s]))

(s/defschema BuildStatus
             {:type (s/enum :bamboo-build-job :bamboo-deploy-job :jenkins-build-job)
              :id s/Str
              :name s/Str
              :build-by s/Str
              :build-completed-at s/Int
              :state (s/enum :failed :success :queued :unknown)
              :lifeCycleState (s/enum :running :queued :finished :unknown)
              :execution-time s/Str
              :estimated-next-execution-time s/Str
              (s/optional-key :display-config) s/Any})

(s/defschema ServiceStatus
             {:type (s/enum :http-service-job)
              :name s/Str
              :url s/Str
              :service-alive? s/Bool
              :http-response-code s/Int
              :execution-time s/Str
              :estimated-next-execution-time s/Str})

(s/defschema SonarStatus
             {:type (s/enum :sonar-job)
              :resource s/Str
              :version s/Str
              :metrics {:test-coverage s/Num
                        :violations {:blocker s/Int
                                     :criticle s/Int
                                     :major s/Int
                                     :minor s/Int
                                     :info s/Int}}
              :execution-time s/Str
              :estimated-next-execution-time s/Str})
