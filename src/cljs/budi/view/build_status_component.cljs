(ns budi.view.build-status-component
  (:require [budi.view.build-tiles.recent-status-tile :refer [recent-status-component]]
            [budi.view.build-tiles.older-status-tile :refer [older-status-component]]
            [budi.view.build-tiles.newest-status-tile :refer [newest-status-component]]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]))

(defn single-build-status-element [build-status _]
  (reify
    om/IRenderState
    (render-state [_ state]
      (dom/div nil
               (cond
                 (= "full" (:size (:display-config build-status))) (om/build newest-status-component build-status)
                 (= "half" (:size (:display-config build-status))) (om/build recent-status-component build-status)
                 (= "third" (:size (:display-config build-status))) (om/build older-status-component build-status)
                 )))))

