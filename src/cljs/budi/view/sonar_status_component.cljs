(ns budi.view.sonar-status-component
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs.core.async :as async :refer [chan <! put!]]
            [jayq.core :refer [$ css]]
            [budi.charts.chart-creators :refer [create-chart]]
            [budi.common.edit-fields :refer [display-editing]]
            [budi.charts.chart-data-calculators :refer [get-pie-chart-data-of-coverage]]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]))

(defn set-display-style [element-id display-style]
  (set! (.. (. js/document (getElementById element-id)) -style -display) display-style))

(defn- draw-visible-charts-only [{:keys [test-coverage] :as metrics} element-root-id radar-chart-canvas-id element-visible?]
  (if element-visible?
    (do
      (set-display-style element-root-id "")
      (let [chart-data (get-pie-chart-data-of-coverage test-coverage)]
        (create-chart {:chart-type :doughnut
                       :data (clj->js chart-data)
                       :element-name radar-chart-canvas-id})))
    (set-display-style element-root-id "none")))

(defn process-visibility-change [visibility-channel owner fn-to-call]
  (go (loop []
        (let [element-visible? (<! visibility-channel)]
          (fn-to-call element-visible?)
          (recur)))))

(defn single-sonar-status-element [{:keys [resource
                                           version
                                           metrics]
                                    :as service-status}
                                   owner
                                   {:keys [visibility-channel
                                           visibility-state]}]
  (let [radar-chart-canvas-id (str "radar-chart-" resource)
        element-root-id (str "element-root-" resource)]
    (reify
      om/IDidMount
      (did-mount [_]
        (process-visibility-change visibility-channel
                                   owner
                                   #(draw-visible-charts-only metrics
                                                              element-root-id
                                                              radar-chart-canvas-id
                                                              %))
        (draw-visible-charts-only metrics
                                  element-root-id
                                  radar-chart-canvas-id
                                  visibility-state))

      om/IRenderState
      (render-state [_ state]
        (dom/div #js {:id element-root-id}
                 (dom/div #js {:className "status-header-text text-big"} resource)
                 (dom/div #js {:style #js {:font-size "28px" :text-align "center"}}
                          (dom/span #js {:style #js {:color "#F7464A" :margin "10px"}} (dom/i #js {:className "fa fa-minus-circle"}) (:blocker (:violations metrics)))
                          (dom/span #js {:style #js {:color "#F7464A" :margin "10px"}} (dom/i #js {:className "fa fa-arrow-circle-up"}) (:criticle (:violations metrics)))
                          (dom/span #js {:style #js {:color "#F7464A" :margin "10px"}} (dom/i #js {:className "fa fa-chevron-circle-up"}) (:major (:violations metrics)))
                          (dom/span #js {:style #js {:color "#46BFBD" :margin "10px"}} (dom/i #js {:className "fa fa-chevron-circle-down"}) (:minor (:violations metrics)))
                          (dom/span #js {:style #js {:color "#46BFBD" :margin "10px"}} (dom/i #js {:className "fa fa-arrow-circle-down"}) (:info (:violations metrics))))
                 (dom/div #js {:className "status-header-text text-big"} (str "Test coverage "(:test-coverage metrics) "%"))
                 (dom/div #js {:style #js {:text-align "center"}}
                          (dom/canvas #js {:id radar-chart-canvas-id :height 150 :width 150})))))))

