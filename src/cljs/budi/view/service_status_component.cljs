(ns budi.view.service-status-component
  (:require [om.core :as om :include-macros true]
            [budi.common.job-execution-details :refer [show-execution-details]]
            [om.dom :as dom :include-macros true]))

(defn get-text-color [service-alive?]
  (if service-alive?
    "text-color-black"
    "text-color-white"))

(defn get-square-style [service-alive? size]
  (if service-alive?
    (str "square square--h50 state-green")
    (str "square square--h50 state-red")))

(defn single-service-status-element [{:keys [name
                                             service-alive?
                                             size
                                             http-response-code]
                                      :as service-status} _]
  (reify
    om/IRenderState
    (render-state [_ state]
      (dom/div #js {:style #js {:cursor "pointer"}
                    :className (get-square-style service-alive? (:size service-status))
                    :onClick #(show-execution-details service-status)}
               (dom/span nil
                         (dom/div #js {:className (str "status-second-text "
                                                       (get-text-color service-alive?)
                                                       " text-medium")} name))))))

