(ns budi.view.budi-sections.timed-section
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs.core.async :as async :refer [chan <! put!]]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]))

(defn find-element [value-list element-contain-key element-contain-value]
  (first (filter #(= element-contain-value (get % element-contain-key)) value-list)))

(defn find-next-element [value-list element-contain-key element-contain-value]
  (let [current-element (find-element value-list element-contain-key element-contain-value)
        current-index (.indexOf (to-array value-list) current-element)
        last-index (- (count value-list) 1)
        next-index (if (not (= current-index last-index)) (+ 1 current-index) 0)]
    (nth value-list next-index)))

(defn replace-element [value-list old-element new-element]
  (let [element-index (.indexOf (to-array value-list) old-element)]
    (assoc (into [] (to-array value-list)) element-index new-element)))

(defn switch-element [value-list]
  (let [true-element (find-element value-list :visible true)
        next-element (find-next-element value-list :visible true)
        previously-true-element (assoc true-element :visible false)
        next-true-element (assoc next-element :visible true)]
    (->
      value-list
      (replace-element true-element previously-true-element)
      (replace-element next-element next-true-element))))

(defn notify-elements [owner]
  (let [element-list (om/get-state owner :visibility-control)
        updated-list (switch-element element-list)]
    (doseq [current-element updated-list]
      (put! (:visibility-channel current-element) (:visible current-element)))
    (om/set-state! owner :visibility-control updated-list)))

(defn timed-section [app-data
                     owner
                     {:keys [om-component-fn
                             title]}]
  (reify
    om/IInitState
    (init-state [_]
      {:visibility-control (map-indexed #(hash-map :index %1
                                                   :visibility-channel (chan)
                                                   :visible (if (= 0 %1) true false))
                                        app-data)})
    om/IDidMount
      (did-mount [_]
        (let [scheduling (.. js/later -parse (recur) (every 15) (second))]
          (.setInterval js/later
                        #(notify-elements owner)
                        scheduling)))

    om/IRenderState
    (render-state [_ state]
      (dom/div nil
               (dom/div #js {:className "status-header-text text-big"}
                        title)
               (apply dom/div nil
                      (map-indexed #(om/build om-component-fn
                                              %2
                                              {:opts {:visibility-channel (:visibility-channel (nth (om/get-state owner :visibility-control) %1))
                                                      :visibility-state (:visible (nth (om/get-state owner :visibility-control) %1))}})
                                   app-data))))))
