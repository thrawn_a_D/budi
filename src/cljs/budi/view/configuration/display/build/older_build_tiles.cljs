(ns ^:figwheel-load budi.view.configuration.display.build.older-build-tiles
  (:require-macros [budi.common.form :refer [form]])
  (:require [budi.view.build-tiles.older-status-tile :refer [older-status-component]]
            [budi.common.dropdown :refer [dropdown-menu]]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]))

(def older-build-formatting-toolbar (form [{:row [{:raw (dom/h2 nil "Recent build tiles configuration")}]}
                                           {:row [{:raw (dom/span nil
                                                                  (dom/label nil "Heading font")
                                                                  (om/build dropdown-menu app-data {:opts {:menu-title "Heading font"
                                                                                                           :menu-id "text-font"
                                                                                                           :on-item-click #(om/update! app-data [:texts :status-header-text :font-family] %)
                                                                                                           :selected-item (:font-family (:status-header-text (:texts  app-data)))
                                                                                                           :items ["Arial" "Times" "Courier" "Fantasy" "Verdana" "Cursive"]
                                                                                                           :permit-empty true}}))}
                                                  {:raw (dom/span nil
                                                                  (dom/label nil "Heading size")
                                                                  (om/build dropdown-menu app-data {:opts {:menu-title "Heading font"
                                                                                                           :menu-id "text-font"
                                                                                                           :on-item-click #(om/update! app-data [:texts :status-header-text :font-size] %)
                                                                                                           :selected-item (:font-size (:status-header-text (:texts  app-data)))
                                                                                                           :items ["30px" "26px" "22px" "20px" "16px" "12px"]
                                                                                                           :permit-empty true}}))}]}
                                           {:row [{:raw (dom/span nil
                                                                  (dom/label nil "Heading font weight")
                                                                  (om/build dropdown-menu app-data {:opts {:menu-title "Heading font"
                                                                                                           :menu-id "text-font"
                                                                                                           :on-item-click #(om/update! app-data [:texts :status-header-text :font-weight] %)
                                                                                                           :selected-item (:font-weight (:status-header-text (:texts  app-data)))
                                                                                                           :items ["lighter" "normal" "bold"]
                                                                                                           :permit-empty true}}))}]}]))

(defn older-build-tile-config
  [{:keys [display-config
           sample-build-status] :as config} owner]
  (reify
    om/IRenderState
    (render-state [_ state]
      (dom/div nil
               (dom/div #js {:className "row" :style #js {:margin-left "30px"}}
                        (dom/div #js {:className "col-md-6 col-lg-6 col-xs-6 col-sm-6"}
                                 (om/build older-build-formatting-toolbar (:older-build-status display-config)))
                        (dom/div #js {:className "col-md-6 col-lg-6 col-xs-6 col-sm-6"}
                                 (om/build older-status-component
                                           (assoc sample-build-status :display-config (:older-build-status display-config)))))))))
