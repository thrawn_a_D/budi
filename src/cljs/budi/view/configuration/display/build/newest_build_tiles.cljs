(ns ^:figwheel-load budi.view.configuration.display.build.newest-build-tiles
  (:require-macros [budi.common.form :refer [form]])
  (:require [budi.view.build-tiles.newest-status-tile :refer [newest-status-component]]
            [budi.common.dropdown :refer [dropdown-menu]]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]))

(def newest-build-formatting-toolbar (form [{:row [{:raw (dom/h2 nil "Newest build tiles configuration")}]}
                                            {:row [{:raw (dom/span nil
                                                                   (dom/label nil "Heading font")
                                                                   (om/build dropdown-menu app-data {:opts {:menu-title "Heading font"
                                                                                                            :menu-id "text-font"
                                                                                                            :on-item-click #(om/update! app-data [:texts :status-header-text :font-family] %)
                                                                                                            :selected-item (:font-family (:status-header-text (:texts  app-data)))
                                                                                                            :items ["Arial" "Times" "Courier" "Fantasy" "Verdana" "Cursive"]
                                                                                                            :permit-empty true}}))}
                                                   {:raw (dom/span nil
                                                                   (dom/label nil "Heading size")
                                                                   (om/build dropdown-menu app-data {:opts {:menu-title "Heading font"
                                                                                                            :menu-id "text-font"
                                                                                                            :on-item-click #(om/update! app-data [:texts :status-header-text :font-size] %)
                                                                                                            :selected-item (:font-size (:status-header-text (:texts  app-data)))
                                                                                                            :items ["30px" "26px" "22px" "20px" "16px" "12px"]
                                                                                                            :permit-empty true}}))}]}
                                            {:row [{:raw (dom/span nil
                                                                   (dom/label nil "Heading font weight")
                                                                   (om/build dropdown-menu app-data {:opts {:menu-title "Heading font"
                                                                                                            :menu-id "text-font"
                                                                                                            :on-item-click #(om/update! app-data [:texts :status-header-text :font-weight] %)
                                                                                                            :selected-item (:font-weight (:status-header-text (:texts  app-data)))
                                                                                                            :items ["lighter" "normal" "bold"]
                                                                                                            :permit-empty true}}))}
                                                   {:raw (dom/span nil
                                                                   (dom/label nil "Build date size")
                                                                   (om/build dropdown-menu app-data {:opts {:menu-title "Build date size"
                                                                                                            :menu-id "date-size"
                                                                                                            :on-item-click #(om/update! app-data [:texts :build-completed-at :font-size] %)
                                                                                                            :selected-item (:font-size (:build-completed-at (:texts  app-data)))
                                                                                                            :items ["30px" "26px" "22px" "20px" "16px" "12px"]
                                                                                                            :permit-empty true}}))}]}
                                            {:row [{:raw (dom/span nil
                                                                   (dom/label nil "Build by text max width")
                                                                   (om/build dropdown-menu app-data {:opts {:menu-title "Shortened text max width"
                                                                                                            :menu-id "text-shortened"
                                                                                                            :on-item-click #(om/update! app-data [:texts :shortened-text :width] %)
                                                                                                            :selected-item (:width (:shortened-text (:texts  app-data)))
                                                                                                            :items ["250px" "200px" "150px" "100px"]
                                                                                                            :permit-empty true}}))}
                                                   {:raw (dom/span nil
                                                                   (dom/label nil "Build by size")
                                                                   (om/build dropdown-menu app-data {:opts {:menu-title "Build by size"
                                                                                                            :menu-id "build-by-size"
                                                                                                            :on-item-click #(om/update! app-data [:texts :build-by-text :font-size] %)
                                                                                                            :selected-item (:font-size (:build-by-text (:texts  app-data)))
                                                                                                            :items ["30px" "26px" "22px" "20px" "16px" "12px"]
                                                                                                            :permit-empty true}}))}]}
                                            {:row [{:raw (dom/span nil
                                                                   (dom/label nil "Build timer visibility")
                                                                   (om/build dropdown-menu app-data {:opts {:menu-title "Build timer visibility"
                                                                                                            :menu-id "build-timer-visibility"
                                                                                                            :on-item-click #(om/update! app-data [:texts :time-badge :display] %)
                                                                                                            :selected-item (:display (:time-badge (:texts  app-data)))
                                                                                                            :items ["block" "none"]
                                                                                                            :permit-empty true}}))}]}]))

(defn newest-build-tile-config
  [{:keys [display-config
           sample-build-status] :as config} owner]
  (reify
    om/IRenderState
    (render-state [_ state]
      (dom/div nil
               (dom/div #js {:className "row" :style #js {:margin-left "30px"}}
                        (dom/div #js {:className "col-md-6 col-lg-6 col-xs-6 col-sm-6"}
                                 (om/build newest-build-formatting-toolbar (:newest-build-status display-config)))
                        (dom/div #js {:className "col-md-6 col-lg-6 col-xs-6 col-sm-6"}
                                 (om/build newest-status-component
                                           (assoc sample-build-status :display-config (:newest-build-status display-config)))))))))
