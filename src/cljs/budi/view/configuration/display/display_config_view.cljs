(ns ^:figwheel-load budi.view.configuration.display.display-config-view
  (:require-macros [budi.common.form :refer [form]])
  (:require [budi.common.main-menu :refer [main-menu-component]]
            [budi.dal.local-data-storage :refer [get-display-config-data]]
            [budi.dal.common-budi-dal :refer [save-user-data]]
            [budi.view.configuration.display.build.newest-build-tiles :refer [newest-build-tile-config]]
            [budi.view.configuration.display.build.recent-build-tiles :refer [recent-build-tile-config]]
            [budi.view.configuration.display.build.older-build-tiles :refer [older-build-tile-config]]
            [budi.common.dropdown :refer [dropdown-menu]]
            [taoensso.timbre :as timbre
             :refer-macros (log  trace  debug  info  warn  error  fatal  report
                                logf tracef debugf infof warnf errorf fatalf reportf
                                spy get-env log-env)]
            [ajax.core :refer [PUT]]
            [cljs-time.core :refer [now
                                    plus
                                    seconds]]
            [cljs-time.format :refer [formatters
                                      unparse]]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]))

(enable-console-print!)

(def formatted-now (unparse (formatters :date-time-no-ms) (now)))
(def sample-execution-time (plus (now) (seconds 60)))
(def formatted-sample-execution-time (unparse (formatters :date-time-no-ms) sample-execution-time))

(def sample-build-status {:size :full
                          :type :bamboo-build-job
                          :id "988065878906121221"
                          :name "Project XYZ [PLACEHOLDER] (branches)"
                          :build-by "Authors Name <div>(Add. Info)</div>"
                          :build-completed-at 1473779279000
                          :state :failed
                          :lifeCycleState :finished
                          :execution-time formatted-now
                          :estimated-next-execution-time formatted-sample-execution-time})

(defn update-tile-size
  ([app-data
    selected-size]
   (cond
     (= "Big" selected-size) (update-tile-size app-data 300 300)
     (= "Medium" selected-size) (update-tile-size app-data 250 250)
     (= "Small" selected-size) (update-tile-size app-data 200 200)))
  ([app-data
    width
    height]
   (update-tile-size app-data width height :newest-build-status)
   (update-tile-size app-data width (- (/ height 2) 5) :recent-build-status)
   (update-tile-size app-data (- (/ width 2) 5) (- (/ height 2) 5) :older-build-status))
  ([app-data
    width
    height
    tile-type]
   (om/update! app-data [tile-type :base :width] (str width "px"))
   (om/update! app-data [tile-type :base :height] (str height "px"))))

(def tile-size-toolbar (form [{:row [{:raw (dom/h2 nil "Common build tiles configuration")}]}
                              {:row [{:raw (dom/span nil
                                                     (dom/label nil "Build tile size")
                                                     (om/build dropdown-menu app-data {:opts {:menu-title "Build tile size"
                                                                                              :menu-id "tile-size-toolbar"
                                                                                              :on-item-click #(update-tile-size app-data %)
                                                                                              :items ["Big" "Medium" "Small"]
                                                                                              :permit-empty true}}))}]}]))

(defn common-build-tile-config
  [config owner]
  (reify
    om/IRenderState
    (render-state [_ state]
      (dom/div nil
               (dom/div #js {:className "row" :style #js {:margin-left "30px"}}
                        (dom/div #js {:className "col-md-12 col-lg-12 col-xs-12 col-sm-12"}
                                 (om/build tile-size-toolbar config)))))))

(defn display-config-view [{:keys [display-config
                                   sample-build-status] :as config} owner]
  (reify
    om/IRenderState
    (render-state [_ state]
      (dom/div #js {:className "row"}
               (dom/div #js {:className "col-md-12 col-lg-12 col-xs-12 col-sm-12"}
                        (dom/div #js {:id "pushobj"}
                                 (dom/div #js {:className "row" :style #js {:margin-left "30px"}}
                                          (dom/div #js {:className "col-md-12 col-lg-12 col-xs-12 col-sm-12"}
                                                   (om/build common-build-tile-config display-config)
                                                   (dom/hr #js {:className "colorgraph"} nil)
                                                   (om/build newest-build-tile-config config)
                                                   (dom/hr #js {:className "colorgraph"} nil)
                                                   (om/build recent-build-tile-config config)
                                                   (dom/hr #js {:className "colorgraph"} nil)
                                                   (om/build older-build-tile-config config)
                                                   ))))
               (om/build main-menu-component nil)))))

(defn save-data-on-change
  [app-data]
  (add-watch app-data :watcher
             (fn [key atom old-state new-state]
               (save-user-data "/api/config/display/build-tiles"
                               (:display-config new-state)
                               (fn [update-result] #_(debug "Updating result " update-result))
                               {:method PUT}))))

(if-let [element (. js/document (getElementById "display_config_view"))]
  (get-display-config-data #(do
                              (cljs.core/let [app-data (atom  {:sample-build-status sample-build-status
                                                               :display-config (:display-config (deref %))})]
                                             (save-data-on-change app-data)
                                             (om/root display-config-view
                                                      app-data
                                                      {:target element})))))
