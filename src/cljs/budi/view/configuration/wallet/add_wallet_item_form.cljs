(ns ^:figwheel-load budi.view.configuration.wallet.add-wallet-item-form
  (:require-macros [cljs.core.async.macros :refer [go]]
                   [budi.common.form :refer [form]])
  (:require [cljs.core.async :as async :refer [chan <! put!]]
            [schema.core :as s]
            [budi.common.edit-fields :refer [display-editing]]
            [budi.schema.validators :refer [validation-messages]]
            [budi.common.dropdown :refer [dropdown-menu
                                          get-dropdown-selected-value]]
            [budi.view.configuration.wallet.util.wallet-check :refer [check-wallet]]
            [clojure.set :refer [difference]]
            [om.core :as om :include-macros  true]
            [om.dom :as dom :include-macros true]))

(def new-form-entry
  (atom {:server-url ""}))

(defn wallet-item-form [wallet-entries]
  (form [{:row [{:raw (dom/span nil
                                (dom/label nil "Type")
                                (om/build dropdown-menu app-data {:opts {:menu-id "walletType"
                                                                         :menu-title "Wallet type"
                                                                         :items #{:bamboo-credentials :jenkins-credentials :sonar-credentials}
                                                                         :on-item-click #(om/update! app-data [:wallet-type] %)
                                                                         :selected-item (:wallet-type (om/value app-data))
                                                                         :value-key :wallet-type
                                                                         :permit-empty true}}))}]}
         {:row [{:col {:field-type dom/input
                       :field-key  :server-url
                       :field-label "Server URL"
                       :schema     s/Str}}]}
         {:row [{:col {:field-type dom/input
                       :field-key  :username
                       :field-label "User name"
                       :schema     s/Str}}
                {:col {:field-type dom/input
                       :field-key  :password
                       :field-label "Password"
                       :attributes {:type "password"}
                       :schema     s/Str}}]}
         {:row [{:btn {:style-tag "primary btn-sm"
                       :attributes {:value (if (:wallet-check-result (om/value app-data))
                                             (str "Check wallet ("(:status (:wallet-check-result (om/value app-data)))")")
                                             "Check wallet")
                                    :onClick #(do
                                                (om/update! app-data [:checking-wallet] true)
                                                (go (om/update! app-data
                                                                [:wallet-check-result]
                                                                (<! (check-wallet (keyword (:wallet-type app-data))
                                                                                  (:server-url app-data)
                                                                                  (:username app-data)
                                                                                  (:password app-data))))
                                                    (om/update! app-data [:checking-wallet] false)))}}}]}
         {:row [{:raw (dom/img #js {:src "/images/loading_bar.gif"
                                    :width "100%"
                                    :height "20px;"
                                    :style (display-editing (:checking-wallet (om/value app-data)))})}]}
         {:row [{:btn {:style-tag "info btn-sm"
                       :attributes {:value "Close"
                                    :onClick #((:close-function opts))}}}
                {:btn {:style-tag "warning btn-sm"
                       :attributes {:value "Add"
                                    :id "btn-commit-wallet-item"
                                    :onClick (fn [_] (do
                                                       (om/transact! wallet-entries :wallet-items #(conj % (om/value app-data)))
                                                       (swap! new-form-entry assoc :username ""
                                                              :password ""
                                                              :server-url "")
                                                       ((:close-function opts))))}}}]}]))

(defn add-wallet-item-form [app-data owner]
  (reify
    om/IDidMount
    (did-mount [_]
      (om/root (wallet-item-form app-data)
               new-form-entry
               {:opts {:close-function #(om/set-state! owner :editing-new-entry false)}
                :target (. js/document (getElementById "div-new-wallet-item-form"))}))
    om/IInitState
    (init-state [_]
      {:editing-new-entry false})
    om/IRenderState
    (render-state [this {:keys [editing-new-entry]}]
      (dom/span nil
                (dom/span #js {:className "list-group-item" :style (display-editing editing-new-entry)}
                          (dom/div #js {:id "div-new-wallet-item-form"}))
                (dom/button #js {:className "btn btn-success btn-block navbar-btn btn-sm"
                                 :id "btn-add-wallet-item"
                                 :style (display-editing (not editing-new-entry))
                                 :onClick #(om/set-state! owner :editing-new-entry true)}
                            (dom/span #js {:className "glyphicon glyphicon-plus-sign margin-right-3px"})
                            "Add wallet-item")))))
