(ns budi.view.configuration.wallet.wallet-item-component
  (:require-macros [cljs.core.async.macros :refer [go]]
                   [budi.common.form :refer [form]])
  (:require [cljs.core.async :as async :refer [chan <! put!]]
            [schema.core :as s]
            [budi.common.edit-fields :refer [display-editing]]
            [budi.common.url-utils :refer [get-username-from-url]]
            [budi.schema.validators :refer [validation-messages]]
            [budi.common.dropdown :refer [dropdown-menu]]
            [budi.view.configuration.wallet.util.wallet-check :refer [check-wallet]]
            [om.core :as om :include-macros  true]
            [om.dom :as dom :include-macros true]))

(defn confirm-user-choice [user-message fn-when-accept]
  (fn [e] (.confirm
            js/bootbox
            user-message
            (fn [i-am-sure] (when i-am-sure
                              (fn-when-accept))))))

(defn confirm-deletion [server-url delete-ch close-fn]
  (confirm-user-choice
    (str "Are you sure you want to drop that wallet: <b>"server-url"</b>")
    #(do
       (put! delete-ch server-url)
       (close-fn))))

(def wallet-form
  (form [{:row [{:raw (dom/span nil
                                (dom/label nil "Type")
                                (om/build dropdown-menu app-data {:opts {:menu-id "walletType"
                                                                         :menu-title "Wallet type"
                                                                         :items [:bamboo-credentials :jenkins-credentials :sonar-credentials]
                                                                         :value-key :wallet-type
                                                                         :on-item-click #(om/update! app-data [:wallet-type] %)
                                                                         :selected-item (:wallet-type (om/value app-data))
                                                                         :permit-empty true}}))}]}
         {:row [{:col {:field-type dom/input
                       :field-key  :server-url
                       :field-label "Server URL"
                       :schema     s/Str}}]}
         {:row [{:col {:field-type dom/input
                       :field-key  :username
                       :field-label "User name"
                       :schema     s/Str}}
                {:col {:field-type dom/input
                       :field-key  :password
                       :field-label "Password"
                       :attributes {:type "password"}
                       :schema     s/Str}}]}
         {:row [{:btn {:style-tag "primary btn-sm"
                       :attributes {:value (if (:wallet-check-result (om/value app-data))
                                             (str "Check wallet ("(:status (:wallet-check-result (om/value app-data)))")")
                                             "Check wallet")
                                    :onClick #(do
                                                (om/update! app-data [:checking-wallet] true)
                                                (go (om/update! app-data
                                                                [:wallet-check-result]
                                                                (<! (check-wallet (keyword (:wallet-type app-data))
                                                                                  (:server-url app-data)
                                                                                  (:username app-data)
                                                                                  (:password app-data))))
                                                    (om/update! app-data [:checking-wallet] false)))}}}]}
         {:row [{:raw (dom/img #js {:src "/images/loading_bar.gif"
                                    :width "100%"
                                    :height "20px;"
                                    :style (display-editing (:checking-wallet (om/value app-data)))})}]}
         {:row [{:btn {:style-tag "info btn-sm"
                       :attributes {:value "Done"
                                    :onClick #((:close-function opts))}}}
                {:btn {:style-tag "warning btn-sm"
                       :attributes {:value "Delete"
                                    :id "btn-delete-wallet-form"
                                    :onClick #((confirm-deletion (:server-url (om/value app-data))
                                                                 (:delete-wallet-ch opts)
                                                                 (:close-function opts)))}}}]}]))

(defn edit-wallet-form [app-data
                       owner
                       {:keys [delete-wallet-ch
                               on-close-form]}]
  (reify
    om/IRenderState
    (render-state [this owner]
      (dom/div #js {:className "list-group-item"}
               (om/build wallet-form app-data {:opts {:save-function #(on-close-form)
                                                   :close-function on-close-form
                                                   :delete-wallet-ch delete-wallet-ch}})))))

(defn wallet-item-component [{:keys [server-url wallet-type] :as app-data}
                             owner
                             {:keys [delete-wallet-ch]}]
  (let [form-visible (fn [is-visible] (om/set-state! owner :editing is-visible))]
    (reify
      om/IInitState
      (init-state [_]
        {:editing false})
      om/IRenderState
      (render-state [this {:keys [editing]}]
        (dom/div #js {:className "row-fluid "}
                 (dom/div #js {:className "col-xs-12 content-34h"}
                          (dom/div #js {:className "row voffset1 gray" :id (str "wallet-" server-url)}
                                   (when editing
                                     (om/build edit-wallet-form app-data {:opts {:delete-wallet-ch delete-wallet-ch
                                                                              :on-close-form #(form-visible false)}}))
                                   (dom/div #js {:className "col-xs-8"}
                                            (dom/span #js {:style (display-editing (not editing))}
                                                      server-url))
                                   (dom/div #js {:className "col-xs-4 float-right"}
                                            (dom/span #js {:className "glyphicon glyphicon-trash icon-button hidden-xs"
                                                           :id "btn-delete-wallet"
                                                           :style (display-editing (not editing))
                                                           :onClick (confirm-deletion server-url
                                                                                      delete-wallet-ch
                                                                                      #(form-visible false))})
                                            (dom/span #js {:className "glyphicon glyphicon-edit margin-right-10px icon-button"
                                                           :id "btn-edit-wallet"
                                                           :style (display-editing (not editing))
                                                           :onClick #(form-visible true)})))))))))
