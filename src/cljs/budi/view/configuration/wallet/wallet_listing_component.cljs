(ns budi.view.configuration.wallet.wallet-listing-component
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs.core.async :as async :refer [chan <!]]
            [budi.view.configuration.wallet.wallet-item-component :refer [wallet-item-component]]
            [budi.view.configuration.wallet.add-wallet-item-form :refer [add-wallet-item-form]]
            [om.core :as om :include-macros  true]
            [om.dom :as dom :include-macros true]))

(defn process-token-selection [delete-ch app-state]
  (go (loop []
        (let [wallet-to-delete (<! delete-ch)]
          (om/transact! app-state :wallet-items #(vec (remove (fn [{:keys [server-url]}] (= server-url wallet-to-delete)) %)))
          (recur)))))

(defn wallet-listing-component [{:keys [wallet-items] :as wallet-data} owner]
  (reify
    om/IInitState
    (init-state [_]
      {:delete-wallet-ch (chan)})
    om/IWillMount
    (will-mount [_]
      (let [delete-wallet-ch (om/get-state owner :delete-wallet-ch)]
        (process-token-selection delete-wallet-ch wallet-data)))
    om/IRenderState
    (render-state [this {:keys [delete-wallet-ch]}]
      (dom/div #js {:className "col-md-12"}
               (dom/div #js {:className "list-group" :id "summary_permissions"}
                        (dom/a #js {:className "list-group-item active"}
                               (dom/div #js {:className "row" :id "title"}
                                        (dom/div #js {:className "col-xs-12"}
                                                 "Credentials")))
                        (dom/a #js {:className "list-group-item" :id "description"}
                               (dom/b #js {:className "list-group-item-heading"}
                                      "Info")
                               (dom/p #js {:className "list-group-item-text"}
                                      "Credentials might depend on one or more user and are typically bound to a specific build tool."))
                        (dom/a #js {:className "list-group-item"}
                               (dom/span #js {:className "container-fluid"}
                                         (apply dom/div nil
                                                (om/build-all wallet-item-component
                                                              wallet-items
                                                              {:opts {:delete-wallet-ch delete-wallet-ch}}))
                                         (dom/div #js {:className "row-fluid"}
                                                    (dom/div #js {:className "col-xs-12" :style #js {:margin-top "10px"}}
                                                             (om/build add-wallet-item-form wallet-data))))))))))
