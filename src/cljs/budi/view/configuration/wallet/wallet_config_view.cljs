(ns ^:figwheel-load budi.view.configuration.wallet.wallet-config-view
  (:require [budi.common.main-menu :refer [main-menu-component]]
            [budi.view.configuration.wallet.wallet-listing-component :refer [wallet-listing-component]]
            [budi.dal.local-data-storage :refer [get-wallet-configuration]]
            [budi.dal.common-budi-dal :refer [save-user-data]]
            [budi.common.element :refer [show-success-element
                                         show-form-safe-error]]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]))

(enable-console-print!)

(defn wallet-configuration-view [app _]
  (reify
    om/IRender
    (render [_]
      (dom/div #js {:className "container"}
               (dom/div #js {:className "row"}
                        (dom/div #js {:className "col-md-12 col-lg-12 col-xs-12 col-sm-12"}
                                 (dom/div #js {:id "pushobj"
                                               :className "container"}
                                          (dom/div #js {:className "row"}
                                                   (dom/div #js {:className "col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3"}
                                                            (dom/h2 #js {:className "text-center"}
                                                                    "Environment credentials"
                                                                    (dom/br nil)
                                                                    (dom/small nil
                                                                               "Credentials to be used for your build/deployment jobs"))
                                                            (dom/div #js {:className "bs-callout bs-callout-info"}
                                                                     (dom/p #js {:className "lead"}
                                                                            "To be able to configure your build and deployment jobs, you have to provide valid user credentials of your build tool."))
                                                            (dom/hr #js {:className "colorgraph"} nil)
                                                            (dom/span #js {:id "saving-successful-info-box"
                                                                           :style #js {:display "none" :width "100%"}
                                                                           :className "alert alert-info"}
                                                                      "Successfully saved!")
                                                            (dom/span #js {:id "saving-failed-info-box"
                                                                           :style #js {:display "none" :width "100%"}
                                                                           :className "alert alert-danger"}
                                                                      "Error while saving the profile")
                                                            (dom/div #js {:className "row"}
                                                                     (om/build wallet-listing-component app))
                                                            (dom/hr #js {:className "colorgraph"} nil)
                                                            (dom/div #js {:className "row"}
                                                                     (dom/div #js {:className "col-md-6 col-lg-6 col-xs-6 col-sm-6"}
                                                                              (dom/button #js {:type "button"
                                                                                               :id "btn-save-wallets"
                                                                                               :className "btn btn-primary btn-block"
                                                                                               :onClick #(do
                                                                                                          (save-user-data "/api/config/wallets"
                                                                                                                          (map (fn [wallet] (dissoc wallet :wallet-check-result :checking-wallet))
                                                                                                                               (:wallet-items (om/value app)))
                                                                                                                          (fn [{:keys [call-result]}]
                                                                                                                            (if (= call-result :success)
                                                                                                                              (show-success-element :#saving-successful-info-box)
                                                                                                                              (show-success-element :#saving-failed-info-box)))))}
                                                                                          "Save"))
                                                                     (dom/div #js {:className "col-md-6 col-lg-6 col-xs-6 col-sm-6"}
                                                                              (dom/button #js {:type "button"
                                                                                               :id "btn-cancel"
                                                                                               :className "btn btn-block btn-success"
                                                                                               :onClick   #(set! (.-location js/window)
                                                                                                                 "/api/pages/budi")}
                                                                                          "Back to BuDi")))))))
                        (om/build main-menu-component nil))))))

(if-let [element (. js/document (getElementById "wallet_config_view"))]
  (get-wallet-configuration #(om/root wallet-configuration-view
                                      (atom {:wallet-items %})
                                      {:target element})))
