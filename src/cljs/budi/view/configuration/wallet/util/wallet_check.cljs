(ns budi.view.configuration.wallet.util.wallet-check
  (:require [budi.dal.local-data-storage :refer [check-bamboo-wallet
                                                 check-jenkins-wallet
                                                 check-sonar-wallet]]))

(defmulti check-wallet (fn [wallet-type server-url username password] wallet-type))

(defmethod check-wallet :jenkins-credentials [wallet-type server-url username password]
  (check-jenkins-wallet server-url))

(defmethod check-wallet :bamboo-credentials [wallet-type server-url username password]
  (check-bamboo-wallet server-url username password))

(defmethod check-wallet :sonar-credentials [wallet-type server-url username password]
  (check-sonar-wallet server-url username password))

