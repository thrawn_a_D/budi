(ns ^:figwheel-load budi.view.configuration.build.build-config-view
  (:require [budi.common.main-menu :refer [main-menu-component]]
            [budi.view.configuration.build.build-listing-component :refer [build-listing-component]]
            [budi.dal.common-budi-dal :refer [save-user-data]]
            [budi.dal.local-data-storage :refer [get-build-configuration]]
            [budi.common.element :refer [show-success-element
                                         show-form-safe-error]]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]))

(enable-console-print!)
(def app-data (get-build-configuration))

(defn build-configuration-view [app _]
  (reify
    om/IRender
    (render [_]
      (dom/div #js {:className "container"}
               (dom/div #js {:className "row"}
                        (dom/div #js {:className "col-md-12 col-lg-12 col-xs-12 col-sm-12"}
                                 (dom/div #js {:id "pushobj"}
                                          (dom/div #js {:className "row"}
                                                   (dom/div #js {:className "col-xs-12 margin-left-60"}

                                                            (dom/h2 #js {:className "text-center"}
                                                                    "Monitoring jobs"
                                                                    (dom/br nil)
                                                                    (dom/small nil
                                                                               "Your current jobs to be monitored jobs"))
                                                            (dom/div #js {:className "bs-callout bs-callout-info"}
                                                                     (dom/p #js {:className "lead"}
                                                                            "Define some jobs or services to be monitored."))
                                                            (dom/hr #js {:className "colorgraph"} nil)
                                                            (dom/span #js {:id "build-job-saving-successful-info-box"
                                                                           :style #js {:display "none" :width "100%"}
                                                                           :className "alert alert-info"}
                                                                      "Build jobs successfully saved!")
                                                            (dom/span #js {:id "build-job-saving-failed-info-box"
                                                                           :style #js {:display "none" :width "100%"}
                                                                           :className "alert alert-danger"}
                                                                      "Error while saving the build configuration")
                                                            (dom/span #js {:id "service-job-saving-successful-info-box"
                                                                           :style #js {:display "none" :width "100%"}
                                                                           :className "alert alert-info"}
                                                                      "Service jobs successfully saved!")
                                                            (dom/span #js {:id "service-job-saving-failed-info-box"
                                                                           :style #js {:display "none" :width "100%"}
                                                                           :className "alert alert-danger"}
                                                                      "Error while saving the service configuration")
                                                            (dom/span #js {:id "sonar-job-saving-successful-info-box"
                                                                           :style #js {:display "none" :width "100%"}
                                                                           :className "alert alert-info"}
                                                                      "Sonar jobs successfully saved!")
                                                            (dom/span #js {:id "sonar-job-saving-failed-info-box"
                                                                           :style #js {:display "none" :width "100%"}
                                                                           :className "alert alert-danger"}
                                                                      "Error while saving the sonar configuration")
                                                            (dom/div #js {:className "row"}
                                                                     (om/build build-listing-component app))
                                                            (dom/hr #js {:className "colorgraph"} nil)
                                                            (dom/div #js {:className "row"}
                                                                     (dom/div #js {:className "col-md-12 col-lg-12 col-xs-12 col-sm-12"}
                                                                              (dom/button #js {:type "button"
                                                                                               :id "btn-cancel"
                                                                                               :className "btn btn-block btn-success"
                                                                                               :onClick   #(set! (.-location js/window)
                                                                                                                 "/api/pages/budi")}
                                                                                          "Back to BuDi")))
                                                            (dom/hr #js {:className "colorgraph"} nil)))))
                        (om/build main-menu-component nil))))))

(if-let [element (. js/document (getElementById "build_config_view"))]
  (om/root build-configuration-view
          app-data
          {:target element}))
