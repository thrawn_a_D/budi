(ns ^:figwheel-load budi.view.configuration.build.add-build-item-form
  (:require-macros [cljs.core.async.macros :refer [go-loop go]]
                   [budi.common.form :refer [form]])
  (:require [cljs.core.async :as async :refer [chan <!]]
            [schema.core :as s]
            [cemerick.url :as u]
            [budi.dal.common-budi-dal :refer [save-user-data]]
            [budi.dal.local-data-storage :refer [search-bamboo-builds
                                                 search-jenkins-builds
                                                 search-sonar-projects
                                                 search-bamboo-deployments]]
            [budi.common.edit-fields :refer [display-editing]]
            [budi.common.list :refer [list-component]]
            [om.core :as om :include-macros  true]
            [om.dom :as dom :include-macros true]
            [taoensso.timbre :as timbre
             :refer-macros (log  trace  debug  info  warn  error  fatal  report
                                logf tracef debugf infof warnf errorf fatalf reportf
                                spy get-env log-env)]))

(def new-deploy-job-entry
  (atom {:id ""
         :name ""
         :type :bamboo-deploy-job}))

(defn new-build-job-entry [type]
  (atom {:id ""
         :name ""
         :type type}))

(def new-http-service-entry
  (atom {:url ""
         :name ""
         :type :http-service-job}))

(def new-sonar-entry
  (atom {:name ""
         :group-id ""
         :artefact-id ""
         :type :sonar-job}))

(defn store-new-entry [item opts entries build-job-type]
  (let [data-to-store (assoc item :type build-job-type)]
    (om/transact! entries :build-config #(conj % data-to-store))
    ((:close-function opts))))

(defn store-deploy-job [item opts entries]
  (store-new-entry item opts entries :bamboo-deploy-job)
  (save-user-data (str "/api/config/builds/" (:id item))
                  item
                  #(debug "Deploy job saving result " %)))

(defn store-build-job [item opts entries]
  (store-new-entry item opts entries (:type item))
  (save-user-data (str "/api/config/builds/" (:id item))
                  item
                  #(debug "Build job saving result " %)))

(defn store-service-job [item opts entries]
  (store-new-entry item opts entries :http-service-job)
  (save-user-data (str "/api/config/http-services/" (u/url-encode (:url item)))
                  item
                  #(debug "Http-service job saving result " %)))


(defn store-sonar-job [item opts entries]
  (store-new-entry item opts entries :sonar-job)
  (save-user-data (str "/api/config/sonar/" (:group-id item) "/" (:artefact-id item))
                  item
                  #(debug "Sonar job saving result " %)))

(defn deploy-item-form [deploy-entries]
  (form [{:row [{:col {:field-type dom/input
                       :field-key  :type
                       :field-label "Type"
                       :schema     s/Str
                       :attributes {:value "bamboo-deploy-job"
                                    :readOnly "readonly"}}}]}
         {:row [{:col {:field-type dom/input
                       :field-key  :search-term
                       :field-label "Search for a deployment job"
                       :schema     s/Str}}]}
         {:row [{:btn {:style-tag "warning btn-sm"
                       :attributes {:value "Search"
                                    :id "btn-search-deploy-jobs"
                                    :onClick (fn [_] (do
                                                       (om/update! app-data [:loading-search-result] true)
                                                       (go (om/update! app-data
                                                                       [:search-result]
                                                                       (<! (search-bamboo-deployments (:search-term app-data))))
                                                           (om/update! app-data [:loading-search-result] false))))}}}]}
         {:row [{:raw (dom/img #js {:src "/images/loading_bar.gif"
                                    :width "100%"
                                    :height "20px;"
                                    :style (display-editing (:loading-search-result (om/value app-data)))})}]}
         {:row [{:raw (when (:search-result (om/value app-data))
                        (om/build list-component app-data {:opts {:list-title "Found deployment jobs"
                                                                  :description "Contains found bamboo deployment jobs you were searching for. Click on an item to add it to your list."
                                                                  :headers [:id :name]
                                                                  :on-item-click (fn [item] (store-deploy-job  item opts deploy-entries))
                                                                  :items-key :search-result}}))}]}
         {:row [{:btn {:style-tag "success btn-sm"
                       :attributes {:value "Close"
                                    :onClick #((:close-function opts))}}} ]}]))

(defn jenkins-build-item-form [build-entries]
  (form [{:row [{:col {:field-type dom/input
                       :field-key  :type
                       :field-label "Type"
                       :schema     s/Str
                       :attributes {:value "jenkins-build-job"
                                    :readOnly "readonly"}}}]}
         {:row [{:col {:field-type dom/input
                       :field-key  :search-term
                       :field-label "Search for build job"
                       :schema     s/Str}}]}
         {:row [{:btn {:style-tag "warning btn-sm"
                       :attributes {:value "Search"
                                    :id "btn-search-build-jobs"
                                    :onClick (fn [_] (do
                                                       (om/update! app-data [:loading-search-result] true)
                                                       (go (om/update! app-data
                                                                       [:search-result]
                                                                       (<! (search-jenkins-builds (:search-term app-data))))
                                                           (om/update! app-data [:loading-search-result] false))))}}}]}
         {:row [{:raw (dom/img #js {:src "/images/loading_bar.gif"
                                    :width "100%"
                                    :height "20px;"
                                    :style (display-editing (:loading-search-result (om/value app-data)))})}]}
         {:row [{:raw (when (:search-result (om/value app-data))
                        (om/build list-component app-data {:opts {:list-title "Found build jobs"
                                                                  :description "Contains found jenkins build jobs you were searching for. Click on an item to add it to your list."
                                                                  :headers [:id :name]
                                                                  :on-item-click (fn [item] (store-build-job item opts build-entries))
                                                                  :items-key :search-result}}))}]}
         {:row [{:btn {:style-tag "success btn-sm"
                       :attributes {:value "Close"
                                    :onClick #((:close-function opts))}}} ]}]))

(defn bamboo-build-item-form [build-entries]
  (form [{:row [{:col {:field-type dom/input
                       :field-key  :type
                       :field-label "Type"
                       :schema     s/Str
                       :attributes {:value "bamboo-build-job"
                                    :readOnly "readonly"}}}]}
         {:row [{:col {:field-type dom/input
                       :field-key  :search-term
                       :field-label "Search for build job"
                       :schema     s/Str}}]}
         {:row [{:btn {:style-tag "warning btn-sm"
                       :attributes {:value "Search"
                                    :id "btn-search-build-jobs"
                                    :onClick (fn [_] (do
                                                       (om/update! app-data [:loading-search-result] true)
                                                       (go (om/update! app-data
                                                                       [:search-result]
                                                                       (<! (search-bamboo-builds (:search-term app-data))))
                                                           (om/update! app-data [:loading-search-result] false))))}}}]}
         {:row [{:raw (dom/img #js {:src "/images/loading_bar.gif"
                                    :width "100%"
                                    :height "20px;"
                                    :style (display-editing (:loading-search-result (om/value app-data)))})}]}
         {:row [{:raw (when (:search-result (om/value app-data))
                        (om/build list-component app-data {:opts {:list-title "Found build jobs"
                                                                 :description "Contains found bamboo build jobs you were searching for. Click on an item to add it to your list."
                                                                 :headers [:id :name]
                                                                 :on-item-click (fn [item] (store-build-job  item opts build-entries))
                                                                 :items-key :search-result}}))}]}
         {:row [{:btn {:style-tag "success btn-sm"
                       :attributes {:value "Close"
                                    :onClick #((:close-function opts))}}} ]}]))

(defn http-service-item-form [build-entries]
  (form [{:row [{:col {:field-type dom/input
                       :field-key  :type
                       :field-label "Type"
                       :schema     s/Str
                       :attributes {:value "http-service-job"
                                    :readOnly "readonly"}}}]}
         {:row [{:col {:field-type dom/input
                       :field-key  :url
                       :field-label "Service-URL"
                       :schema     s/Str}}
                {:col {:field-type dom/input
                       :field-key  :name
                       :field-label "Service name"
                       :schema     s/Str}}]}
         {:row [{:btn {:style-tag "success btn-sm"
                       :attributes {:value "Close"
                                    :onClick #((:close-function opts))}}}
                {:btn {:style-tag "warning btn-sm"
                       :attributes {:value "Add"
                                    :id "btn-commit-build-item"
                                    :onClick (fn [_] (store-service-job app-data opts build-entries))}}}]}]))

(defn sonar-item-form [build-entries]
  (form [{:row [{:col {:field-type dom/input
                       :field-key  :type
                       :field-label "Type"
                       :schema     s/Str
                       :attributes {:value "sonar-job"
                                    :readOnly "readonly"}}}]}
         {:row [{:col {:field-type dom/input
                       :field-key  :search-term
                       :field-label "Search for sonar project"
                       :schema     s/Str}}]}
         {:row [{:btn {:style-tag "warning btn-sm"
                       :attributes {:value "Search"
                                    :id "btn-search-sonar-jobs"
                                    :onClick (fn [_] (do
                                                       (om/update! app-data [:loading-search-result] true)
                                                       (go (om/update! app-data
                                                                       [:search-result]
                                                                       (<! (search-sonar-projects (:search-term app-data))))
                                                           (om/update! app-data [:loading-search-result] false))))}}}]}
         {:row [{:raw (dom/img #js {:src "/images/loading_bar.gif"
                                    :width "100%"
                                    :height "20px;"
                                    :style (display-editing (:loading-search-result (om/value app-data)))})}]}
         {:row [{:raw (when (:search-result (om/value app-data))
                        (om/build list-component app-data {:opts {:list-title "Found sonar projects"
                                                                 :description "Contains found bamboo build jobs you were searching for. Click on an item to add it to your list."
                                                                 :headers [:name :group-id :artefact-id]
                                                                 :on-item-click (fn [item] (store-sonar-job item opts build-entries))
                                                                 :items-key :search-result}}))}]}
         {:row [{:btn {:style-tag "success btn-sm"
                       :attributes {:value "Close"
                                    :onClick #((:close-function opts))}}} ]}]))

(defn init-add-form [app-data owner form-visibility-channel div-id visibility-key form form-data]
  (om/root (form app-data)
           form-data
           {:opts {:close-function #(om/set-state! owner visibility-key false)}
            :target (. js/document (getElementById div-id))})
  (go-loop []
           (let [form-visibile? (<! form-visibility-channel)]
             (when form-visibile? (om/set-state! owner visibility-key true)))
           (recur)))

(defn add-build-item-form [app-data owner {:keys [show-add-deploy-ch
                                                  show-add-http-service-ch
                                                  show-add-bamboo-build-ch
                                                  show-add-jenkins-build-ch
                                                  show-add-sonar-ch]}]
  (reify
    om/IDidMount
    (did-mount [_]
      (init-add-form app-data owner show-add-http-service-ch "div-new-http-service-item-form" :editing-new-http-service-entry http-service-item-form new-http-service-entry)
      (init-add-form app-data owner show-add-bamboo-build-ch "div-new-bamboo-build-item-form" :editing-new-bamboo-build-entry bamboo-build-item-form (new-build-job-entry :bamboo-build-job))
      (init-add-form app-data owner show-add-jenkins-build-ch "div-new-jenkins-build-item-form" :editing-new-jenkins-build-entry jenkins-build-item-form (new-build-job-entry :jenkins-build-job))
      (init-add-form app-data owner show-add-deploy-ch "div-new-deploy-item-form" :editing-new-deploy-entry deploy-item-form new-deploy-job-entry)
      (init-add-form app-data owner show-add-sonar-ch "div-new-sonar-form" :editing-new-sonar-entry sonar-item-form new-sonar-entry))
    om/IInitState
    (init-state [_]
      {:editing-new-bamboo-build-entry false
       :editing-new-jenkins-build-entry false
       :editing-new-http-service-entry false
       :editing-new-deploy-entry false
       :editing-new-sonar-entry false})
    om/IRenderState
    (render-state [this {:keys [editing-new-bamboo-build-entry
                                editing-new-jenkins-build-entry
                                editing-new-http-service-entry
                                editing-new-deploy-entry
                                editing-new-sonar-entry]}]
      (dom/span nil
                (dom/span #js {:className "list-group-item" :style (display-editing editing-new-http-service-entry)}
                          (dom/div #js {:id "div-new-http-service-item-form"}))
                (dom/span #js {:className "list-group-item" :style (display-editing editing-new-deploy-entry)}
                          (dom/div #js {:id "div-new-deploy-item-form"}))
                (dom/span #js {:className "list-group-item" :style (display-editing editing-new-bamboo-build-entry)}
                          (dom/div #js {:id "div-new-bamboo-build-item-form"}))
                (dom/span #js {:className "list-group-item" :style (display-editing editing-new-jenkins-build-entry)}
                          (dom/div #js {:id "div-new-jenkins-build-item-form"}))
                (dom/span #js {:className "list-group-item" :style (display-editing editing-new-sonar-entry)}
                          (dom/div #js {:id "div-new-sonar-form"}))))))
