(ns budi.view.configuration.build.build-listing-component
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs.core.async :as async :refer [chan <! put!]]
            [budi.view.configuration.build.build-item-component :refer [build-item-component]]
            [budi.view.configuration.build.add-build-item-form :refer [add-build-item-form]]
            [budi.dal.local-data-storage :refer [delete-job]]
            [om.core :as om :include-macros  true]
            [om.dom :as dom :include-macros true]))

(defn process-job-selection [delete-ch app-state]
  (go (loop []
        (let [job-to-delete (<! delete-ch)]
          (om/transact! app-state :build-config #(vec (remove (fn [entry] (= entry job-to-delete)) %)))
          (delete-job (:type job-to-delete) job-to-delete)
          (recur)))))

(defn get-image-link [job-type]
  (if (= :bamboo-deploy-job job-type)
     "/images/bamboo_deploy_job.png"
     (if (= :bamboo-build-job job-type)
       "/images/bamboo_build_job.png"
       (if (= :http-service-job job-type)
         "/images/http_service_job.png"
         (if (= :jenkins-build-job job-type)
           "/images/jenkins_logo.png"
           "/images/sonar_job.jpg")))))

(defn build-listing-component [build-data owner]
  (reify
    om/IInitState
    (init-state [_]
      {:delete-job-ch (chan)
       :show-add-http-service-ch (chan)
       :show-add-bamboo-build-ch (chan)
       :show-add-jenkins-build-ch (chan)
       :show-add-deploy-ch (chan)
       :show-add-sonar-ch (chan)})
    om/IWillMount
    (will-mount [_]
      (let [delete-job-ch (om/get-state owner :delete-job-ch)]
        (process-job-selection delete-job-ch build-data)))
    om/IRenderState
    (render-state [this {:keys [delete-job-ch
                                show-add-deploy-ch
                                show-add-http-service-ch
                                show-add-sonar-ch
                                show-add-jenkins-build-ch
                                show-add-bamboo-build-ch]}]
      (dom/div #js {:className "col-md-12"}
               (dom/div #js {:className "list-group" :id "summary_permissions"}
                        (dom/a #js {:className "list-group-item active"}
                               (dom/div #js {:className "row" :id "title"}
                                        (dom/div #js {:className "col-xs-12"}
                                                 "Monitoring Jobs")))
                        (dom/a #js {:className "list-group-item" :id "description"}
                               (dom/b #js {:className "list-group-item-heading"}
                                      "Info")
                               (dom/p #js {:className "list-group-item-text"}
                                      "A monitoring job usually depends on the type of build system you use. Choose a monitoring type to be used and provide necessary parameters."))
                        (dom/a #js {:className "list-group-item"}
                               (dom/span #js {:className "container-fluid"}
                                         (apply dom/div nil
                                                (om/build-all build-item-component
                                                              (:build-config build-data)
                                                              {:opts {:delete-job-ch delete-job-ch}}))
                                         (dom/div #js {:className "row-fluid"}
                                                    (dom/div #js {:className "col-xs-12" :style #js {:margin-top "10px"}}
                                                             (om/build add-build-item-form
                                                                       build-data
                                                                       {:opts {:show-add-http-service-ch show-add-http-service-ch
                                                                               :show-add-deploy-ch show-add-deploy-ch
                                                                               :show-add-bamboo-build-ch show-add-bamboo-build-ch
                                                                               :show-add-jenkins-build-ch show-add-jenkins-build-ch
                                                                               :show-add-sonar-ch show-add-sonar-ch}}))))))
               (dom/div #js {:className "btn-group navbar-btn btn-block"
                              :id "btn-add-build-item" }
                         (dom/button #js {:type "button"
                                          :className "btn btn-success btn-block btn-sm dropdown-toggle"
                                          :data-toggle "dropdown"
                                          :aria-haspopup true
                                          :aria-expanded true}
                                     "Add " (dom/span #js {:className "caret"} nil))
                         (dom/ul #js {:className "dropdown-menu btn-block"}
                                 (dom/li nil (dom/a #js {:onClick #(put! show-add-bamboo-build-ch true)}
                                                    (dom/img #js {:src (get-image-link :bamboo-build-job)
                                                                  :style #js {:width "20px;" :height "20px;"}}
                                                             nil)
                                                    " Bamboo Build job"))
                                 (dom/li nil (dom/a #js {:onClick #(put! show-add-deploy-ch true)}
                                                    (dom/img #js {:src (get-image-link :bamboo-deploy-job)
                                                                  :style #js {:width "20px;" :height "20px;"}}
                                                             nil)
                                                    " Deployment job"))
                                 (dom/li nil (dom/a #js {:onClick #(put! show-add-jenkins-build-ch true)}
                                                    (dom/img #js {:src (get-image-link :jenkins-build-job)
                                                                  :style #js {:width "20px;" :height "20px;"}}
                                                             nil)
                                                    " Jenkins Build job"))
                                 (dom/li nil (dom/a #js {:onClick #(put! show-add-http-service-ch true)}
                                                    (dom/img #js {:src (get-image-link :http-service-job)
                                                                  :style #js {:width "20px;" :height "20px;"}}
                                                             nil)
                                                    " Http service job"))
                                 (dom/li nil (dom/a #js {:onClick #(put! show-add-sonar-ch true)}
                                                    (dom/img #js {:src (get-image-link :sonar-job)
                                                                  :style #js {:width "20px;" :height "20px;"}}
                                                             nil)
                                                    " Sonar metrics job"))))))))
