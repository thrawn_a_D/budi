(ns budi.view.configuration.build.build-item-component
  (:require-macros [cljs.core.async.macros :refer [go]]
                   [budi.common.form :refer [form]])
  (:require [cljs.core.async :as async :refer [put!]]
            [schema.core :as s]
            [budi.common.job-execution-details :refer [show-execution-details]]
            [budi.dal.local-data-storage :refer [update-job]]
            [budi.common.edit-fields :refer [display-editing]]
            [budi.common.url-utils :refer [get-username-from-url]]
            [budi.schema.validators :refer [validation-messages]]
            [om.core :as om :include-macros  true]
            [om.dom :as dom :include-macros true]))

(defn confirm-user-choice [user-message fn-when-accept]
  (fn [e] (.confirm
            js/bootbox
            user-message
            (fn [i-am-sure] (when i-am-sure
                              (fn-when-accept))))))

(defn confirm-deletion [build-id delete-ch close-fn]
  (confirm-user-choice
    (str "Are you sure you want to drop that token: <b>"build-id"</b>")
    #(do
       (put! delete-ch build-id)
       (close-fn))))

(defn build-job-form [type]
  (form [{:row [{:col {:field-type dom/input
                       :field-key  :type
                       :field-label "Type"
                       :schema     s/Str
                       :attributes {:value (name type)
                                    :readOnly "readonly"}}}]}
         {:row [{:col {:field-type dom/input
                       :field-key  :id
                       :field-label "Job-Id"
                       :schema     s/Str
                       :attributes {:readOnly "readonly"}}}
                {:col {:field-type dom/input
                       :field-key  :name
                       :field-label "Job title"
                       :schema     s/Str}}]}
         {:row [{:btn {:style-tag "success btn-sm"
                       :attributes {:value "Apply"
                                    :onClick #(do
                                               (update-job type (om/value app-data))
                                               ((:close-function opts)))}}}]}
         {:row [{:btn {:style-tag "primary btn-sm"
                       :attributes {:value "Cancel"
                                    :onClick #((:close-function opts))}}}
                {:btn {:style-tag "warning btn-sm"
                       :attributes {:value "Delete"
                                    :id "btn-delete-token-form"
                                    :onClick #((confirm-deletion (om/value app-data)
                                                                 (:delete-job-ch opts)
                                                                 (:close-function opts)))}}}]}]))

(def deploy-job-form
  (form [{:row [{:col {:field-type dom/input
                       :field-key  :type
                       :field-label "Type"
                       :schema     s/Str
                       :attributes {:value "bamboo-deploy-job"
                                    :readOnly "readonly"}}}]}
         {:row [{:col {:field-type dom/input
                       :field-key  :id
                       :field-label "Environment-Id"
                       :schema     s/Str
                       :attributes {:readOnly "readonly"}}}
                {:col {:field-type dom/input
                       :field-key  :name
                       :field-label "Job title"
                       :schema     s/Str}}]}
         {:row [{:btn {:style-tag "success btn-sm"
                       :attributes {:value "Apply"
                                    :onClick #(do
                                               (update-job :bamboo-deploy-job (om/value app-data))
                                               ((:close-function opts)))}}}]}
         {:row [{:btn {:style-tag "primary btn-sm"
                       :attributes {:value "Cancel"
                                    :onClick #((:close-function opts))}}}
                {:btn {:style-tag "warning btn-sm"
                       :attributes {:value "Delete"
                                    :id "btn-delete-token-form"
                                    :onClick #((confirm-deletion (om/value app-data)
                                                                 (:delete-job-ch opts)
                                                                 (:close-function opts)))}}}]}]))

(def http-service-form
  (form [{:row [{:col {:field-type dom/input
                       :field-key  :type
                       :field-label "Type"
                       :schema     s/Str
                       :attributes {:value "http-service-job"
                                    :readOnly "readonly"}}}]}
         {:row [{:col {:field-type dom/input
                       :field-key  :url
                       :field-label "Service-URL"
                       :schema     s/Str
                       :attributes {:readOnly "readonly"}}}
                {:col {:field-type dom/input
                       :field-key  :name
                       :field-label "Service name"
                       :schema     s/Str}}]}
         {:row [{:btn {:style-tag "success btn-sm"
                       :attributes {:value "Apply"
                                    :onClick #(do (update-job :http-service-job (om/value app-data))
                                                  ((:close-function opts)))}}}]}
         {:row [{:btn {:style-tag "primary btn-sm"
                       :attributes {:value "Cancel"
                                    :onClick #((:close-function opts))}}}
                {:btn {:style-tag "warning btn-sm"
                       :attributes {:value "Delete"
                                    :id "btn-delete-token-form"
                                    :onClick #((confirm-deletion (om/value app-data)
                                                                 (:delete-job-ch opts)
                                                                 (:close-function opts)))}}}]}]))

(def sonar-form
  (form [{:row [{:col {:field-type dom/input
                       :field-key  :type
                       :field-label "Type"
                       :schema     s/Str
                       :attributes {:value "sonar-job"
                                    :readOnly "readonly"}}}]}
         {:row [{:col {:field-type dom/input
                       :field-key  :name
                       :field-label "Name"
                       :schema     s/Str}}]}
         {:row [{:col {:field-type dom/input
                       :field-key  :group-id
                       :field-label "Group id"
                       :schema     s/Str
                       :attributes {:readOnly "readonly"}}}
                {:col {:field-type dom/input
                       :field-key  :artefact-id
                       :field-label "Artefact id"
                       :schema     s/Str
                       :attributes {:readOnly "readonly"}}}]}
         {:row [{:btn {:style-tag "success btn-sm"
                       :attributes {:value "Apply"
                                    :onClick #(do
                                               (update-job :sonar-job (om/value app-data))
                                               ((:close-function opts)))}}}]}
         {:row [{:btn {:style-tag "primary btn-sm"
                       :attributes {:value "Cancel"
                                    :onClick #((:close-function opts))}}}
                {:btn {:style-tag "warning btn-sm"
                       :attributes {:value "Delete"
                                    :id "btn-delete-token-form"
                                    :onClick #((confirm-deletion (om/value app-data)
                                                                 (:delete-job-ch opts)
                                                                 (:close-function opts)))}}}]}]))

(defn get-form [job-type]
  (if (= :bamboo-deploy-job job-type)
    deploy-job-form
    (if (or (= :bamboo-build-job job-type) (= :jenkins-build-job job-type))
      (build-job-form job-type)
      (if (= :sonar-job job-type)
        sonar-form
        http-service-form))))

(defn edit-token-form [app-data
                       owner
                       {:keys [delete-job-ch
                               on-close-form]}]
  (reify
    om/IRenderState
    (render-state [this owner]
      (let [form (get-form (:type app-data))]
       (dom/div #js {:className "list-group-item"}
                (om/build form app-data {:opts {:save-function #(on-close-form)
                                                :close-function on-close-form
                                                :delete-job-ch delete-job-ch}}))))))

(defn get-image-link [job-type]
  (if (= :bamboo-deploy-job job-type)
    "/images/bamboo_deploy_job.png"
    (if (= :bamboo-build-job job-type)
      "/images/bamboo_build_job.png"
      (if (= :http-service-job job-type)
        "/images/http_service_job.png"
        (if (= :jenkins-build-job job-type)
          "/images/jenkins_logo.png"
          "/images/sonar_job.jpg")))))

(defn build-item-component [app-data
                            owner
                            {:keys [delete-job-ch]}]
  (let [form-visible (fn [is-visible] (om/set-state! owner :editing is-visible))]
    (reify
      om/IInitState
      (init-state [_]
        {:editing false})
      om/IRenderState
      (render-state [this {:keys [editing]}]
        (dom/div #js {:className "row-fluid "}
                 (dom/div #js {:className "col-xs-12 content-34h div-hoverable"}
                          (dom/div #js {:className "row voffset1 gray"}
                                   (when editing
                                     (om/build edit-token-form app-data {:opts {:delete-job-ch delete-job-ch
                                                                                :on-close-form #(form-visible false)}}))
                                   (dom/div #js {:className "col-xs-2" :style (display-editing (not editing))}
                                            (dom/img #js {:src (get-image-link (:type app-data))
                                                          :style #js {:width "20px;" :height "20px;"}}
                                                      nil))
                                   (dom/div #js {:className "col-xs-6"}
                                            (dom/span #js {:className "text-shortened" :style (display-editing (not editing))}
                                                      (:name app-data)))
                                   (dom/div #js {:className "col-xs-4 float-right"}
                                            (dom/span #js {:className "glyphicon glyphicon-trash icon-button"
                                                           :id "btn-delete-token"
                                                           :style (display-editing (not editing))
                                                           :onClick (confirm-deletion (om/value app-data)
                                                                                      delete-job-ch
                                                                                      #(form-visible false))})
                                            (dom/span #js {:className "glyphicon glyphicon-list-alt margin-right-10px icon-button"
                                                           :id "btn-execution-details"
                                                           :style (display-editing (not editing))
                                                           :onClick #(show-execution-details (om/value app-data))})
                                            (dom/span #js {:className "glyphicon glyphicon-edit margin-right-10px icon-button"
                                                           :id "btn-edit-token"
                                                           :style (display-editing (not editing))
                                                           :onClick #(form-visible true)})))))))))
