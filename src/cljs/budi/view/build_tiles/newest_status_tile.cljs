(ns budi.view.build-tiles.newest-status-tile
  (:require [budi.common.date-format :refer [format-date-generic]]
            [budi.view.build-tiles.tiles-util :refer [get-square-style
                                                      get-text-color]]
            [budi.common.timer :refer [countdown
                                       cleanup-timer]]
            [om.core :as om :include-macros true]
            [budi.common.job-execution-details :refer [show-execution-details]]
            [om.dom :as dom :include-macros true]))

(defn newest-status-component [{:keys [display-config] :as build-status} _]
  (cljs.core/let [timer-id (str "timer-" (:id build-status))]
                 (reify
                   om/IWillUpdate
                   (will-update [this next-props next-state]
                     (if (not (= :success (:state next-props)))
                       (countdown timer-id (:estimated-next-execution-time next-props))
                       (cleanup-timer timer-id)))

                   om/IDidMount
                   (did-mount [_]
                     (when (not (= :success (:state build-status)))
                      (countdown timer-id (:estimated-next-execution-time build-status))))

                   om/IRenderState
                   (render-state [_ state]
                     (dom/div #js {:style (clj->js (:base display-config))
                                   :className (get-square-style (:state build-status) (:lifeCycleState build-status))
                                   :onClick #(show-execution-details build-status)}
                              (dom/span nil
                                        (dom/div #js {:className (get-text-color (:state build-status))
                                                      :style (clj->js (:status-header-text (:texts display-config)))}
                                                 (:name build-status))
                                        (dom/div nil
                                                 (dom/div #js {:className (get-text-color (:state build-status))
                                                               :style (clj->js (:build-completed-at (:texts display-config)))}
                                                          (format-date-generic "dd MMMM yyyy HH:mm" (js/Date. (:build-completed-at build-status)))))
                                        (dom/div #js {:className (get-text-color (:state build-status))
                                                      :style (clj->js (:build-by-text (:texts display-config)))}
                                                 (dom/div #js {:className "budi-label label-default"
                                                               :style (clj->js (:shortened-text (:texts display-config)))
                                                               :dangerouslySetInnerHTML #js {:__html (:build-by build-status)}} nil))
                                        (dom/div #js {:style (clj->js (:time-badge (:texts display-config)))}
                                                 (dom/span #js {:id timer-id :className "badge"}))))))))
