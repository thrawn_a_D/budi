(ns budi.view.build-tiles.tiles-util
  (:require [budi.common.date-format :refer [format-date-generic]]
            [budi.common.timer :refer [countdown
                                       cleanup-timer]]
            [om.core :as om :include-macros true]
            [budi.common.job-execution-details :refer [show-execution-details]]
            [om.dom :as dom :include-macros true]))

(defn get-text-color [state]
  (if (or (= state :success)
          (= state :unknown))
    "text-color-black"
    "text-color-white"))

(defn get-square-style [state life-cycle-state]
  (if (= state :unknown)
    (str "square state-gray")
    (if (= life-cycle-state :running)
      (str "square state-blue")
      (if (= state :success)
        (str "square state-green")
        (str "square state-red")))))


