(ns budi.view.build-tiles.older-status-tile
  (:require [budi.view.build-tiles.tiles-util :refer [get-square-style
                                                      get-text-color]]
            [om.core :as om :include-macros true]
            [budi.common.job-execution-details :refer [show-execution-details]]
            [om.dom :as dom :include-macros true]))

(defn older-status-component [{:keys [display-config] :as build-status} _]
  (reify
    om/IRenderState
    (render-state [_ state]
      (dom/div #js {:style (clj->js (:base display-config))
                    :className (get-square-style (:state build-status) (:lifeCycleState build-status))
                    :onClick #(show-execution-details build-status)}
               (dom/span nil
                         (dom/div #js {:className (get-text-color (:state build-status))
                                       :style (clj->js (:status-header-text (:texts display-config)))}
                                  (:name build-status)))))))


