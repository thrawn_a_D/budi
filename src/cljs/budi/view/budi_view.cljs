(ns ^:figwheel-load budi.view.budi-view
  (:require [budi.provider.build-status-provider :refer [provide-build-status-data]]
            [budi.provider.service-status-provider :refer [provide-service-status-data]]
            [budi.provider.sonar-status-provider :refer [provide-sonar-status-data]]
            [budi.view.budi-sections.timed-section :refer [timed-section]]
            [budi.view.build-status-component :refer [single-build-status-element]]
            [budi.view.service-status-component :refer [single-service-status-element]]
            [budi.view.sonar-status-component :refer [single-sonar-status-element]]
            [budi.common.main-menu :refer [main-menu-component]]
            [budi.common.isotope :refer [setup-grid
                                         update-grid]]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]))

(enable-console-print!)

(def app-data (atom  {:build-status (provide-build-status-data)
                      :service-status (provide-service-status-data)
                      :sonar-status (provide-sonar-status-data)}))

(let [scheduling (.. js/later -parse (recur) (every 5) (second))]
  (.setInterval js/later
                #(swap! app-data assoc :service-status (provide-service-status-data))
                scheduling))

(let [scheduling (.. js/later -parse (recur) (every 5) (second))]
  (.setInterval js/later
                #(swap! app-data assoc :build-status (provide-build-status-data))
                scheduling))

(let [scheduling (.. js/later -parse (recur) (every 5) (second))]
  (.setInterval js/later
                #(swap! app-data assoc :sonar-status (provide-sonar-status-data))
                scheduling))

(defn build-section [{:keys [title
                             section-data
                             grid-id
                             om-component-fn]}]
  (dom/div nil
           (dom/div #js {:className "status-header-text text-big"}
                    title)
           (dom/div  (if grid-id #js {:id grid-id} nil)
                    (apply dom/span  #js {:className "row"}
                           (om/build-all om-component-fn section-data)))))

(defn build-two-sectioned-display [{:keys [section1
                                           section2]}]
  (dom/div #js {:className "row"}
           (dom/div #js {:className "col-md-8" :style #js {:background "#9E9E9E"}}
                    (build-section section1))
           (dom/div #js {:className "col-md-4" :style #js {:background "gray"}}
                    (build-section section2))))

(defn build-three-sectioned-display [{:keys [section1
                                             section2
                                             section3]}]
  (dom/div #js {:className "row"}
           (dom/div #js {:className "col-md-8" :style #js {:background "#9E9E9E"}}
                    (build-section section1))
           (dom/div #js {:className "col-md-4" :style #js {:background "gray"}}
                    (dom/div #js {:className "row"}
                      (dom/div #js {:className "col-sm-12"}
                               (om/build timed-section
                                         (:section-data section2)
                                         {:opts section2})))
                    (dom/div #js {:className "row"}
                      (dom/div #js {:className "col-sm-12" :style #js {:text-align "center"}}
                               (build-section section3))))))

(defn build-one-sectioned-display [{:keys [section]}]
  (dom/div #js {:className "row"}
           (dom/div #js {:className "col-md-12" :style #js {:background "#9E9E9E"}}
                    (build-section section))))

(defn build-view [{:keys [build-status
                          service-status
                          sonar-status] :as app} owner]
  (let [build-grid-id "budi-build-grid"
        service-grid-id "service-build-grid"
        service-grid-id-keyword (keyword (str "#" service-grid-id))
        build-grid-id-keyword (keyword (str "#" build-grid-id))]
    (reify
      om/IDidMount
      (did-mount [_]
        (setup-grid service-grid-id-keyword)
        (setup-grid build-grid-id-keyword))

      om/IDidUpdate
      (did-update [_ _ _]
        (update-grid service-grid-id-keyword)
        (update-grid build-grid-id-keyword))

      om/IRenderState
      (render-state [_ state]
        (dom/div #js {:className "row"}
                 (dom/div #js {:className "col-md-12 col-lg-12 col-xs-12 col-sm-12"}
                          (dom/div #js {:id "pushobj"}
                                   (dom/div #js {:className "row" :style #js {:margin-left "30px"}}
                                            (dom/div #js {:className "col-md-12 col-lg-12 col-xs-12 col-sm-12"}
                                                     (if (and (not (empty? build-status)) (not (empty? service-status)) (not (empty? sonar-status)))
                                                       (build-three-sectioned-display {:section1 {:title "Build status"
                                                                                                  :section-data build-status
                                                                                                  :grid-id build-grid-id
                                                                                                  :om-component-fn single-build-status-element}
                                                                                       :section2 {:title "Sonar metrics"
                                                                                                  :section-data sonar-status
                                                                                                  :om-component-fn single-sonar-status-element}
                                                                                       :section3 {:title "Service status"
                                                                                                  :section-data service-status
                                                                                                  :grid-id service-grid-id
                                                                                                  :om-component-fn single-service-status-element}})
                                                       (if (and (not (empty? build-status)) (not (empty? service-status)))
                                                         (build-two-sectioned-display {:section1 {:title "Build status"
                                                                                                  :section-data build-status
                                                                                                  :grid-id build-grid-id
                                                                                                  :om-component-fn single-build-status-element}
                                                                                       :section2 {:title "Service status"
                                                                                                  :section-data service-status
                                                                                                  :grid-id service-grid-id
                                                                                                  :om-component-fn single-service-status-element}})
                                                         (if (and (not (empty? build-status)) (empty? service-status))
                                                           (build-one-sectioned-display {:section {:title "Build status"
                                                                                                   :section-data build-status
                                                                                                   :grid-id build-grid-id
                                                                                                   :om-component-fn single-build-status-element}}))))))))
                 (om/build main-menu-component nil))))))


(if-let [element (. js/document (getElementById "build_view"))]
  (om/root build-view
           app-data
           {:target element}))
