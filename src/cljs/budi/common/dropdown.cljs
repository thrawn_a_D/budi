(ns budi.common.dropdown
  (:require [jayq.core :refer [$ html css] :as jayq]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]))

(defn- menu-item [_ _ {:keys [item
                              menu-id
                              on-item-click]}]
  (let [menu-element ($ (keyword (str "#" menu-id)))
        item-value (name item)]
    (om/component
      (dom/a #js {:role     "menuitem"
                  :tabIndex "-1"
                  :href     "#"
                  :id       (str "item-" item-value)
                  :onClick  #(do
                               (-> menu-element (html (str item-value "  <span class=\"caret\"></span>")))
                               (when on-item-click (on-item-click item-value))
                               (.stopPropagation %))}
             item-value))))

(defn dropdown-menu [_ _ {:keys [menu-title
                                 menu-id
                                 on-item-click
                                 selected-item
                                 items
                                 permit-empty]}]
  (reify
    om/IRenderState
    (render-state [_ state]
      (let [show-not-selected-error (and permit-empty (nil? selected-item))
            form-group-style (if show-not-selected-error "form-group has-feedback has-error" "form-group")]
        (dom/div #js {:className form-group-style}
                 (dom/div nil
                          (dom/div #js {:className "dropdown"}
                                   (dom/button #js {:className "btn btn-default dropdown-toggle"
                                                    :type "button"
                                                    :id menu-id
                                                    :data-toggle "dropdown"
                                                    :aria-expand "true"}
                                               (if selected-item
                                                 (str (name selected-item) " ")
                                                 (str menu-title " "))
                                               (dom/span #js {:className "caret"}))
                                   (dom/ul #js {:className "dropdown-menu"
                                                :role "menu"
                                                :aria-labelledby menu-id}
                                           (apply dom/li #js {:role "presentation"}
                                                  (map #(om/build menu-item nil {:opts {:item %
                                                                                        :menu-id menu-id
                                                                                        :on-item-click on-item-click}}) items)))))
                 (when show-not-selected-error
                   (dom/div #js {:className "help-block"}
                            "Please choose a value from the drop down box")))))))

(defn get-dropdown-selected-value [dropdown-id]
  (->> ($ dropdown-id)
      (html)
      (re-find #"^\w+")
      (keyword)))
