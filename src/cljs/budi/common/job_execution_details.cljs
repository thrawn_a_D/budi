(ns budi.common.job-execution-details
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require
    [promesa.core :as p]
    [schema.utils :as sutils]
    [jayq.core :refer [$]]
    [cljs.core.async :as async :refer [chan <!]]
    [budi.dal.local-data-storage :refer [get-job-data
                                         coerce-job-data]]
    [taoensso.timbre :as timbre
     :refer-macros (log  trace  debug  info  warn  error  fatal  report
                        logf tracef debugf infof warnf errorf fatalf reportf
                        spy get-env log-env)]))

(defn- get-job-id [job]
  (let [job-type (:type job)]
   (if (or (= :bamboo-build-job job-type)(= :bamboo-deploy-job job-type) (= :jenkins-build-job job-type))
       (:id job)
       (if (= :sonar-job job-type)
         (:resource job)
         (:url job)))))

(defn- get-job-status [job]
  (let [job-type (:type job)]
   (if (or (= :bamboo-build-job job-type) (= :bamboo-deploy-job job-type) (= :jenkins-build-job job-type))
       (:state job)
       (if (= :sonar-job job-type)
         :unknown
         (:service-alive? job)))))

(defn- get-complete-job-status-form [job]
  (str "<div role=\"form\">"
           "<fieldset>"
             "<div class=\"row\">"
               "<div class=\"col-xs-6 col-sm-6 col-md-6\">"
                  "<span>"
                    "<label>Job name</label>"
                    "<div class=\"form-group has-feedback has-success\">"
                      "<input name=\"txt-group-id\" value=\""(:name job)"\" type=\"text\" class=\"form-control input-sm\" readonly="">"
                    "</div>"
                  "</span>"
               "</div>"
               "<div class=\"col-xs-6 col-sm-6 col-md-6\">"
                  "<span>"
                    "<label>Job id</label>"
                    "<div class=\"form-group has-feedback has-success\">"
                    "<input name=\"txt-group-id\" value=\""(get-job-id job)"\" type=\"text\" class=\"form-control input-sm\" readonly="">"
                    "</div>"
                  "</span>"
               "</div>"
             "</div>"
             "<div class=\"row\">"
               "<div class=\"col-xs-6 col-sm-6 col-md-6\">"
                  "<span>"
                    "<label>Last execution time</label>"
                    "<div class=\"form-group has-feedback has-success\">"
                      "<input value=\""(:execution-time job)"\" type=\"text\" class=\"form-control input-sm\" readonly="">"
                    "</div>"
                  "</span>"
               "</div>"
               "<div class=\"col-xs-6 col-sm-6 col-md-6\">"
                  "<span>"
                    "<label>Next execution time</label>"
                    "<div class=\"form-group has-feedback has-success\">"
                    "<input value=\""(:estimated-next-execution-time job)"\" type=\"text\" class=\"form-control input-sm\" readonly="">"
                    "</div>"
                  "</span>"
               "</div>"
             "</div>"
             "<div class=\"row\">"
               "<div class=\"col-xs-12\">"
                  "<span>"
                    "<label>Build status</label>"
                    "<div class=\"form-group has-feedback has-success\">"
                      "<input value=\""(get-job-status job)"\" type=\"text\" class=\"form-control input-sm\" readonly="">"
                    "</div>"
                  "</span>"
               "</div>"
             "</div>"
           "</fieldset>"
         "</div>"))

(defn- get-no-status-found-form []
  (str "<div role=\"form\">"
         "<fieldset>"
           "<div class=\"row\">"
             "<div class=\"col-xs-12\">"
               "<span>"
                 "<label>No job data found!</label>"
                 "<label>The job data is not yet gathered. There is probably no execution of the job.</label>"
               "</span>"
             "</div>"
           "</div>"
         "</fieldset>"
       "</div>"))

(defn- get-job-status-data [job]
  (if (not (sutils/error? job))
    (get-complete-job-status-form job)
    (get-no-status-found-form)))

(defn- show-job-status-dialog [form-html]
  (.dialog js/bootbox (clj->js {:title "Job execution details"
                                :message form-html
                                :buttons {:add {:label " Restart job"
                                                :className "btn-success glyphicon glyphicon-refresh"
                                                :callback #(debug "Clicked details")}
                                          :cancel {:label " Cancel"
                                                   :className "btn-info glyphicon glyphicon-ban-circle"}}}))
  (.bootstrapValidator ($ :#add-group-form)))

(defn show-execution-details [job]
  (go
    (-> (p/promise (<! (get-job-data (:type job) job)))
        (p/then coerce-job-data)
        (p/then get-job-status-data)
        (p/then show-job-status-dialog))))

