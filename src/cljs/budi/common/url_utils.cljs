 (ns budi.common.url-utils
  (:require
   [clojure.string :as string]))


 (defn- to-map [key-value]
  (if (not (empty? key-value))
   (let [splitted-entries (string/split key-value #"=")
         key-entry (first splitted-entries)
         value-entry (second splitted-entries)]
    (hash-map (keyword (.replace key-entry "_" "-")) value-entry))))

 (defn get-request-params []
  (reduce #(if (map? %1)
           (merge (to-map %2) %1)
           (merge (to-map %1) (to-map %2)))
          (->
           (.-location js/document)
           (string/split #"\?")
           (last)
           (string/split #"&"))))

 (defn get-url-path-segments []
   (->
     (.-location js/document)
     (string/split #"\?")
     (first)
     (string/split #"/")))

 (defn get-username-from-url []
   (->
     (get-url-path-segments)
     (last)))
