(ns budi.common.edit-fields
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs.core.async :as async :refer [put! chan <! timeout]]
            [goog.dom :as gdom]
            [om.core :as om :include-macros  true]
            [om.dom :as dom :include-macros true]))

(defn display-editing [show]
  (if show
      #js {}
      #js {:display "none"}))

(defn commit-change [text owner edit-field]
      (om/set-state! owner edit-field false))

(defn handle-change [e item owner field-name]
  (om/transact! item field-name (fn [_] (.. e -target -value))))

(defn edit-buttons [owner field-name is-editable]
  (dom/span nil
           (dom/span #js {:className "glyphicon glyphicon-check margin-right-10px icon-button"
                          :id "edit-button-save"
                          :style (display-editing is-editable)
                          :onClick #(om/set-state! owner field-name false)})
           (dom/span #js {:className "glyphicon glyphicon-edit margin-right-10px icon-button"
                          :id "edit-button-open"
                          :style (display-editing (not is-editable))
                          :onClick #(om/set-state! owner field-name true)})))

(defn- edit-element [owner
                     item
                     html-element-command
                     is-editable
                     is-editable-field
                     field-name
                     additional-html-attributes]
  (let [field-value (om/value (field-name item))
        element-attributes {:style (display-editing is-editable)
                            :type "text"
                            :className "form-control input-sm"
                            :onChange #(handle-change % item owner field-name)
                            :onKeyPress #(when (== (.-keyCode %) 13)
                                           (commit-change item owner is-editable-field))
                            :onBlur (fn [e] (commit-change item owner is-editable-field))
                            :value field-value}
        merged-attributes (clj->js (merge element-attributes additional-html-attributes))]
    (dom/span nil
              (dom/span #js {:style (display-editing (not is-editable)) :id "edit-field-readonly"} field-value)
              (html-element-command merged-attributes))))

(defn edit-field [owner item is-editable is-editable-field field-name]
  (edit-element owner item dom/input is-editable is-editable-field field-name {:id "edit-field-writable"}))

(defn edit-textarea [owner item is-editable is-editable-field field-name]
  (edit-element owner item dom/textarea is-editable is-editable-field field-name {:id "text-field-writable" :rows 5}))
