(ns budi.common.element
  (:require [jayq.core :refer [$ html css] :as jayq]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]))

(defn show-element [element-name]
    (.. ($ element-name)
              (slideDown 200)))

(defn hide-element [element-name]
  (.. ($ element-name)
      (delay 4000)
      (slideUp 200)))

(defn set-property! [element-id property value]
  (let [element (. js/document (getElementById (name element-id)))]
    (aset element (name property) value)))

(defn show-success-element [element-id]
  (do
    (show-element element-id)
    (hide-element element-id)))

(defn show-form-safe-error [error-info-box-id
                            fields-in-error]
  (do
    (doseq [error-field fields-in-error]
      (->
        ($ (keyword (str "#txt-" (name error-field))))
        jayq/parent
        (jayq/attr :class "form-group has-feedback has-error")
        (jayq/children "i")
        (jayq/attr :class "form-control-feedback glyphicon glyphicon-remove")
        jayq/parent
        (jayq/children "small")
        (css {:display "block"})))
    (show-element error-info-box-id)
    (hide-element error-info-box-id)))
