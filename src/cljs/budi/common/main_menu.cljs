(ns ^:figwheel-load budi.common.main-menu
  (:require
    [jayq.core :refer [$]]
    [om.core :as om :include-macros true]
    [om.dom :as dom :include-macros true]))

(defn ^:export init-menu []
  (.ready ($ "document")
          #(do (.. ($ "#menu")
                   (multilevelpushmenu #js {:containersToPush #js [($ "#pushobj")]
                                            :collapsed        true,
                                            :menuWidth        "300px"
                                            :preventItemClick false}))
               (.. ($ "#menu")
                   (multilevelpushmenu "option" "menuHeight" (.height ($ "document")))))))

(defn main-menu-component [app _ {:keys [user-name]}]
  (reify
    om/IDidMount
    (did-mount [this]
      (init-menu))
    om/IRenderState
    (render-state [_ state]
      (dom/div #js {:id "menu" :style #js {:margin-left "15px"}}
               (dom/nav #js {:className "multilevelpushmenu_wrapper"}
                        (dom/h2 #js {:style #js {:textAlign "left" :display "none"}}
                                (dom/i #js {:className "fa fa-reorder floatRight cursorPointer"} nil)
                                "BuDi")
                        (dom/ul #js {:style #js {:display "none"}}
                                (dom/li nil
                                        (dom/a #js {:href "/api/pages/budi"}
                                               (dom/i #js {:className "fa fa-check-circle-o"} nil)
                                               "Build display wall"))
                                (dom/li nil
                                        (dom/a #js {:className "#"}
                                               (dom/i #js {:className "fa fa-certificate"} nil)
                                               "Configuration")
                                        (dom/h2 nil
                                                (dom/i #js {:className "fa fa-certificate"} nil)
                                                "Configuration")
                                        (dom/ul nil
                                                (dom/li nil
                                                        (dom/a #js {:href "/api/pages/config/wallet"}
                                                               (dom/i #js {:className "glyphicon glyphicon-briefcase"} nil)
                                                               "Wallet configuration"))
                                                (dom/li nil
                                                        (dom/a #js {:href "/api/pages/config/build"}
                                                               (dom/i #js {:className "fa fa-check-circle-o"} nil)
                                                               "Build/Deploy job config"))
                                                (dom/li nil
                                                        (dom/a #js {:href "/api/pages/config/display"}
                                                               (dom/i #js {:className "glyphicon glyphicon-th"} nil)
                                                               "Display configuration"))))))))))
