(ns budi.common.form)
#_(.log js/console (str "something bad happened: "(:field-key (:col (first (:row ~body))))))

(defn btn-element [data owner]
  (let [style-tag (:style-tag data)
        attributes (:attributes data)
        mandatory-attributes {:className (str "btn btn-lg btn-"style-tag" btn-block" )
                              :type      "button"}
        complete-attributes (conj mandatory-attributes attributes)]
    `(dom/input (cljs.core/clj->js ~complete-attributes) nil)))

(defn column [data owner form-id col-data]
  (let [field-type (:field-type col-data)
        field-key (:field-key col-data)
        field-label (:field-label col-data)
        attributes (:attributes col-data)
        field-description (:field-description col-data)
        schema (:schema col-data)
        check-fn-id (str "validation-fn-" (rand-int 10000000))
        mandatory-attributes {:className                 "form-control input-sm"
                              :type                      "text"
                              :id                        (str "txt-" (name field-key))
                              :name                      (str "txt-" (name field-key))
                              :value                     `(~field-key (om/value ~data))
                              :onChange                  `(fn [e#] (om/transact! ~data ~field-key (fn [_#] (.. e# -target -value))))
                              :data-bv-callback          "true"
                              :data-bv-callback-message  `(~field-key budi.schema.validators/validation-messages)
                              :data-bv-callback-callback check-fn-id}
        complete-attributes (conj mandatory-attributes attributes)]
    `(do
       (aset js/window ~check-fn-id (cljs.core/clj->js (fn [value# _#] (if (not (empty? value#)) (nil? (schema.core/check ~schema value#)) true))))
       (dom/span nil
                 (when ~field-label (dom/label nil ~field-label))
                 (when ~field-description (dom/div nil ~field-description))
                 (dom/div (cljs.core/clj->js  {:id (str "form-group-" (name ~field-key)) :className "form-group"})
                          (~field-type (cljs.core/clj->js
                                         ~complete-attributes) nil))))))

(defn element [data owner form-id {:keys [col
                                          raw
                                          btn]}]
  (if col
    (column data owner form-id col)
    (if btn
      (btn-element btn owner)
      (when raw raw))))

(defn row [data owner form-id body]
  (if (> (count (:row body)) 1)
    `(dom/div (cljs.core/clj->js {:className "row" :style (cljs.core/clj->js {:margin "3px;"})})
              ~@(for [row (:row body)]
                  `(dom/div (cljs.core/clj->js {:className "col-xs-6 col-sm-6 col-md-6"})
                            ~(element data owner form-id row))))
    (let [row-entries (first (:row body))]
      `(dom/div (cljs.core/clj->js {:className "row" :style (cljs.core/clj->js {:margin "3px;"})})
                (dom/div (cljs.core/clj->js {:className "col-xs-12 col-sm-12 col-md-12"})
                         ~(element data owner form-id row-entries))))))

(defn get-form-elements [data owner body form-id]
  (if (> (count body) 1)
    (for [current-row body]
      (row data owner form-id current-row))
    (row data owner form-id body)))

(defmacro form
  [body]
  (let [form-id (rand-int 10000000)]
    `(fn [~'app-data ~'owner ~'opts]
       (reify
         om/IDidMount
         (~'did-mount [_#]
           (.bootstrapValidator (jayq.core/$ (keyword (str "#" ~form-id))))
           (.validate (.data (jayq.core/$ (keyword (str "#" ~form-id))) "bootstrapValidator")))
         om.core/IRender
         (~'render [this#]
           (dom/div (cljs.core/clj->js {:id                               ~form-id
                                        :role                             "form"
                                        :data-bv-feedbackicons-valid      "glyphicon glyphicon-ok"
                                        :data-bv-feedbackicons-invalid    "glyphicon glyphicon-remove"
                                        :data-bv-feedbackicons-validating "glyphicon glyphicon-refresh"})
                    (dom/fieldset nil
                                  ~@(get-form-elements 'app-data 'owner body form-id))))))))
