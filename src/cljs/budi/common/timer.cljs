(ns budi.common.timer
  (:require [jayq.core :refer [$ css html]]
            [cljs-time.core :refer [in-minutes
                                    in-seconds
                                    interval
                                    after?
                                    time-now]]
            [cljs-time.format :refer [parse
                                      unparse
                                      formatters]]))

(def timer-running (atom []))

(defn set-interval
  "Invoke the given function after and every delay milliseconds."
  [delay f]
  (js/setInterval f delay))

(defn clear-interval
  "Cancel the periodic invokation specified by the given interval id."
  [interval-id]
  (js/clearInterval interval-id))

(defn get-timer-text [seconds]
  (if (> seconds 60)
    (str "Next update in: " (int (/ seconds 60)) " min")
    (str "Next update in: " seconds " sec")))

(defn is-visible? [element visible?]
  (if visible?
    (css element {:display ""})
    (css element {:display "none"})))

(defn adjust-timer [timer-atom element-id]
  (if (>= (:timer @timer-atom) 0)
    (do
      (-> ($ (keyword (str "#" element-id)))
          (html (get-timer-text (:timer @timer-atom)))
          (is-visible? true))
      (swap! timer-atom assoc :timer (dec (:timer @timer-atom))))
    (clear-interval (:interval-id @timer-atom))))

(defn get-counter-time [next-exec-time]
  (let [in-seconds (in-seconds (interval (time-now) next-exec-time))]
    in-seconds))

(defn remove-timer-if-already-running [timer-div-id]
  (let [already-running-timer (first (filter #(= timer-div-id (:timer-id %)) @timer-running))]
  (when already-running-timer
    (do
      (clear-interval (:interval-id @(:timer-data already-running-timer)))
      (swap! timer-running #(remove (fn [item] (= timer-div-id (:timer-id item))) %))))))

(defn cleanup-timer [timer-div-id]
  (remove-timer-if-already-running timer-div-id)
  (-> ($ (keyword (str "#" timer-div-id)))
      (html "Next updte in: unknown")
      (is-visible? false)))

(defn countdown [timer-div-id estinamed-execution-time]
   (remove-timer-if-already-running timer-div-id)
   (let [timer-atom (atom {})
         next-exec-time (parse (formatters :date-time-no-ms) estinamed-execution-time)]
     (if (after? next-exec-time (time-now))
       (let [exec-time-in-sec (get-counter-time next-exec-time)]
         (reset! timer-atom {:timer exec-time-in-sec})
         (swap! timer-atom assoc :interval-id (set-interval 1000 #(adjust-timer timer-atom timer-div-id)))
         (swap! timer-running conj {:timer-id timer-div-id
                                    :timer-data timer-atom}))
       (cleanup-timer timer-div-id))))
