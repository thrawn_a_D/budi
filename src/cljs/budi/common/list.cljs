(ns budi.common.list
  (:require [jayq.core :refer [$ html css] :as jayq]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]))

(defn list-component [app _ {:keys [list-title
                                    description
                                    on-item-click
                                    headers
                                    items-key]}]
  (reify
    om/IRenderState
    (render-state [_ state]
      (dom/div  #js {:className "panel panel-default"}
               (dom/div #js {:className "panel-heading"}
                        list-title)
               (when description
                 (dom/div #js {:className "panel-body"}
                         (dom/p nil
                            description)))
               (dom/table #js {:className "table"}
                  (dom/thead nil
                     (apply dom/tr nil
                        (map #(dom/th nil (name %)) headers)))
                  (apply dom/tbody nil
                         (map (fn [row-entry]
                                (apply dom/tr #js {:onClick #(on-item-click row-entry) :className "div-hoverable"}
                                       (map (fn [column] (dom/td nil column))
                                            (vals (select-keys row-entry headers)))))
                              (get (om/value app) items-key))))))))
