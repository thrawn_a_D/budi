(ns budi.common.isotope
  (:require [jayq.core :refer [$]]))

(def isotope-options #js {:itemSelector ".square"
                          :layoutMode "packery"
                          :transitionDuration "1.8s"})

(defn setup-grid [grid-identifier]
  (.isotope ($ grid-identifier) isotope-options))

(defn update-grid [grid-identifier]
  (let [$grid-element ($ grid-identifier)]
    (try
      (.isotope $grid-element
               "destroy")
      (catch js/Object e ))
    (setup-grid grid-identifier)))
