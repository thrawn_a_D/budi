(ns budi.charts.chart-creators
  (:require
    [jayq.core :refer [$]]))

(defn- create-chart-from-canvas [element-name]
  (if-let [canvas (. js/document (getElementById element-name))]
    (if-let [context (.getContext canvas "2d")]
     (new js/Chart context))))

(defmulti create-chart
  "Create a chart based on the provided data.
  The element name is an id of a canvas to be drawn at"
  :chart-type)

(defmethod create-chart :doughnut [{:keys [element-name data]}]
  (let [chart (create-chart-from-canvas element-name)]
    (.Doughnut chart data (clj->js {:legendTemplate "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li style=\"color:<%=segments[i].fillColor%>;\"><span style=\"color:gray\"><%if(segments[i].label){%><%=segments[i].label%><%}%></span></li><%}%></ul>"}))))

(defmethod create-chart :bar-chart [{:keys [element-name data]}]
    (let [chart (create-chart-from-canvas element-name)]
          (.Bar chart data)))

(defmethod create-chart :radar-chart [{:keys [element-name data]}]
      (let [chart (create-chart-from-canvas element-name)]
                  (.Radar chart data)))

(defmethod create-chart :polar-area [{:keys [element-name data]}]
  (let [chart (create-chart-from-canvas element-name)]
    (.PolarArea chart data)))
