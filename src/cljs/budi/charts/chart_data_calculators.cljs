(ns budi.charts.chart-data-calculators)

(defn get-radar-chart-data-of-metrics [{:keys [blocker critical major minor info]}]
  {:labels ["blocker" "critical" "major" "minor" "info"]
   :datasets [{:label "Issues count by criticality"
               :fillColor "rgba(220,220,220,0.5)",
               :strokeColor "rgba(220,220,220,0.8)",
               :highlightFill "rgba(220,220,220,0.75)",
               :highlightStroke "rgba(220,220,220,1)",
               :data [blocker critical major minor info]}]})

(defn get-pie-chart-data-of-coverage [coverage]
  [{:value coverage
    :color "#46BFBD"
    :highlight "#5AD3D1"
    :label "Test coverage"}
   {:value (- 100 coverage)
    :color "#F7464A"
    :highlight "#FF5A5E"
    :label "Untested code"}])
