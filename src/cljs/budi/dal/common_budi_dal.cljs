(ns budi.dal.common-budi-dal
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [ajax.core :refer [json-response-format
                               PUT
                               POST
                               GET
                               DELETE]]
            [cljs.core.async :as async :refer [<! put! chan close!]]
            [om.core :as om :include-macros true]))

(defn error-handler [{:keys [status status-text]}]
    (.log js/console (str "something bad happened: " status " " status-text)))

(defn fail-delivery-on-error [{:keys [status status-text]} response-channel]
  (.log js/console (str "something bad happened: " status " " status-text))
  (put! response-channel {}))

(defn get-user-data
  [backend-service-url]
  (let [response-channel (chan)]
    (GET backend-service-url
         {:handler #(put! response-channel %)
          :keywords? true
          :response-format (json-response-format {:keywords? true})
          :error-handler #(fail-delivery-on-error % response-channel)})
    response-channel))

(defn delete-data
  [backend-service-url]
  (let [response-channel (chan)]
    (DELETE backend-service-url
         {:handler #(put! response-channel %)
          :error-handler #(fail-delivery-on-error % response-channel)})
    response-channel))

(defn get-data-map [map-or-cursor]
  (try
    (om/state map-or-cursor)
    (om/value map-or-cursor)
    (catch js/Object ex
      map-or-cursor)))

(defn get-fields-in-error [errorneus-response]
  (-> errorneus-response
      :response
      :errors
      keys))

(defn get-error-status [{:keys [status]}]
  (case status
    409 :conflict
    :error))

(defn get-request-result-structure [body]
  (if (contains? body :status)
    (as-> body b
        (conj b {:call-result (get-error-status body)})
        (conj b {:error-fields (get-fields-in-error b)}))
    {:status 200
     :call-result :success
     :response-body body}))

(defn get-http-method [{:keys [method]}]
  (if method
    method
    POST))

(defn save-user-data [backend-service-url data handler & args]
  (let [callback-fn (fn [body] (handler (get-request-result-structure body)))
        http-method (get-http-method (first args))]
    (http-method backend-service-url
                 {:params (get-data-map data)
                  :keywords? true
                  :error-handler callback-fn
                  :handler callback-fn})))
