(ns budi.dal.local-data-storage
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs.core.async :as async :refer [<! put! chan close!]]
            [cemerick.url :as u]
            [ajax.core :refer [PUT]]
            [schema.coerce :as coerce]
            [budi.schemas.config.build :refer [BuildConfig]]
            [budi.schemas.config.http-service :refer [HttpService]]
            [budi.schemas.config.wallet :refer [WalletConfig]]
            [budi.schemas.config.sonar :refer [SonarConfig]]
            [budi.schemas.config.display :refer [BuildTilesDisplayConfig]]
            [budi.schemas.budi :refer [BuildStatus
                                       ServiceStatus
                                       SonarStatus]]
            [budi.dal.common-budi-dal :refer [get-user-data
                                              save-user-data
                                              delete-data]]
            [taoensso.timbre :as timbre
             :refer-macros (log  trace  debug  info  warn  error  fatal  report
                                logf tracef debugf infof warnf errorf fatalf reportf
                                spy get-env log-env)]))

(def status-data-map (atom {}))

(def coerce-build-status
  (coerce/coercer BuildStatus coerce/json-coercion-matcher))

(def coerce-service-status
  (coerce/coercer ServiceStatus coerce/json-coercion-matcher))

(def coerce-sonar-status
  (coerce/coercer SonarStatus coerce/json-coercion-matcher))

(def coerce-build-config
  (coerce/coercer BuildConfig coerce/json-coercion-matcher))

(def coerce-http-service-config
  (coerce/coercer HttpService coerce/json-coercion-matcher))

(def coerce-sonar-config
  (coerce/coercer SonarConfig coerce/json-coercion-matcher))

(def coerce-wallet-config
  (coerce/coercer WalletConfig coerce/json-coercion-matcher))

(def coerce-display-build-tiles-config
  (coerce/coercer BuildTilesDisplayConfig coerce/json-coercion-matcher))

(defn get-build-data []
  (go
      (let [status-data (<! (get-user-data "/api/status/build-and-deploy?with-display-config=true"))]
        (swap! status-data-map assoc :build-status (map coerce-build-status status-data))))
  status-data-map)

(defn get-service-data []
  (go
      (let [status-data (<! (get-user-data "/api/status/http-services"))]
        (swap! status-data-map assoc :service-status (map coerce-service-status status-data))))
  status-data-map)

(defn get-sonar-data []
  (go
      (let [sonar-data (<! (get-user-data "/api/status/sonar"))]
        (swap! status-data-map assoc :sonar-status (map coerce-sonar-status sonar-data))))
  status-data-map)

(defn get-display-config-data [handler]
  (let [data-map (atom {:display-config {}})]
    (go
      (let [build-tiles-display-config-data (<! (get-user-data "/api/config/display/build-tiles"))]
        (swap! data-map assoc :display-config (coerce-display-build-tiles-config build-tiles-display-config-data))
        (handler data-map)))))

(defn get-build-configuration []
  (let [data-map (atom {:build-config []})]
    (go
      (let [build-config-data (<! (get-user-data "/api/config/builds"))]
        (swap! data-map assoc :build-config (into (into [] (map coerce-build-config build-config-data))
                                                  (:build-config @data-map)))))
    (go
      (let [http-service-config-data (<! (get-user-data "/api/config/http-services"))]
        (swap! data-map assoc :build-config (into (into [] (map #(conj % {:type :http-service-job})
                                                                 (map coerce-http-service-config http-service-config-data)))
                                                   (:build-config @data-map)))))
    (go
      (let [sonar-config-data (<! (get-user-data "/api/config/sonar"))]
        (swap! data-map assoc :build-config (into (into [] (map #(conj % {:type :sonar-job})
                                                                (map coerce-sonar-config sonar-config-data)))
                                                  (:build-config @data-map)))))
    data-map))

(defn get-wallet-configuration [handler]
  (go
    (let [wallet-config-data (<! (get-user-data "/api/config/wallets"))
          coerced-data (map coerce-wallet-config wallet-config-data)]
      (handler (into [] coerced-data)))))

(defn search-bamboo-builds [search-query]
  (get-user-data (str "/api/infos/bamboo/build-jobs?search-term=" search-query)))

(defn search-jenkins-builds [search-query]
  (get-user-data (str "/api/infos/jenkins/build-jobs?search-term=" search-query)))

(defn search-bamboo-deployments [search-query]
  (get-user-data (str "/api/infos/bamboo/deploy-jobs?search-term=" search-query)))

(defn check-bamboo-wallet [server-url username password]
  (get-user-data (str "/api/infos/bamboo/wallet?server-url="server-url"&username="username"&password="password)))

(defn check-jenkins-wallet [server-url]
  (get-user-data (str "/api/infos/jenkins/wallet?server-url="server-url)))

(defn check-sonar-wallet [server-url username password]
  (let [path (str "/api/infos/sonar/wallet?server-url="(u/url-encode server-url))
        subpath (if username (str path "&username="username) path)
        final-path (if password (str subpath "&password="password) subpath)]
    (get-user-data final-path)))

(defn search-sonar-projects [search-query]
  (get-user-data (str "/api/infos/sonar/projects?search-term=" search-query)))

(defn coerce-job-data [job-data]
  (let [job-type (:type job-data)]
    (if (= "bamboo-deploy-job" job-type)
      (coerce-build-status job-data)
      (if (or (= "bamboo-build-job" job-type) (= "jenkins-build-job" job-type))
        (coerce-build-status job-data)
        (if (= "sonar-job" job-type)
          (coerce-sonar-status job-data)
          (coerce-service-status job-data))))))

; ######### JOB DELETION

(defmulti delete-job (fn [job-type job-data] job-type))

(defmethod delete-job :http-service-job [job-type {:keys [url] :as job-data}]
  (delete-data (str "/api/config/http-services/" (u/url-encode url))))

(defmethod delete-job :bamboo-deploy-job [job-type {:keys [id] :as job-data}]
  (delete-data (str "/api/config/builds/" id)))

(defmethod delete-job :jenkins-build-job [job-type {:keys [id] :as job-data}]
  (delete-data (str "/api/config/builds/" id)))

(defmethod delete-job :bamboo-build-job [job-type {:keys [id] :as job-data}]
  (delete-data (str "/api/config/builds/" id)))

(defmethod delete-job :sonar-job [job-type {:keys [group-id article-id] :as job-data}]
  (delete-data (str "/api/config/sonar/" group-id "/" article-id)))

; ########## JOB PATCHING
(defmulti update-job (fn [job-type job-data] job-type))

(defmethod update-job :http-service-job [job-type {:keys [url] :as job-data}]
  (save-user-data (str "/api/config/http-services/" (u/url-encode url))
                  job-data
                  #(debug "Updating result " %)
                  {:method PUT}))

(defmethod update-job :bamboo-deploy-job [job-type {:keys [id] :as job-data}]
  (save-user-data (str "/api/config/builds/" id)
                  job-data
                  #(debug "Updating result " %)
                  {:method PUT}))

(defmethod update-job :bamboo-build-job [job-type {:keys [id] :as job-data}]
  (save-user-data (str "/api/config/builds/" id)
                  job-data
                  #(debug "Updating result " %)
                  {:method PUT}))

(defmethod update-job :sonar-job [job-type {:keys [group-id artefact-id] :as job-data}]
  (save-user-data (str "/api/config/sonar/" group-id "/" artefact-id)
                  job-data
                  #(debug "Updating result " %)
                  {:method PUT}))

; ########## GET JOB DATA
(defmulti get-job-data (fn [job-type job-data] job-type))

(defmethod get-job-data :http-service-job [job-type {:keys [url] :as job-data}]
  (get-user-data (str "/api/status/http-services/" (u/url-encode url))))

(defmethod get-job-data :bamboo-deploy-job [job-type {:keys [id] :as job-data}]
  (get-user-data (str "/api/status/build-and-deploy/" id)))

(defmethod get-job-data :jenkins-build-job [job-type {:keys [id] :as job-data}]
  (get-user-data (str "/api/status/build-and-deploy/" id)))

(defmethod get-job-data :bamboo-build-job [job-type {:keys [id] :as job-data}]
  (get-user-data (str "/api/status/build-and-deploy/" id)))

(defmethod get-job-data :sonar-job [job-type {:keys [group-id artefact-id] :as job-data}]
  (get-user-data (str "/api/status/sonar/" group-id "/" artefact-id)))

