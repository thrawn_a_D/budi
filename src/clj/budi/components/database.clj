(ns budi.components.database
  (:require [clojure.java.io :as io]
            [com.stuartsierra.component :as component]
            [clj-time.local :as l]
            [clj-time.core :as t]
            [gardendb.core :as db]
            [metrics.gauges :refer [defgauge]]
            [taoensso.timbre :as timbre]))

(timbre/refer-timbre)

(defmulti initialize-database (fn [type component] type))

(defmethod initialize-database :atom [type component]
  (timbre/debug "Initialize an atom as local storage")
  (-> component
      (assoc :database (atom {:service-status-sources (:service-status-sources component)
                              :build-status-sources (:build-status-sources component)
                              :initial-refresh-timespan (:initial-refresh-timespan component)
                              :wallet-items (:wallet-items component)
                              :http-service-items (:http-service-items component)
                              :build-items (:build-items component)}))))

(defmethod initialize-database :nosql [type component]
  (let [{{file-path :file-path} :db} component
        file-to-db (io/file file-path)
        file-name (.getName file-to-db)
        root-file-path (.getParent file-to-db)]
    (timbre/debug "Setup nosql db")
    (db/initialize! :path root-file-path
                    :database file-name
                    :clear? true?
                    :db-name "budi"
                    :persists? true)
    (db/load!)
    (-> component
        (assoc :database :gardendb))))

(defmulti disconnect-database (fn [type component] type))
(defmethod disconnect-database :atom [type component]
  (timbre/debug "Disconnecting from local datanase")
  (dissoc component :database))
(defmethod disconnect-database :nosql [type {:keys [database-connection] :as component }]
  (let [{{file-path :file-path} :db} component
        file-to-db (io/file file-path)
        time (l/local-now)
        bkp-file-name (str (.getName file-to-db) "_" (l/format-local-time time :date-time-no-ms) "_bkp")]
    (timbre/debug "Disconnecting from nosql datanase")
    (db/backup! {:loc (str "./" bkp-file-name)})
    (db/force-persist!)
    (dissoc component :database)))

(defrecord Database [app]
  component/Lifecycle
  (start [component]
    (if (:database component)
      component
      (let [{{type :type} :db} component]
        (timbre/debug "Setup database connection")
        (initialize-database type component))))
  (stop [component]
    (if (:database component)
      (let [{{type :type} :db} component]
        (timbre/debug "Disconnecting database")
        (disconnect-database type component))
      component)))

(defn database [options]
  (map->Database options))
