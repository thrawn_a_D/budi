(ns budi.components.status-retreiver
  (:require [com.stuartsierra.component :as component]
            [clojure.core.async :as async :refer :all]
            [budi.status.gathering.coordinator :refer [start-jobs-execution
                                                       stop-jobs-execution]]
            [budi.dao.generic-crud :refer [get-identifiable-data
                                           store-identifiable-data]]
            [taoensso.timbre :as timbre]))

(defn store-incoming-status-results [database
                                     status-result-chan
                                     status-key
                                     id-key]
  (go-loop []
           (if-let [status-query-result (<!! status-result-chan)]
             (do (store-identifiable-data database status-key (get status-query-result id-key) status-query-result)
                 (recur)))))

(defn start-scheduler [database
                       build-status-result-chan
                       service-status-result-chan
                       sonar-status-result-chan
                       initial-refresh-timespan]
  (atom
    (concat
      (start-jobs-execution database :id :build-items build-status-result-chan initial-refresh-timespan)
      (start-jobs-execution database :url :http-service-items service-status-result-chan initial-refresh-timespan)
      (start-jobs-execution database :artefact-id :sonar-items sonar-status-result-chan initial-refresh-timespan))))

(defrecord StatusRetreiver [database]
  component/Lifecycle
  (start [{:keys [initial-refresh-timespan db] :as component}]
    (if (:status-retreiver component)
      component
      (let [db (:database database)]
        (timbre/debug "Start status retreivement ")
        (let [build-status-result-chan (chan 10)
              sonar-status-result-chan (chan 10)
              service-status-result-chan (chan 10)]
          (store-incoming-status-results db build-status-result-chan :build-status :id)
          (store-incoming-status-results db service-status-result-chan :service-status :url)
          (store-incoming-status-results db sonar-status-result-chan :sonar-status :resource)
          (->
            component
            (assoc-in [:status-retreiver :initial-refresh-timespan] initial-refresh-timespan)
            (assoc-in [:status-retreiver :build-status-result-chan] build-status-result-chan)
            (assoc-in [:status-retreiver :service-status-result-chan] service-status-result-chan)
            (assoc-in [:status-retreiver :sonar-status-result-chan] sonar-status-result-chan)
            (assoc-in [:status-retreiver :status-refresh-schedule] (start-scheduler db
                                                                                    build-status-result-chan
                                                                                    service-status-result-chan
                                                                                    sonar-status-result-chan
                                                                                    initial-refresh-timespan)))))))
  (stop [{:keys [status-retreiver] :as component}]
    (if status-retreiver
      (do
        (timbre/debug "Stop status retreivement")
        (close! (:build-status-result-chan status-retreiver))
        (close! (:service-status-result-chan status-retreiver))
        (close! (:sonar-status-result-chan status-retreiver))
        (stop-jobs-execution (:status-refresh-schedule status-retreiver))
        (timbre/debug "Status-Resceiver channel closed"))
      component)))

(defn status-retreiver [options]
  (map->StatusRetreiver options))
