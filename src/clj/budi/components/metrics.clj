(ns budi.components.metrics
  (:require [com.stuartsierra.component :as component]
            [metrics.gauges :refer [gauge-fn]]
            [taoensso.timbre :as timbre]))

(defrecord Metrics [app db metrics]
  component/Lifecycle
  (start [component]
    (let [{dbo :database} db]
     (timbre/debug "Starting metrics")
     (if metrics
       component
       (assoc component :metrics []))))
  (stop [component]
    (timbre/debug "Destroy metrics")
    (dissoc component :metrics)))

(defn metrics
  [options]
  (map->Metrics options))
