(ns budi.components.budi-system
  (:require [com.stuartsierra.component :as component]
            [budi.configuration.loader :refer [load-config]]
            [budi.core :refer [budi-routes]]
            [budi.components.http-server :refer [jetty-server]]
            [budi.components.database :refer [database]]
            [budi.components.status-retreiver :refer [status-retreiver]]
            [budi.components.metrics :refer [metrics]]
            [budi.configuration.logging-config :refer [budi-logging-config]]
            [taoensso.timbre :as timbre]))

(timbre/refer-timbre)

(defn budi-system [config-options]
  (component/system-map
    :database (database config-options)
    :metrics (component/using
               (metrics config-options)
               {:database :database})
    :status-retreiver (component/using
                        (status-retreiver config-options)
                        {:database :database})
    :jetty-server (component/using
                    (jetty-server config-options)
                    {:database :database
                     :status-retreiver :status-retreiver})))

(defn get-budi-configuration [config-file-path]
  (let [loaded-config (load-config config-file-path)]
    (assoc loaded-config :app {:handler budi-routes})))

(def system nil)

(defn init-system [config-file-path]
  (alter-var-root #'system
                  (constantly (budi-system (get-budi-configuration config-file-path)))))

(defn start-system[config-file-path]
  (timbre/set-config! budi-logging-config)
  (init-system config-file-path)
  (alter-var-root #'system component/start))

(defn stop-system[]
  (alter-var-root #'system component/stop))

(defn restart-system [pre-start-fn config-file-path]
  (stop-system)
  (when pre-start-fn (pre-start-fn))
  (start-system config-file-path))

(.addShutdownHook (Runtime/getRuntime) (Thread. (fn [] (do (timbre/info "Budi shutdown hook...")
                                                           (stop-system)))))
