(ns budi.components.http-server
  (:import org.eclipse.jetty.server.Server)
  (:require [com.stuartsierra.component :as component]
            [ring.adapter.jetty :as jetty]
            [taoensso.timbre :as timbre]))

(defrecord JettyServer [app jetty-server]
  component/Lifecycle
  (start [component]
    (let [{:keys [handler]} app]
     (timbre/debug "Starting http server.")
     (if jetty-server
       component
       (let [options (-> component (dissoc :app) (assoc :join? false))
             jetty-server (jetty/run-jetty (handler component) options)]
         (assoc component :jetty-server jetty-server)))))
  (stop [component]
    (if-let [^Server server jetty-server]
      (do
        (timbre/debug "Stopping http server.")
        (.stop server)
        (.join server)
        (dissoc component :jetty-server))
      component)))

(defn jetty-server
  "Create a Jetty server component from a map of options. The component expects
  an :app key that contains a map or record with a :handler key. This allows
  the Ring handler to be supplied in a dependent component.
  All other options are passed to the Jetty adapter, except for :join?, which
  is always false to ensure that starting the component doesn't block the
  current thread."
  [options]
  (map->JettyServer options))
