(ns budi.main
  (:gen-class)
  (:require [budi.components.budi-system :refer [start-system]]
            [clojure.string :as string]
            [clojure.tools.cli :refer [parse-opts]]
            [taoensso.timbre :as timbre]))

(def cli-options
  ;; An option with a required argument
  [["-c" "--config-path CONFIG-PATH" "Path to configuration edn"
    :parse-fn #(new java.io.File %)
    :validate [#(.exists %) "Configuration file is either not there or is not a file"]]
   ["-h" "--help"]])

(defn usage [options-summary]
  (->> [""
        ""
        "-------------------------------------------------------"
        ""
        "Budi the build monitor"
        ""
        "Usage: budi-main.jar --config-path [budi.edn]"
        ""
        "Options:"
        options-summary
        ""
        "Please refer to the manual page for more information."]
       (string/join \newline)))

(defn error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (string/join \newline errors)))

(defn exit [status msg]
  (timbre/info msg)
  (System/exit status))

(defn -main[& args]
  (let [{:keys [options arguments errors summary] :as params} (parse-opts args cli-options)]
    (cond
      (:help options) (exit 0 (usage summary))
      errors (exit 1 (error-msg errors)))
    (start-system (.getAbsolutePath (:config-path options)))))
