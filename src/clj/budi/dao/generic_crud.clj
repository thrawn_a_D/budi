(ns budi.dao.generic-crud
  (require [gardendb.core :as nosql]))

(defmulti get-data (fn [db kathegory] (class db)))
(defmulti get-identifiable-data (fn [db kathegory identifier] (class db)))

(defmulti get-data-count (fn [db kathegory] (class db)))

(defmulti store-data (fn [db kathegory entry] (class db)))
(defmulti store-identifiable-data (fn [db kathegory identifier entry] (class db)))

(defmulti delete-data (fn [db kathegory] (class db)))
(defmulti delete-identifiable-data (fn [db kathegory identifier] (class db)))

;Methods only for test purpose
(defmulti remove-all-entries class)

(defmulti get-all-data class)

; ################## LOCAL CRUD

(defmethod get-data clojure.lang.Atom [db kathegory]
  (when (get @db kathegory)
    (-> @db
        kathegory)))

(defmethod get-identifiable-data clojure.lang.Atom [db kathegory identifier]
  (when (get @db kathegory)
    (-> @db
        kathegory
        identifier)))

(defmethod get-data-count clojure.lang.Atom [db kathegory]
  (count (-> @db
             kathegory)))

(defmethod store-data clojure.lang.Atom [db kathegory entry]
  (swap! db assoc-in [kathegory] entry)
  entry)

(defmethod store-identifiable-data clojure.lang.Atom [db kathegory identifier entry]
  (swap! db assoc-in [kathegory identifier] entry)
  entry)

(defmethod delete-data clojure.lang.Atom [db kathegory]
    (swap! db update-in dissoc kathegory))

(defmethod delete-identifiable-data clojure.lang.Atom [db kathegory identifier]
    (swap! db update-in dissoc [kathegory identifier]))

(defmethod remove-all-entries clojure.lang.Atom [db]
  (reset! db {}))

(defmethod get-all-data clojure.lang.Atom [db]
  @db)

; ################## NOSQL CRUD
(defmethod get-data clojure.lang.Keyword [db kathegory]
  (map #(dissoc % :_id) (nosql/documents kathegory)))

 (defmethod get-identifiable-data clojure.lang.Keyword [db kathegory identifier]
   (dissoc (nosql/pull kathegory identifier) :_id))

 (defmethod get-data-count clojure.lang.Keyword [db kathegory]
   (count (get-data db kathegory)))

 (defmethod store-data clojure.lang.Keyword [db kathegory entry]
   (nosql/put! kathegory entry))

 (defmethod store-identifiable-data clojure.lang.Keyword [db kathegory identifier entry]
   (nosql/put! kathegory (assoc entry :_id identifier)))

 (defmethod delete-data clojure.lang.Keyword [db kathegory]
   (nosql/delete-collection! kathegory))

(defmethod delete-identifiable-data clojure.lang.Keyword [db kathegory identifier]
   (nosql/delete! kathegory identifier))

 (defmethod remove-all-entries clojure.lang.PersistentArrayMap [db]
   )

 (defmethod get-all-data clojure.lang.PersistentArrayMap [db]
   )
