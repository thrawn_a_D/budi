(ns budi.routes.pages.configuration
  (:require
    [ring.util.http-response :refer :all]
    [compojure.api.sweet :refer :all]))

(defroutes* page-configuration-route
  (context* "/config" []
            (GET* "/wallet" [:as req]
                  (resource-response "pages/configuration/wallet-config-min.html" {:root "swagger-ui"}))
            (GET* "/build" [:as req]
                  (resource-response "pages/configuration/build-config-min.html" {:root "swagger-ui"}))
            (GET* "/display" [:as req]
                  (resource-response "pages/configuration/display/display-config-min.html" {:root "swagger-ui"}))))
