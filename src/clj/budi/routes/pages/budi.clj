(ns budi.routes.pages.budi
  (:require
    [ring.util.http-response :refer :all]
    [compojure.api.sweet :refer :all]))

(defroutes* page-budi-route
  (GET* "/budi" [:as req]
        (resource-response "budi-min.html" {:root "swagger-ui"})))

