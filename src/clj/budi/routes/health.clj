(ns budi.routes.health
  (:require [compojure.api.sweet :refer [defroutes* GET* PUT* POST* DELETE*]]
            [schema.core :as s]
            [ring.util.http-response :refer :all]
            [metrics.health.core :as health]))

(health/defhealthcheck "simple-check" (fn [] (health/healthy "Everything is fine")))

(def Health-Response {(s/optional-key :healthy) [s/Str]
                      (s/optional-key :unhealthy) [s/Str]})

(defn get-reult-as-string [result]
  (map #(.getMessage (.getValue %)) result))

(defn get-health-status []
  (let  [{:keys [healthy unhealthy]} (health/check-all)]
    (if healthy
      {:healthy (get-reult-as-string healthy)}
      {:unhealthy (get-reult-as-string unhealthy)})))

(defroutes* health-routes
  (GET* "/health" [:as req]
       :return String
       :responses {200 {:schema Health-Response}}
       (ok (get-health-status)))
)
