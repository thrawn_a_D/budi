(ns budi.routes.config.sonar
  (:require
    [budi.schemas.http-schema :as http-schema]
    [ring.util.http-response :refer [ok]]
    [taoensso.timbre :as timbre]
    [budi.dao.generic-crud :refer [get-data
                                   delete-data
                                   delete-identifiable-data
                                   store-identifiable-data]]
    [budi.status.gathering.coordinator :refer [start-job-execution
                                               restart-job-execution
                                               stop-job-execution]]
    [budi.schemas.config.sonar :refer [SonarConfig]]
    [compojure.api.sweet :refer [defroutes*
                                 GET*
                                 POST*
                                 PUT*
                                 DELETE*
                                 context*]]))

(timbre/refer-timbre)

(defroutes* sonar-routes
            (context* "/config" []
                      (GET* "/sonar" [:as req]
                            :return [SonarConfig]
                            :responses {200 {:schema [SonarConfig]}
                                        404 {:schema budi.schemas.http-schema/Missing}}
                            :components [database]
                            :summary "Get configured sonar"
                            (ok (get-data (:database database) :sonar-items)))
                      (POST* "/sonar/:group-id/:artefact-id" [:as req]
                             :return SonarConfig
                             :body [sonar-item SonarConfig]
                             :path-params [group-id :- String
                                           artefact-id :- String]
                             :components [database
                                          status-retreiver]
                             :summary "Store configured sonar"
                             (let [db (:database database)
                                   initial-refresh-timespan (:initial-refresh-timespan (:status-retreiver status-retreiver))
                                   sonar-status-result-chan (:sonar-status-result-chan (:status-retreiver status-retreiver))
                                   jobs-in-execution (:status-refresh-schedule (:status-retreiver status-retreiver))]
                               (store-identifiable-data db :sonar-items (str group-id ":" artefact-id) sonar-item)
                               (as-> db b
                                     (start-job-execution b :artefact-id sonar-item sonar-status-result-chan initial-refresh-timespan)
                                     (swap! jobs-in-execution conj b))
                               (ok sonar-item)))
                      (PUT* "/sonar/:group-id/:artefact-id" [:as req]
                             :return SonarConfig
                             :body [sonar-item SonarConfig]
                             :path-params [group-id :- String
                                           artefact-id :- String]
                             :components [database
                                          status-retreiver]
                             :summary "update configured sonar"
                            (let [db (:database database)
                                  sonar-status-result-chan (:sonar-status-result-chan (:status-retreiver status-retreiver))]
                              (store-identifiable-data db :sonar-items (str group-id ":" artefact-id) sonar-item)
                              (restart-job-execution db :artefact-id :sonar-status sonar-status-result-chan sonar-item status-retreiver)
                              (ok sonar-item)))
                      (DELETE* "/sonar/:group-id/:artefact-id" [id]
                               :path-params [group-id :- String
                                             artefact-id :- String]
                               :components [database
                                            status-retreiver]
                               (let [jobs-in-execution (:status-refresh-schedule (:status-retreiver status-retreiver))]
                                 (timbre/debug "Removing sonar job " group-id ":" artefact-id)
                                 (stop-job-execution jobs-in-execution artefact-id)
                                 (delete-identifiable-data (:database database) :sonar-items (str group-id ":" artefact-id))
                                 (delete-identifiable-data (:database database) :sonar-status artefact-id)
                                 (ok http-schema/ack)))))
