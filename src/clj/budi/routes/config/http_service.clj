(ns budi.routes.config.http-service
  (:require
    [budi.schemas.http-schema :as http-schema]
    [ring.util.http-response :refer :all]
    [taoensso.timbre :as timbre]
    [budi.dao.generic-crud :refer [get-data
                                   delete-data
                                   delete-identifiable-data
                                   store-identifiable-data]]
    [budi.status.gathering.coordinator :refer [start-job-execution
                                               restart-job-execution
                                               stop-job-execution]]
    [budi.schemas.config.http-service :refer [HttpService]]
    [compojure.api.sweet :refer [defroutes*
                                 GET*
                                 POST*
                                 PUT*
                                 DELETE*
                                 context*]]
    [taoensso.timbre :as timbre]))

(timbre/refer-timbre)

(defroutes* http-service-routes
  (context* "/config/http-services" []
            (GET* "/" [:as req]
                  :return [HttpService]
                  :responses {200 {:schema [HttpService]}
                              404 {:schema budi.schemas.http-schema/Missing}}
                  :components [database]
                  :summary "Get configured http-services"
                  (ok (get-data (:database database) :http-service-items)))
            (POST* "/:url" [:as req]
                  :return HttpService
                  :body [http-service-item HttpService]
                  :path-params [url :- String]
                  :components [database
                               status-retreiver]
                  :summary "Store configured http-services"
                  (let [db (:database database)
                        initial-refresh-timespan (:initial-refresh-timespan (:status-retreiver status-retreiver))
                        service-status-result-chan (:service-status-result-chan (:status-retreiver status-retreiver))
                        jobs-in-execution (:status-refresh-schedule (:status-retreiver status-retreiver))]
                    (store-identifiable-data db :http-service-items url http-service-item)
                    (as-> db b
                        (start-job-execution b :url http-service-item service-status-result-chan initial-refresh-timespan)
                        (swap! jobs-in-execution conj b))
                    (ok http-service-item)))
            (PUT* "/:url" [:as req]
                    :return HttpService
                    :body [http-service-item HttpService]
                    :path-params [url :- String]
                    :components [database
                                 status-retreiver]
                    :summary "Store configured http-services"
                    (let [db (:database database)
                          service-status-result-chan (:service-status-result-chan (:status-retreiver status-retreiver))]
                      (store-identifiable-data db :http-service-items url http-service-item)
                      (restart-job-execution db :url :service-status service-status-result-chan http-service-item status-retreiver)
                      (ok http-service-item)))
            (DELETE* "/:url" [url]
                     :components [database
                                  status-retreiver]
                     (let [jobs-in-execution (:status-refresh-schedule (:status-retreiver status-retreiver))]
                       (timbre/debug "Removing http-service-job " url)
                       (stop-job-execution jobs-in-execution url)
                       (delete-identifiable-data (:database database) :http-service-items url)
                       (delete-identifiable-data (:database database) :service-status url)
                       (ok http-schema/ack)))))
