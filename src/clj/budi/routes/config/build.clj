(ns budi.routes.config.build
  (:require
    [budi.schemas.http-schema :as http-schema]
    [ring.util.http-response :refer :all]
    [taoensso.timbre :as timbre]
    [budi.dao.generic-crud :refer [get-data
                                   delete-data
                                   delete-identifiable-data
                                   store-identifiable-data]]
    [budi.schemas.config.build :refer [BuildConfig]]
    [budi.status.gathering.coordinator :refer [start-job-execution
                                               restart-job-execution
                                               stop-job-execution]]
    [compojure.api.sweet :refer [defroutes*
                                 GET*
                                 PUT*
                                 POST*
                                 DELETE*
                                 context*]]
    [taoensso.timbre :as timbre]))

(timbre/refer-timbre)

(defroutes* build-routes
  (context* "/config/builds" []
            (GET* "/" [:as req]
                  :return String
                  :responses {200 {:schema [BuildConfig]}
                              404 {:schema budi.schemas.http-schema/Missing}}
                  :components [database]
                  :summary "Get configured builds"
                  (ok (get-data (:database database) :build-items)))
            (POST* "/:id" [:as req]
                  :return BuildConfig
                  :body [build-config BuildConfig]
                  :path-params [id :- String]
                  :components [database
                               status-retreiver]
                  :summary "Store configured builds"
                  (let [db (:database database)
                        initial-refresh-timespan (:initial-refresh-timespan (:status-retreiver status-retreiver))
                        build-status-result-chan (:build-status-result-chan (:status-retreiver status-retreiver))
                        jobs-in-execution (:status-refresh-schedule (:status-retreiver status-retreiver))]
                    (store-identifiable-data db :build-items id build-config)
                    (as-> db b
                        (start-job-execution b :id build-config build-status-result-chan initial-refresh-timespan)
                        (swap! jobs-in-execution conj b))
                    (ok build-config)))
            (PUT* "/:id" [:as req]
                  :return BuildConfig
                  :body [build-config BuildConfig]
                  :path-params [id :- String]
                  :components [database
                               status-retreiver]
                  :summary "Update configured builds"
                  (let [db (:database database)
                        build-status-result-chan (:build-status-result-chan (:status-retreiver status-retreiver))]
                    (store-identifiable-data db :build-items id build-config)
                    (restart-job-execution db :id :build-status build-status-result-chan build-config status-retreiver)
                    (ok build-config)))
            (DELETE* "/:id" [id]
                     :path-params [id :- String]
                     :components [database
                                  status-retreiver]
                     (let [jobs-in-execution (:status-refresh-schedule (:status-retreiver status-retreiver))]
                       (timbre/debug "Removing build " id)
                       (stop-job-execution jobs-in-execution id)
                       (delete-identifiable-data (:database database) :build-items id)
                       (delete-identifiable-data (:database database) :build-status id)
                       (ok http-schema/ack)))))
