(ns budi.routes.config.display.build-tiles-display-config
  (:require
    [ring.util.http-response :refer :all]
    [taoensso.timbre :as timbre]
    [budi.dao.generic-crud :refer [get-data
                                   delete-data
                                   store-identifiable-data]]
    [budi.schemas.config.display :refer [BuildTilesDisplayConfig]]
    [budi.configuration.display.display-config-provider :refer [get-display-config-data]]
    [compojure.api.sweet :refer [defroutes*
                                 GET*
                                 POST*
                                 PUT*
                                 context*]]))
(timbre/refer-timbre)


(defroutes* build-tiles-display-config-routes
  (context* "/config/display/build-tiles" []
            (GET* "/" [:as req]
                  :return BuildTilesDisplayConfig
                  :responses {200 {:schema BuildTilesDisplayConfig}
                              404 {:schema budi.schemas.http-schema/Missing}}
                  :components [database]
                  :summary "Get configured display-tiles settings"
                  (ok (get-display-config-data (:database database))))
            (POST* "/" [:as req]
                  :return BuildTilesDisplayConfig
                  :body [build-tiles-config BuildTilesDisplayConfig]
                  :components [database]
                  :summary "Store configured display-tiles settings"
                  (do
                    (delete-data (:database database) :display-config)
                    (store-identifiable-data (:database database) :display-config :build-tiles-display-config build-tiles-config)
                    (ok build-tiles-config)))
            (PUT* "/" [:as req]
                  :return BuildTilesDisplayConfig
                  :body [build-tiles-config BuildTilesDisplayConfig]
                  :components [database]
                  :summary "Store configured display-tiles settings"
                  (do
                    (delete-data (:database database) :display-config)
                    (store-identifiable-data (:database database) :display-config :build-tiles-display-config build-tiles-config)
                    (ok build-tiles-config)))))
