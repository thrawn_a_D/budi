(ns budi.routes.config.wallet
  (:require
    [ring.util.http-response :refer :all]
    [taoensso.timbre :as timbre]
    [budi.dao.generic-crud :refer [get-data
                                   delete-data
                                   store-identifiable-data]]
    [budi.schemas.config.wallet :refer [WalletConfig]]
    [compojure.api.sweet :refer [defroutes*
                                 GET*
                                 POST*
                                 context*]]))

(timbre/refer-timbre)

(defroutes* wallet-routes
  (context* "/config" []
            (GET* "/wallets" [:as req]
                  :return [WalletConfig]
                  :responses {200 {:schema [WalletConfig]}
                              404 {:schema budi.schemas.http-schema/Missing}}
                  :components [database]
                  :summary "Get configured wallets"
                  (ok (get-data (:database database) :wallet-items)))
            (POST* "/wallets" [:as req]
                  :return [WalletConfig]
                  :body [wallet-items [WalletConfig]]
                  :components [database]
                  :summary "Store configured wallets"
                  (do
                    (delete-data (:database database) :wallet-items)
                    (doseq [{:keys [server-url] :as wallet-item} wallet-items]
                      (store-identifiable-data (:database database) :wallet-items server-url wallet-item))
                    (ok wallet-items)))))
