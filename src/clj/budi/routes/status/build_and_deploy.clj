(ns budi.routes.status.build-and-deploy
  (:require
    [budi.schemas.http-schema :as http-schema]
    [schema.core :as s]
    [ring.util.http-response :refer :all]
    [taoensso.timbre :as timbre]
    [budi.configuration.display.display-config-provider :refer [merge-display-config-with-build-config]]
    [budi.dao.generic-crud :refer [get-data
                                   get-identifiable-data
                                   delete-identifiable-data]]
    [budi.schemas.budi :refer [build-status]]
    [compojure.api.sweet :refer [defroutes*
                                 GET*
                                 DELETE*
                                 context*]]))

(timbre/refer-timbre)

(defroutes* build-and-deploy-routes
  (context* "/status/build-and-deploy" []
            (GET* "/" [:as req]
                  :return build-status
                  :responses {200 {:schema [build-status]}
                              404 {:schema http-schema/Missing}}
                  :query-params [{with-display-config :- Boolean false}]
                  :components [database]
                  :summary "Get wall display status data"
                  (let [data (get-data (:database database) :build-status)]
                    (if with-display-config
                      (ok (merge-display-config-with-build-config (:database database) data))
                      data)))
            (GET* "/:id" [:as req]
                  :return build-status
                  :responses {200 {:schema build-status}
                              404 {:schema http-schema/Missing}}
                  :path-params [id :- String]
                  :components [database]
                  :summary "Get wall display status data"
                  (let [data (get-identifiable-data (:database database) :build-status id)]
                    (if data
                      (ok data)
                      (not-found (http-schema/not-found "Build/Deployment status data is not found")))))
            (DELETE* "/:id" [id]
                     :components [database]
                     (do
                       (delete-identifiable-data (:database database) :build-status id)
                       (ok http-schema/ack)))))

