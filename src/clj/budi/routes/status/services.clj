(ns budi.routes.status.services
  (:require
    [budi.schemas.http-schema :as http-schema]
    [ring.util.http-response :refer :all]
    [taoensso.timbre :as timbre]
    [budi.dao.generic-crud :refer [get-data
                                   get-identifiable-data
                                   delete-identifiable-data]]
    [budi.schemas.budi :refer [build-status
                               service-status]]
    [compojure.api.sweet :refer [defroutes*
                                 GET*
                                 DELETE*
                                 context*]]))

(timbre/refer-timbre)

(defroutes* service-status-routes
  (context* "/status/http-services" []
            (GET* "/" [:as req]
                  :return String
                  :responses {200 {:schema [service-status]}
                              404 {:schema http-schema/Missing}}
                  :components [database]
                  :summary "Get wall display service status data"
                  (ok (get-data (:database database) :service-status)))
            (GET* "/:url" [:as req]
                  :return String
                  :responses {200 {:schema service-status}
                              404 {:schema http-schema/Missing}}
                  :path-params [url :- String]
                  :components [database]
                  :summary "Get wall display service status data of a single entry"
                  (let [data (get-identifiable-data (:database database) :service-status url)]
                    (if data
                     (ok data)
                     (not-found (http-schema/not-found "Service status data is not found")))))
            (DELETE* "/:url" [url]
                     :components [database]
                     (do
                       (delete-identifiable-data (:database database) :service-status url)
                       (ok http-schema/ack)))))

