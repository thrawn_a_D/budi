(ns budi.routes.status.sonar
  (:require
    [budi.schemas.http-schema :as http-schema]
    [budi.schemas.budi :refer [sonar-status]]
    [ring.util.http-response :refer :all]
    [taoensso.timbre :as timbre]
    [budi.dao.generic-crud :refer [get-data
                                   get-identifiable-data
                                   delete-identifiable-data]]
    [compojure.api.sweet :refer [defroutes*
                                 GET*
                                 DELETE*
                                 context*]]))
(timbre/refer-timbre)

(defroutes* sonar-status-routes
  (context* "/status/sonar" []
            (GET* "/" [:as req]
                  :return String
                  :responses {200 {:schema [sonar-status]}
                              404 {:schema http-schema/Missing}}
                  :components [database]
                  :summary "Get wall display sonar status data"
                  (ok (get-data (:database database) :sonar-status)))
            (GET* "/:group-id/:artefact-id" [:as req]
                  :return String
                  :responses {200 {:schema sonar-status}
                              404 {:schema http-schema/Missing}}
                  :path-params [group-id :- String
                                artefact-id :- String]
                  :components [database]
                  :summary "Get wall display sonar status data for a specific project"
                  (let [resource (str group-id ":" artefact-id)
                        data (get-identifiable-data (:database database) :sonar-status resource)]
                    (if data
                      (ok data)
                      (not-found (http-schema/not-found "Service status data is not found")))))
            (DELETE* "/:resource" [resource]
                     :components [database]
                     (do
                       (delete-identifiable-data (:database database) :sonar-status resource)
                       (ok http-schema/ack)))))
