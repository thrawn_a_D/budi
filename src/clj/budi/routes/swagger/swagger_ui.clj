(ns budi.routes.swagger.swagger-ui
  (:require
    [ring.util.http-response :refer :all]
    [taoensso.timbre :as timbre]
    [compojure.api.sweet :refer :all]))

(timbre/refer-timbre)

(defroutes* swagger-ui-route
            (GET* "/swagger-ui" [:as req]
                  (do
                    (debug "Received a GET request for swagger-ui")
                    (resource-response "index.html" {:root "swagger-ui"}))))

