(ns budi.routes.info.bamboo
  (:require
    [budi.schemas.http-schema :as http-schema]
    [budi.util.http-utils :refer [http-get]]
    [ring.util.http-response :refer :all]
    [taoensso.timbre :as timbre]
    [budi.dao.generic-crud :refer [get-data]]
    [budi.util.wallet-utils :refer [get-wallet]]
    [budi.schemas.config.build :refer [BuildConfig]]
    [budi.schemas.config.wallet :refer [WalletConfig]]
    [schema.core :as s]
    [compojure.api.sweet :refer [defroutes*
                                 GET*
                                 POST*
                                 context*]]))

(timbre/refer-timbre)

(defn map-search-result-to-build-config [search-results]
  (map #(hash-map :id (:id %)
                  :type :bamboo-build-job
                  :name (:planName (:searchEntity %))) search-results))

(defn map-search-result-to-deploy-config [search-results]
  (mapcat (fn [{:keys [environments] :as deploy-job}]
            (map #(hash-map :id (str (:id %))
                            :type :bamboo-deploy-job
                            :name (str (:name deploy-job) " | " (:name %))) environments))
          search-results))

(defn search-for-bamboo-jobs [bamboo-url
                              bamboo-user-name
                              bamboo-password
                              search-term]
  (let [response (http-get (str bamboo-url "/rest/api/latest/search/plans?searchTerm=" search-term)
                           bamboo-user-name
                           bamboo-password)]
    (-> response
        :body
        :searchResults
        map-search-result-to-build-config)))

(defn search-for-bamboo-deploy-jobs [bamboo-url
                                     bamboo-user-name
                                     bamboo-password
                                     search-term]
  (let [response (http-get (str bamboo-url "/rest/api/latest/deploy/project/all")
                           bamboo-user-name
                           bamboo-password)]
    (->> response
        :body
        (filter #(.contains (.toLowerCase (:name %))
                            (.toLowerCase search-term)))
        map-search-result-to-deploy-config)))

(defn isSuccess? [status]
  (if (= 200 status)
    {:status :success }
    {:status :failed}))

(defn check-credentials [bamboo-url
                         bamboo-user-name
                         bamboo-password]
  (try
    (let [response (http-get (str bamboo-url "/rest/api/latest/info")
                             bamboo-user-name
                             bamboo-password)]
      (->> response
          :status
          isSuccess?))
    (catch Exception ex {:status :failed})))

(defroutes* bamboo-infos
  (context* "/infos/bamboo" []
            (GET* "/build-jobs" [:as req]
                  :return String
                  :responses {200 {:schema [BuildConfig]}
                              404 {:schema http-schema/Missing}}
                  :components [database]
                  :query-params [search-term :- s/Str]
                  :summary "Search for a specific build job in bamboo"
                  (let [{:keys [server-url username password]} (get-wallet (:database database) :bamboo-credentials)]
                    (ok (search-for-bamboo-jobs server-url username password search-term))))
            (GET* "/deploy-jobs" [:as req]
                  :return String
                  :responses {200 {:schema [BuildConfig]}
                              404 {:schema http-schema/Missing}}
                  :components [database]
                  :query-params [search-term :- s/Str]
                  :summary "Search for a specific deployment job in bamboo"
                  (let [{:keys [server-url username password]} (get-wallet (:database database) :bamboo-credentials)]
                    (ok (search-for-bamboo-deploy-jobs server-url username password search-term))))
            (GET* "/wallet" [:as req]
                  :return String
                  :responses {200 {:schema {:status (s/enum :success :failed)}}
                              404 {:schema http-schema/Missing}}
                  :query-params [server-url :- s/Str
                                 username :- s/Str
                                 password :- s/Str]
                  :components [database]
                  :summary "Check a given bamboo wallet"
                  (ok (check-credentials server-url username password)))))
