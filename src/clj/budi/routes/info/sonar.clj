(ns budi.routes.info.sonar
  (:require
    [budi.schemas.http-schema :as http-schema]
    [budi.util.http-utils :refer [http-get]]
    [ring.util.http-response :refer :all]
    [taoensso.timbre :as timbre]
    [budi.dao.generic-crud :refer [get-data]]
    [budi.util.wallet-utils :refer [get-wallet]]
    [budi.schemas.config.sonar :refer [SonarConfig]]
    [budi.schemas.config.wallet :refer [WalletConfig]]
    [schema.core :as s]
    [compojure.api.sweet :refer [defroutes*
                                 GET*
                                 POST*
                                 context*]]))

(timbre/refer-timbre)

(defn map-search-result-to-sonar-config [search-results]
  (map #(hash-map :type :sonar-job
                  :group-id (first (clojure.string/split (:k %) #":"))
                  :artefact-id (second (clojure.string/split (:k %) #":"))
                  :name (:nm %)) search-results))

(defn search-for-sonar-projects [sonar-url
                                 sonar-user-name
                                 sonar-password
                                 search-term]
  (let [response (http-get (str sonar-url "/api/projects?search=" search-term))]
    (-> response
        :body
        map-search-result-to-sonar-config)))

(defn isSuccess? [status]
  (if (= 200 status)
    {:status :success }
    {:status :failed}))

(defn check-credentials [sonar-url]
  (try
    (let [response (http-get (str sonar-url "/api/webservices/list"))]
      (->> response
          :status
          isSuccess?))
    (catch Exception ex {:status :failed})))

(defroutes* sonar-infos
  (context* "/infos/sonar" []
            (GET* "/projects" [:as req]
                  :return String
                  :responses {200 {:schema [SonarConfig]}
                              404 {:schema http-schema/Missing}}
                  :components [database]
                  :query-params [search-term :- s/Str]
                  :summary "Search for a specific project in sonar"
                  (let [{:keys [server-url username password]} (get-wallet (:database database) :sonar-credentials)]
                    (ok (search-for-sonar-projects server-url username password search-term))))
            (GET* "/wallet" [:as req]
                  :return String
                  :responses {200 {:schema {:status (s/enum :success :failed)}}
                              404 {:schema http-schema/Missing}}
                  :query-params [server-url :- s/Str]
                  :components [database]
                  :summary "Check a given sonar wallet"
                  (ok (check-credentials server-url)))))
