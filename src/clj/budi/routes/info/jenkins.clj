(ns budi.routes.info.jenkins
  (:require
    [budi.schemas.http-schema :as http-schema]
    [budi.util.http-utils :refer [http-get]]
    [ring.util.http-response :refer :all]
    [taoensso.timbre :as timbre]
    [budi.dao.generic-crud :refer [get-data]]
    [budi.util.wallet-utils :refer [get-wallet]]
    [budi.schemas.config.build :refer [BuildConfig]]
    [budi.schemas.config.wallet :refer [WalletConfig]]
    [schema.core :as s]
    [compojure.api.sweet :refer [defroutes*
                                 GET*
                                 POST*
                                 context*]]))

(timbre/refer-timbre)

(defn get-id-from-name
  [displayName]
  (clojure.string/replace displayName #" " "%20"))

(defn get-job-data
  [parent-display-name parent-name job]
  (let [display-name (if parent-display-name (str parent-display-name " > " (:displayName job)) (:displayName job))
        name (if parent-name (str parent-name "_job_" (get-id-from-name (:name job)) ) (get-id-from-name (:name job)))]
    (if (:jobs job)
      (map #(get-job-data display-name name %) (:jobs job))
      (do
        {:id name
         :type :jenkins-build-job
         :name display-name }))))

(defn search-for-jenkins-jobs [server-url search-term]
  (let [build-result (http-get (str server-url "/api/json?depth=3&pretty=true&tree=jobs[displayName,name,jobs[displayName,name,color,jobs[displayName,name,color]]]"))]
    (flatten (map #(get-job-data nil nil %) (:jobs (:body build-result))))))

(defn isSuccess? [status]
  (if (= 200 status)
    {:status :success }
    {:status :failed}))

(defn check-credentials [jenkins-url]
  (try
    (let [response (http-get jenkins-url)]
      (->> response
          :status
          isSuccess?))
    (catch Exception ex {:status :failed})))

(defroutes* jenkins-infos
  (context* "/infos/jenkins" []
            (GET* "/build-jobs" [:as req]
                  :return String
                  :responses {200 {:schema [BuildConfig]}
                              404 {:schema http-schema/Missing}}
                  :components [database]
                  :query-params [search-term :- s/Str]
                  :summary "Search for a specific build job in jenkins"
                  (let [{:keys [server-url]} (get-wallet (:database database) :jenkins-credentials)]
                    (ok (search-for-jenkins-jobs server-url search-term))))
            (GET* "/wallet" [:as req]
                  :return String
                  :responses {200 {:schema {:status (s/enum :success :failed)}}
                              404 {:schema http-schema/Missing}}
                  :query-params [server-url :- s/Str]
                  :components [database]
                  :summary "Check a given jenkins wallet"
                  (ok (check-credentials server-url)))))
