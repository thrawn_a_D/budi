(ns budi.util.http-utils
  (:require [clj-http.client :as client]))

(defn http-get
  ([url
    username
    password]
   (client/get url
               {:as :json
                :accept :json
                 :insecure? true
                :basic-auth [username password]}))
  ([url]
  (client/get url
              {:as :auto
               :socket-timeout 4000
               :insecure? true
               :conn-timeout 4000})))
