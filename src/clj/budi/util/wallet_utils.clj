(ns budi.util.wallet-utils
  (:require [budi.dao.generic-crud :refer [get-data]]))

(defmulti get-wallet (fn [database wallet-type] wallet-type))

(defn get-credentials [wallet-items credential-type]
    (-> (filter #(= (:wallet-type %) credential-type) wallet-items)
              first))

(defmethod get-wallet :jenkins-credentials [database wallet-type]
  (get-credentials (get-data database :wallet-items) :jenkins-credentials))

(defmethod get-wallet :bamboo-credentials [database wallet-type]
  (get-credentials (get-data database :wallet-items) :bamboo-credentials))

(defmethod get-wallet :sonar-credentials [database wallet-type]
    (get-credentials (get-data database :wallet-items) :sonar-credentials))
