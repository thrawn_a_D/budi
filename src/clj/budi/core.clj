(ns budi.core
  (:gen-class)
  (:require
    (compojure [handler :as handler])
    [budi.routes.status.services :as services-status]
    [budi.routes.status.sonar :as sonar-status]
    [budi.routes.status.build-and-deploy :as build-and-deploy]
    [budi.routes.config.wallet :as wallet-config]
    [budi.routes.config.build :as build-config]
    [budi.routes.config.sonar :as sonar-config]
    [budi.routes.config.display.build-tiles-display-config :as build-tiles-display-config]
    [budi.routes.config.http-service :as http-service]
    [budi.routes.health :as health]
    [budi.routes.pages.budi :as page-budi]
    [budi.routes.pages.configuration :as page-configuration]
    [budi.routes.info.bamboo :as info-bamboo]
    [budi.routes.info.jenkins :as info-jenkins]
    [budi.routes.info.sonar :as sonar-bamboo]
    [budi.routes.swagger.swagger-ui :as swagger-ui]
    [metrics.core :refer [default-registry]]
    [metrics.ring.expose :refer [expose-metrics-as-json]]
    [compojure.api.sweet :refer :all]
    [compojure.route :refer [resources]]))

(defn budi-routes [components]
  (defapi app
    {:components components}
    (swagger-ui)
    (swagger-docs
      {:info {:title "Budi api"}})
    (context* "" []
              :description "Budi api that supports access to build data."
              (handler/site
                (->
                  (context "/api" [:as req]
                           swagger-ui/swagger-ui-route
                           health/health-routes
                           services-status/service-status-routes
                           sonar-status/sonar-status-routes
                           build-and-deploy/build-and-deploy-routes
                           http-service/http-service-routes
                           wallet-config/wallet-routes
                           build-config/build-routes
                           sonar-config/sonar-routes
                           build-tiles-display-config/build-tiles-display-config-routes
                           info-bamboo/bamboo-infos
                           info-jenkins/jenkins-infos
                           sonar-bamboo/sonar-infos
                           (context "/pages" [:as req]
                                    page-budi/page-budi-route
                                    page-configuration/page-configuration-route)
                           (resources "/" {:root "swagger-ui"}))
                  (expose-metrics-as-json "/api/metrics" default-registry {:pretty-print? true}))))))
