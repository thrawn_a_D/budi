(ns budi.status.build.jenkins.jenkins-build-status
  (:require [budi.util.wallet-utils :refer [get-wallet]]
            [budi.util.http-utils :refer [http-get]]
            [taoensso.timbre :as timbre]))

(def database {:wallet-items [ {:wallet-type :jenkins-credentials, :server-url "https://drpennywise.jenkins.rewe-digital.com" :_id "https://drpennywise.jenkins.rewe-digital.com"} ]})

(defn transform-livecycle-state [jenkins-livecycle-state-value]
  (if (= true jenkins-livecycle-state-value)
    :running
    (if (= false jenkins-livecycle-state-value)
      :finished
      :queued)))

(defn transform-state [jenkins-state-value]
  (if jenkins-state-value
    (if (.contains (.toUpperCase jenkins-state-value) "SUCCESS")
      :success
      :failed)
    :failed))

(defn get-job-data
  [id job]
  {:id id
   :type :jenkins-build-job
   :state (transform-state (:result job))
   :lifeCycleState (transform-livecycle-state (:building job))
   :build-completed-at (:timestamp job)
   :build-by "Build by unknown"
   :name (:fullDisplayName job)})

(defn get-jenkins-build-status [id database]
  (let [{:keys [server-url]} (get-wallet database :jenkins-credentials)
        build-id-for-query (clojure.string/replace id #"_job_" "/job/")
        build-result (http-get (str server-url "/job/"build-id-for-query"/lastBuild/api/json?tree=result,timestamp,result,building,fullDisplayName,id"))]
    (get-job-data id (:body build-result))))

(defn get-build-status [{:keys [id
                                type
                                name]}
                        database]
  (try
    (get-jenkins-build-status id database)
    (catch Exception ex
      (do
        (timbre/error "Could not query ("name") for status. Error: " ex)
        {:id id
         :type type
         :state :unknown
         :lifeCycleState :unknown
         :build-completed-at (.getTime (new java.util.Date))
         :build-by "Build by unknown"
         :name name}))))
