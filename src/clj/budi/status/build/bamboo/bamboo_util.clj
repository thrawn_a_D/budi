(ns budi.status.build.bamboo.bamboo-util)

(defn transform-state [bamboo-state-value]
  (if bamboo-state-value
    (if (.contains (.toUpperCase bamboo-state-value) "SUCCESS")
      :success
      :failed)
    :failed))

(defn transform-livecycle-state [bamboo-livecycle-state-value]
  (if bamboo-livecycle-state-value
    (if (.contains (.toUpperCase bamboo-livecycle-state-value) "FINISHED")
     :finished
     (if (.contains (.toUpperCase bamboo-livecycle-state-value) "PROGRESS")
       :running
       (if (or (.contains (.toUpperCase bamboo-livecycle-state-value) "QUEUED")
               (.contains (.toUpperCase bamboo-livecycle-state-value) "PENDING"))
         :queued
         :unknown)))
    :unknown))
