(ns budi.status.build.bamboo.bamboo-build-status
  (:require [budi.status.build.bamboo.bamboo-build-job-handler :refer [get-bamboo-build-status]]
            [budi.status.build.bamboo.bamboo-deploy-job-handler :refer [get-bamboo-deploy-status]]
            [taoensso.timbre :as timbre]))


(defmulti get-bamboo-status (fn [type id database] type))

(defmethod get-bamboo-status :bamboo-build-job [type id database]
  (get-bamboo-build-status id database))

(defmethod get-bamboo-status :bamboo-deploy-job [type id database]
  (get-bamboo-deploy-status id database))

(defn get-build-status [{:keys [type
                                id
                                name]
                         :as source}
                        database]
  (try
    (assoc (get-bamboo-status type id database)
           :name name
           :id id)
    (catch Exception ex
      (do
        (timbre/error "Could not query ("name") for status. Error: " ex)
        {:id id
         :type type
         :state :unknown
         :lifeCycleState :unknown
         :build-completed-at (.getTime (new java.util.Date))
         :build-by "Build by unknown"
         :name name}))))
