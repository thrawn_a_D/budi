(ns budi.status.build.bamboo.bamboo-deploy-job-handler
  (:require [budi.util.http-utils :refer [http-get]]
            [budi.util.wallet-utils :refer [get-wallet]]
            [budi.status.build.bamboo.bamboo-util :refer [transform-state transform-livecycle-state]]))

(defn get-deplay-job-completed-time [completed-time]
  (try
    (.getTime (new java.util.Date completed-time))
    (catch Exception ex
      (.getTime (new java.util.Date)))))

(defn transform-bamboo-deploy-job-data [status]
  {:type :bamboo-deploy-job
   :state (transform-state (:deploymentState status))
   :build-by (:reasonSummary status)
   :build-completed-at (get-deplay-job-completed-time (:finishedDate status))
   :lifeCycleState (transform-livecycle-state (:lifeCycleState status))})

(defn get-bamboo-deploy-status [id database]
  (let [{:keys [server-url username password]} (get-wallet database :bamboo-credentials)
        bamboo-response (http-get (str server-url "/rest/api/latest/deploy/environment/"id"/results?os_authType=basic")
                                  username
                                  password)]
    (-> bamboo-response
        :body
        :results
        first
        transform-bamboo-deploy-job-data)))

