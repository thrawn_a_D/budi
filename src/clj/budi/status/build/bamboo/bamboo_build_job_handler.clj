(ns budi.status.build.bamboo.bamboo-build-job-handler
  (:require [budi.util.http-utils :refer [http-get]]
            [budi.util.wallet-utils :refer [get-wallet]]
            [budi.status.build.bamboo.bamboo-util :refer [transform-state
                                                          transform-livecycle-state]]))

(def build-job-completed-time-formatter (new java.text.SimpleDateFormat "yyyy-MM-dd'T'HH:mm:ss"))

(defn get-build-job-completed-time [completed-time]
  (try
    (.getTime (.parse build-job-completed-time-formatter completed-time))
    (catch Exception ex
      (.getTime (new java.util.Date)))))

(defn transform-bamboo-build-job-data [status]
  {:type :bamboo-build-job
   :state (transform-state (:state status))
   :build-by (if (:buildReason status) (:buildReason status) "Unknown")
   :build-completed-at (get-build-job-completed-time (:buildCompletedTime status))
   :lifeCycleState (transform-livecycle-state (:lifeCycleState status))})

(defn get-bamboo-build-status [id database]
  (let [{:keys [server-url username password]} (get-wallet database :bamboo-credentials)
        bamboo-response (http-get (str server-url "/rest/api/latest/result/"id"-latest?os_authType=basic&max-results=1")
                                  username
                                  password)]
    (-> bamboo-response
        :body
        transform-bamboo-build-job-data)))
