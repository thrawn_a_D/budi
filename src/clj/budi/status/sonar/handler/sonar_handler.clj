(ns budi.status.sonar.handler.sonar-handler
  (:require [budi.util.http-utils :refer [http-get]]
            [budi.util.wallet-utils :refer [get-wallet]]
            [taoensso.timbre :as timbre]))

(defn get-metric [metrics-key metrics-list]
  (:val (first (filter #(= (name metrics-key) (:key %)) metrics-list))))

(defn transform-sonar-build-job-data [{:keys [key msr version] :as status}]
  {:type :sonar-job
   :resource key
   :version version
   :metrics {:test-coverage (get-metric :coverage msr)
             :violations {:blocker (get-metric :blocker_violations msr)
                          :criticle (get-metric :critical_violations msr)
                          :major (get-metric :major_violations msr)
                          :minor (get-metric :minor_violations msr)
                          :info (get-metric :info_violations msr)}}})

(defn get-sonar-status [{:keys [group-id artefact-id]} database]
  (let [{:keys [server-url username password]} (get-wallet database :sonar-credentials)]
    (try
      (let [sonar-response (http-get (str server-url "/api/resources/index?resource="group-id":"artefact-id"&metrics=critical_violations,blocker_violations,major_violations,minor_violations,info_violations,coverage,tests"))]
        (-> sonar-response
            :body
            first
            transform-sonar-build-job-data))
      (catch Exception ex
        (do
          (timbre/error "Error while getting response from : " server-url " : " (.getMessage ex))
          {:type :sonar-job
           :resource (str group-id":"artefact-id)
           :version "unknown"
           :metrics {:test-coverage 0
                     :violations {:blocker 0
                                  :criticle 0
                                  :major 0
                                  :minor 0
                                  :info 0}}})))))
