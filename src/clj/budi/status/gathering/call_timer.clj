(ns budi.status.gathering.call-timer
  (:require [clojure.core.async :refer [go >!]]
            [clj-time.local :as l]
            [clj-time.core :as t]
            [taoensso.timbre :as timbre]))

(def fib-seq
  ((fn rfib [a b]
     (lazy-seq (cons a (rfib b (+ a b)))))
   0 1))

(def fib-values (take 18 fib-seq))

(defn- next-val [value-seq value]
  (let [index-of-val (.indexOf value-seq value)
        complete-index (- (count fib-values) 1)]
    (if (> complete-index index-of-val)
      (nth value-seq (+ index-of-val 1))
      (nth value-seq index-of-val))))

(defn get-enriched-call-result [current-call-result
                                execution-time
                                estimated-next-execution-time]
  (assoc current-call-result
         :execution-time execution-time
         :estimated-next-execution-time estimated-next-execution-time))

(defn status-changed? [{:keys [new-call-result last-call-result]}]
  (let [last-cr-without-time (dissoc last-call-result :execution-time :estimated-next-execution-time)
        new-cr-without-time (dissoc new-call-result :execution-time :estimated-next-execution-time)]
    (not (= last-cr-without-time new-cr-without-time))))

(defn get-next-time-intervall [current-time-intervall
                               initial-intervall-in-sec
                               status-map]
  (if (not (status-changed? status-map))
    (next-val fib-values current-time-intervall)
    initial-intervall-in-sec))

(defn get-execution-time []
  (l/format-local-time (l/local-now) :date-time-no-ms))

(defn get-next-execution-time [wait-timespan]
  (l/format-local-time (t/plus (l/local-now) (t/seconds wait-timespan)) :date-time-no-ms))

(defn get-next-execution-duration [wait-timespan initial-intervall-in-sec status-map]
  (if (status-changed? status-map) initial-intervall-in-sec wait-timespan))

(defn- execute-fn [wait-timespan
                   initial-intervall-in-sec
                   call-fn
                   status-atom]
  (try
    (let [call-result (call-fn)]
      (swap! status-atom assoc :last-call-result (:new-call-result @status-atom))
      (swap! status-atom assoc :new-call-result (get-enriched-call-result call-result
                                                                          (get-execution-time)
                                                                          (get-next-execution-time (get-next-execution-duration wait-timespan initial-intervall-in-sec @status-atom)))))
    (catch Exception ex
      (timbre/error "Error while executing a function: " (.getMessage ex))
      (swap! status-atom assoc :last-call-result (:new-call-result @status-atom))
      (swap! status-atom assoc :new-call-result (get-enriched-call-result (:last-call-result @status-atom)
                                                                          (get-execution-time)
                                                                          (get-next-execution-time (get-next-execution-duration wait-timespan initial-intervall-in-sec @status-atom))))))
  status-atom)

(defn continuously-execute [call-fn status-result-chan initial-intervall-in-sec]
  (let [p (promise)]
    (future (loop [response-result (atom {:last-call-result {}
                                          :new-call-result {}})
                   sec initial-intervall-in-sec]
              (when (and (= (deref p 0 ::timeout) ::timeout)
                         (not (= (deref p 0 ::timeout) ::stop)))
                (do
                  (let [status-atom (execute-fn sec initial-intervall-in-sec call-fn response-result)]
                    (go (>! status-result-chan (:new-call-result @status-atom)))
                    (Thread/sleep (* (get-next-execution-duration sec initial-intervall-in-sec @status-atom) 1000))
                    (recur status-atom
                           (get-next-time-intervall sec
                                                    initial-intervall-in-sec
                                                    @status-atom)))))))
    #(deliver p ::stop)))

