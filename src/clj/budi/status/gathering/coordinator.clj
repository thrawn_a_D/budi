(ns budi.status.gathering.coordinator
  (:require [budi.status.gathering.call-timer :refer [continuously-execute]]
            [budi.status.gathering.registrator :refer [get-status]]
            [budi.dao.generic-crud :refer [get-data
                                           delete-identifiable-data
                                           store-identifiable-data]]))

(defn get-call-fn [{:keys [type] :as source} database]
  (fn []
    (get-status type source database)))

(defn start-job-execution [database
                           identifier-key
                           item
                           status-result-chan
                           initial-refresh-timespan]
  (let [job-key (get item identifier-key)]
    {:id job-key
     :stop-fn (continuously-execute (get-call-fn item database)
                                    status-result-chan
                                    initial-refresh-timespan)}))

(defn start-jobs-execution [database
                            identifier-key
                            job-type
                            status-result-chan
                            initial-refresh-timespan]
  (let [jobs-to-start (get-data database job-type)]
    (doall
      (for [item jobs-to-start]
        (start-job-execution database
                             identifier-key
                             item
                             status-result-chan
                             initial-refresh-timespan)))))

(defn get-job [jobs-in-execution job-identifier]
  (first (filter #(= (:id %) job-identifier) @jobs-in-execution)))

(defn stop-job-execution
  ([job-in-execution]
   ((:stop-fn job-in-execution)))
  ([jobs-in-execution job-identifier]
   (if-let [job-to-stop (get-job jobs-in-execution job-identifier)]
     (do
       (stop-job-execution job-to-stop)
       (swap! jobs-in-execution #(remove (fn [job](= job job-to-stop)) %))))))

(defn restart-job-execution
  [database
   identifier-key
   status-key
   status-result-chan
   job-data
   status-retreiver]
  (let [key (get job-data identifier-key)
        initial-refresh-timespan (:initial-refresh-timespan (:status-retreiver status-retreiver))
        jobs-in-execution (:status-refresh-schedule (:status-retreiver status-retreiver))]
        (as-> database b
            (start-job-execution b :url job-data status-result-chan initial-refresh-timespan)
            (swap! jobs-in-execution conj b))
        (stop-job-execution jobs-in-execution key)
        (delete-identifiable-data database status-key key)))

(defn stop-jobs-execution [jobs-in-execution]
  (doseq [job @jobs-in-execution]
    (stop-job-execution job))
  (reset! jobs-in-execution []))

