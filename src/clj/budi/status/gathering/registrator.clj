(ns budi.status.gathering.registrator
  (:require [budi.status.build.bamboo.bamboo-build-status :refer [get-build-status]]
            [budi.status.service.service-status :refer [get-service-status]]
            [budi.status.build.jenkins.jenkins-build-status :as jenkins]
            [budi.status.sonar.handler.sonar-handler :refer [get-sonar-status]]))

(defmulti get-status (fn [type item database] type))

(defmethod get-status :bamboo-deploy-job [type item database]
  (get-build-status item database))

(defmethod get-status :bamboo-build-job [type item database]
  (get-build-status item database))

(defmethod get-status :jenkins-build-job [type item database]
  (jenkins/get-build-status item database))

(defmethod get-status :http-service-job [type item database]
  (get-service-status item))

(defmethod get-status :sonar-job [type item database]
  (get-sonar-status item database))
