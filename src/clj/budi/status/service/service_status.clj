(ns budi.status.service.service-status
  (:require [budi.util.http-utils :refer [http-get]]
            [taoensso.timbre :as timbre]))

(defn get-service-status [{:keys [name url] :as source}]
  (try
    (let [service-response (http-get url)]
      {:type :http-service-job
       :name name
       :url url
       :service-alive?     true
       :http-response-code 200})
    (catch Exception ex
      (do
       (timbre/error "Error while getting response from : " url " : " (.getMessage ex))
       {:type :http-service-job
        :name name
        :url url
        :service-alive?     false
        :http-response-code 0}))))
