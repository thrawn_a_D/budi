(ns budi.schemas.config.build
  (:require [schema.core :as s]))

(s/defschema BuildConfig
             {:type (s/enum :bamboo-build-job :bamboo-deploy-job :jenkins-build-job)
              :id s/Str
              :name s/Str})
