(ns budi.schemas.config.http-service
  (:require [schema.core :as s]))

(s/defschema HttpService
             {:type (s/enum :http-service-job)
              :url s/Str
              :name s/Str})
