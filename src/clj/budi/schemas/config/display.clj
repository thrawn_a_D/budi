(ns budi.schemas.config.display
  (:require [schema.core :as s]))

(s/defschema NewestBuildStatus
             {:size (s/enum :full)
              :base {:width s/Str
                     :height s/Str
                     :cursor s/Str}
              :texts {:status-header-text {:margin s/Str
                                           :font-family s/Str
                                           :font-size s/Str
                                           :font-weight s/Str
                                           :letter-spacing s/Str
                                           :line-height s/Str
                                           :text-align s/Str}
                      :build-completed-at {:font-family s/Str
                                           :font-weight s/Str
                                           :font-size s/Str
                                           :line-height s/Str
                                           :margin s/Str
                                           :text-align s/Str}
                      :build-by-text {:font-family s/Str
                                      :font-weight s/Str
                                      :line-height s/Str
                                      :font-size s/Str
                                      :text-align s/Str}
                      :time-badge {:font-family s/Str
                                   :font-weight s/Str
                                   :display s/Str
                                   :text-align s/Str}
                      :shortened-text {:white-space s/Str
                                       :overflow s/Str
                                       :text-overflow s/Str
                                       :width s/Str}}})

(s/defschema RecentBuildStatus
             {:size (s/enum :half)
              :base {:width s/Str
                     :height s/Str
                     :cursor s/Str}
              :texts {:status-header-text {:margin s/Str
                                           :font-family s/Str
                                           :font-size s/Str
                                           :font-weight s/Str
                                           :letter-spacing s/Str
                                           :line-height s/Str
                                           :text-align s/Str}
                      :build-completed-at {:font-family s/Str
                                           :font-weight s/Str
                                           :font-size s/Str
                                           :line-height s/Str
                                           :margin s/Str
                                           :text-align s/Str}}})

(s/defschema OlderBuildStatus
             {:size (s/enum :third)
              :base {:width s/Str
                     :height s/Str
                     :cursor s/Str}
              :texts {:status-header-text {:margin s/Str
                                           :font-family s/Str
                                           :font-size s/Str
                                           :font-weight s/Str
                                           :letter-spacing s/Str
                                           :line-height s/Str
                                           :text-align s/Str}}})

(s/defschema BuildTilesDisplayConfig
             {:newest-build-status NewestBuildStatus
              :recent-build-status RecentBuildStatus
              :older-build-status  OlderBuildStatus})
