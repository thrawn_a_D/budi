(ns budi.schemas.http-schema
  "Schemas and constructors that are used in http context"
  (:require
    [schema.core :as s]))

(s/defschema Ack
             "Simple acknowledgement for successful requests"
             {:message (s/eq "OK")})

(def ack {:message "OK"})

(s/defschema Precondition-Failed
             "A precondition for a particular call has failed"
             {:message (s/eq "Precondition failed!")})

(def precondition-failed
  {:status 412
   :body {:message "Precondition failed!"}})

(s/defschema Resource-Conflict
             "A resource conflict occurred"
             {:message (s/eq "Resource conflict!")})

(def resource-conflict
  {:status 409
   :body {:message "Resource conflict!"}})

(s/defschema Missing
             {:message String})

(s/defn not-found
        [message :- String]
        {:message message})
