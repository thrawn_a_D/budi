(ns budi.schemas.common-schema
  "Schemas and constructors that are common to all schemas"
  (:require
   [ring.swagger.json-schema :as jsons]))

(defmethod jsons/json-type schema.core.Predicate [e] (if-let [c (jsons/predicate-to-class (:p? e))]
                                                       (jsons/->json c)
                                                       {:type "string"}))
