(ns budi.configuration.display.display-config-provider
  (:require [budi.dao.generic-crud :refer [get-data]]
            [budi.schemas.config.display :refer [BuildTilesDisplayConfig
                                                 NewestBuildStatus
                                                 RecentBuildStatus
                                                 OlderBuildStatus]]
            [budi.schemas.budi :refer [build-status]]
            [schema.core :as s]
            [taoensso.timbre :as timbre]))


(def default-display-config {:newest-build-status {:size :full
                                                   :base {:width "300px"
                                                          :height "300px"
                                                          :cursor "pointer"}
                                                   :texts {:status-header-text {:margin "10px auto;"
                                                                                :font-family "Arial"
                                                                                :font-size "30px;"
                                                                                :font-weight "bold;"
                                                                                :letter-spacing "-1px;"
                                                                                :line-height "1;"
                                                                                :text-align "center;"}
                                                           :build-completed-at {:font-family "'Open Sans', sans-serif;"
                                                                                :font-weight "300;"
                                                                                :font-size "24px;"
                                                                                :line-height "32px;"
                                                                                :margin "0 0 12px;"
                                                                                :text-align "center;"}
                                                           :build-by-text {:font-family "'Open Sans', sans-serif;"
                                                                           :font-weight "300;"
                                                                           :line-height "32px;"
                                                                           :font-size "20px;"
                                                                           :text-align "center;"}
                                                           :time-badge {:font-family "'Open Sans', sans-serif;"
                                                                        :font-weight "300;"
                                                                        :display "block"
                                                                        :text-align "center;"}
                                                           :shortened-text {:white-space "nowrap !important;"
                                                                            :overflow "hidden !important;"
                                                                            :text-overflow "ellipsis !important;"
                                                                            :width "250px;"}}}
                             :recent-build-status {:size :half
                                                   :base {:width "300px"
                                                          :height "145px"
                                                          :cursor "pointer"}
                                                   :texts {:status-header-text {:margin "10px auto;"
                                                                                :font-family "Arial"
                                                                                :font-size "24px;"
                                                                                :font-weight "bold;"
                                                                                :letter-spacing "-1px;"
                                                                                :line-height "1;"
                                                                                :text-align "center;"}
                                                           :build-completed-at {:font-family "'Open Sans', sans-serif;"
                                                                                :font-weight "300;"
                                                                                :font-size "24px;"
                                                                                :line-height "32px;"
                                                                                :margin "0 0 12px;"
                                                                                :text-align "center;"}}}
                             :older-build-status {:size :third
                                                  :base {:width "145px"
                                                         :height "145px"
                                                         :cursor "pointer"}
                                                  :texts {:status-header-text {:margin "10px auto;"
                                                                               :font-family "Arial"
                                                                               :font-size "16px;"
                                                                               :font-weight "bold;"
                                                                               :letter-spacing "-1px;"
                                                                               :line-height "1;"
                                                                               :text-align "center;"}}}})

(defn categorize-data [build-data]
  (->> build-data
       (map-indexed (fn [idx item]
                      (if (< idx 6)
                        (assoc-in item [:display-config :size] :full)
                        (if (< idx 15)
                          (assoc-in item [:display-config :size] :half)
                          (assoc-in item [:display-config :size] :third)))))))

(s/defn get-display-config-by-size :- (s/enum NewestBuildStatus
                                              RecentBuildStatus
                                              OlderBuildStatus)
        [size :- (s/enum :full :half :third)
         display-config :- BuildTilesDisplayConfig]
        (cond
          (= :full size) (:newest-build-status display-config)
          (= :half size) (:recent-build-status display-config)
          (= :third size) (:older-build-status display-config)))

(s/defn categorize-build-data :- [build-status]
        [build-jobs :- [build-status]]
        (->> build-jobs
             (sort-by :build-completed-at)
             (sort-by :state)
             categorize-data))

(s/defn get-display-config-data :- BuildTilesDisplayConfig
        [database]
        (if-let [saved-display-config (first (get-data database :display-config))]
          saved-display-config
          default-display-config))

(s/defn merge-display-config-with-build-config :- [build-status]
        [database
         build-jobs :- [build-status]]
        (let [display-config (get-display-config-data database)
              categorized-build-data (categorize-build-data build-jobs)]
          (map #(assoc % :display-config (get-display-config-by-size (:size (:display-config %)) display-config))
               categorized-build-data)))
