(ns budi.configuration.loader
  (:require [clojure.edn :as edn]
            [taoensso.timbre :as timbre]))

(defn ^:private deep-merge
  "Deep merge two maps"
  [& values]
  (if (every? map? values)
    (apply merge-with deep-merge values)
    (last values)))

(defn read-file [file]
  (try
    (-> file
        slurp
        edn/read-string)
    (catch java.io.IOException ex
      (timbre/error "Error while reading config file " file)
      {})))

(defn load-config
  ([internal-config external-config]
   (reduce deep-merge (map read-file
                           [internal-config external-config])))
  ([external-config-path]
   (load-config (clojure.java.io/resource "internal-backend-config.edn") external-config-path)))
