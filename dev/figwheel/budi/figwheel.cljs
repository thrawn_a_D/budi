(ns figwheel.summary.figwheel
    (:require [figwheel.client :as fw :include-macros true]
              [om.core :as om :include-macros  true]
              [budi.common.test-data :refer [user-profile]]
              [budi.view.configuration.display.display-config-view :refer [display-config-view]]))

(def app-data (atom  {:display-conf {:status-elements {:newest-build-status {:width 300
                                                                             :height 300
                                                                             :font "Arial"
                                                                             :font-size "14"
                                                                             :max-text-length "200"}
                                                       :recent-build-status {:width 300
                                                                             :height 300
                                                                             :font "Arial"
                                                                             :font-size "14"
                                                                             :max-text-length "200"}
                                                       :older-build-status {:width 300
                                                                            :height 300
                                                                            :font "Arial"
                                                                            :font-size "14"
                                                                            :max-text-length "200"}}}}))

(fw/watch-and-reload :jsload-callback (fn []
                                        ;; you would add this if you
                                        ;; have more than one file
                                        (om/root display-config-view app-data {:target (. js/document (getElementById "display_config_view"))})))

