#!/bin/bash

LOG_FILE_PATH=./budi.log
JAVA=$JAVA_HOME/bin/java
BUDI_CONF=./budi-conf.edn

msg() {
     echo "##> $1"
}

checkJAVA() {
     if [ ! -f $JAVA ]
     then
          msg "Java is not installed or JAVA_HOME is not set properly."
     fi
}

checkJAVA
msg "Starting budi using configuration: $BUDI_CONF"
msg "Check log file for further details: $LOG_FILE_PATH"
$JAVA -jar budi-main.jar --config-path $BUDI_CONF > $LOG_FILE_PATH &
