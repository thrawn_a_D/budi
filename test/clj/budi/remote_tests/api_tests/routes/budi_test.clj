(ns budi.remote-tests.api-tests.routes.budi-test
  (:use plumbing.core
        midje.sweet
        budi.core)
  (:require
    [budi.remote-tests.common.server-utils :refer [start-server
                                         stop-server]]
    [budi.remote-tests.common.test-configuration :refer [get-database-object]]
    [budi.remote-tests.api-tests.common.http-utils :as utils]))


#_(against-background [(before :contents (start-server) :after (stop-server))]
                    (fact-group "Test budi access api"
                                (let [username (:username al-entry)
                                      db (get-database-object)]
                                  (try
                                    (fact :api-test "Show budi text of a user"
                                          (crud/store-user-data db username :profile al-entry)
                                          (crud/store-user-data db username :skills [skill-group-entry])
                                          (let [budi-text (:body (utils/http-get "api/budi" (str "?username=" username)))]
                                            budi-text => (contains "has gained knowledge")))

                                    (fact :api-test "No user found for a budi"
                                          (let [not-existing-user "not-existant"
                                                {:keys [status body]} (utils/http-get "api/budi" (str "?username=" not-existing-user))]
                                            status => 404))))))
