(ns budi.remote-tests.api-tests.routes.health-test
  (:use plumbing.core
        midje.sweet
        budi.core)
  (:require
   [budi.remote-tests.api-tests.common.http-utils :as utils]
   [budi.routes.health :refer [Health-Response]]
   [schema.core :as s]
   [budi.remote-tests.common.server-utils :refer [start-server
                                                     stop-server]]
   [budi.schemas.http-schema :as http-schema]))

#_(against-background [(before :contents (start-server) :after (stop-server))]
                    (fact-group "Receive health status from server"
                                  (fact :api-test "Get current health status"
                                    (let [health-status (:body (utils/http-get-coerce "api/health" Health-Response))]
                                      health-status => (contains {:healthy ["Everything is fine"]})))))
