(ns budi.remote-tests.api-tests.routes.config.display.display-config-test
  (:use midje.sweet)
  (:require
    [budi.dao.generic-crud :refer [get-data
                                   delete-data
                                   store-identifiable-data]]
    [budi.remote-tests.api-tests.common.http-utils :as utils]
    [budi.configuration.display.display-config-provider :refer [default-display-config]]
    [budi.remote-tests.common.server-utils :refer [start-server
                                                   stop-server]]
    [budi.schemas.config.display :refer [BuildTilesDisplayConfig]]
    [budi.schemas.http-schema :as http-schema]))

(against-background [(before :contents (start-server) :after (stop-server))
                     (before :facts (delete-data (:database (:database budi.components.budi-system/system)) :display-config))]
                    (fact-group "Test budi display configuration"
                                (fact :api-test "Create a display config and get the result"
                                      (let [path  "/api/config/display/build-tiles"
                                            post-response (utils/http-post-coerce path default-display-config BuildTilesDisplayConfig)
                                            get-response (utils/http-get-coerce path BuildTilesDisplayConfig)
                                            post-response-status (:status post-response)
                                            post-response-body (:body post-response)
                                            get-response-status (:status get-response)
                                            get-response-body (:body get-response)]
                                        post-response-status => 200
                                        get-response-status => 200
                                        post-response-body => default-display-config
                                        get-response-body => default-display-config))
                                (fact :api-test "Update an existing sonar project"
                                      (let [new-name "new-sonar-project"
                                            path "/api/config/display/build-tiles"
                                            modified-test-data (assoc-in default-display-config [:newest-build-status :base :width] "10px")]
                                        (:body (utils/http-put-coerce path modified-test-data BuildTilesDisplayConfig)) => modified-test-data
                                        (:status (utils/http-put-coerce path modified-test-data BuildTilesDisplayConfig)) => 200
                                        (:body (utils/http-get-coerce path BuildTilesDisplayConfig)) => modified-test-data
                                        ))
                                (fact :api-test "Getting a config without ever creating one, provides a default"
                                      (let [path "/api/config/display/build-tiles"]
                                        (:body (utils/http-get-coerce path BuildTilesDisplayConfig)) => default-display-config))
                                ))
