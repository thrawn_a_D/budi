(ns budi.remote-tests.api-tests.routes.config.sonar-test
  (:use midje.sweet)
  (:require
    [budi.dao.generic-crud :refer [get-data
                                   delete-identifiable-data
                                   store-identifiable-data]]
    [budi.remote-tests.api-tests.common.http-utils :as utils]
    [budi.status.gathering.coordinator :refer [restart-job-execution
                                               stop-job-execution]]
    [budi.remote-tests.common.server-utils :refer [start-server
                                                   stop-server]]
    [budi.schemas.config.sonar :refer [SonarConfig]]
    [budi.remote-tests.api-tests.common.job-utils :refer [get-job-status-data
                                                          store-data]]
    [budi.schemas.http-schema :as http-schema]))

(def test-service-data {:type :sonar-job
                        :name "test-project"
                        :group-id "my-gr"
                        :artefact-id "my-art-id"})

(against-background [(before :contents (start-server) :after (stop-server))]
                    (fact-group "Test budi sonar job configuration"
                                (fact :api-test "Create a sonar job entry and get the result"
                                      (let [path (str "/api/config/sonar/" (:group-id test-service-data) "/" (:artefact-id test-service-data))
                                            post-response (utils/http-post-coerce path test-service-data SonarConfig)
                                            get-response (utils/http-get-coerce "/api/config/sonar" [SonarConfig])
                                            post-response-status (:status post-response)
                                            post-response-body (:body post-response)
                                            get-response-status (:status get-response)
                                            get-response-body (:body get-response)]
                                        post-response-status => 200
                                        get-response-status => 200
                                        post-response-body => test-service-data
                                        get-response-body => [test-service-data]))
                                (fact :api-test "Update an existing sonar project"
                                      (let [new-name "new-sonar-project"
                                            path (str "/api/config/sonar/" (:group-id test-service-data) "/" (:artefact-id test-service-data))
                                            modified-test-data (assoc test-service-data :name new-name)]
                                        (:body (utils/http-put-coerce path modified-test-data SonarConfig)) => modified-test-data
                                        (provided
                                          (restart-job-execution irrelevant :artefact-id :sonar-status irrelevant modified-test-data irrelevant) => irrelevant)
                                        (:status (utils/http-put-coerce path modified-test-data SonarConfig)) => 200
                                        (:body (utils/http-get-coerce "/api/config/sonar" [SonarConfig])) => [modified-test-data]
                                        ))
                                (fact :api-test "Delete an existing sonar project"
                                      (let [group-id (:group-id test-service-data)
                                            artefact-id (:artefact-id test-service-data)
                                            complete-identifier (str group-id ":" artefact-id)
                                            path (str "api/config/sonar/" group-id "/" artefact-id)]
                                        (:status (utils/http-delete path)) => 200
                                        (provided
                                          (stop-job-execution irrelevant artefact-id) => irrelevant
                                          (delete-identifiable-data irrelevant :sonar-items complete-identifier) => irrelevant
                                          (delete-identifiable-data irrelevant :sonar-status artefact-id) => irrelevant)
                                        ))
                                ))
