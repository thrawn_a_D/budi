(ns budi.remote-tests.api-tests.routes.config.build-test
  (:use midje.sweet)
  (:require
    [budi.dao.generic-crud :refer [get-data
                                   delete-identifiable-data
                                   store-identifiable-data]]
    [budi.remote-tests.api-tests.common.http-utils :as utils]
    [budi.status.gathering.coordinator :refer [restart-job-execution
                                               get-job
                                               stop-job-execution]]
    [budi.remote-tests.common.server-utils :refer [start-server
                                                   stop-server]]
    [budi.schemas.config.build :refer [BuildConfig]]
    [budi.remote-tests.api-tests.common.job-utils :refer [get-job-status-data
                                                          store-data]]
    [budi.schemas.http-schema :as http-schema]))

(def test-service-data {:type :bamboo-build-job
                        :id "908723489ß7"
                        :name "Some name"})

(against-background [(before :contents (start-server) :after (stop-server))]
                    (fact-group "Test budi build job configuration"
                                (fact :api-test "Create a build job entry and get the result"
                                      (let [path (str "/api/config/builds/" (:id test-service-data))
                                            post-response (utils/http-post-coerce path test-service-data BuildConfig)
                                            get-response (utils/http-get-coerce "/api/config/builds/" [BuildConfig])
                                            post-response-status (:status post-response)
                                            post-response-body (:body post-response)
                                            get-response-status (:status get-response)
                                            get-response-body (:body get-response)]
                                        post-response-status => 200
                                        get-response-status => 200
                                        post-response-body => test-service-data
                                        get-response-body => [test-service-data]))
                                (fact :api-test "Update an existing build project"
                                      (let [new-name "new-build-project"
                                            path (str "/api/config/builds/" (:id test-service-data))
                                            modified-test-data (assoc test-service-data :name new-name)]
                                        (:body (utils/http-put-coerce path modified-test-data BuildConfig)) => modified-test-data
                                        (provided
                                          (restart-job-execution irrelevant :id :build-status irrelevant modified-test-data irrelevant) => irrelevant)
                                        (:status (utils/http-put-coerce path modified-test-data BuildConfig)) => 200
                                        (:body (utils/http-get-coerce "/api/config/builds" [BuildConfig])) => [modified-test-data]
                                        ))
                                (fact :api-test "Delete an existing build item"
                                      (let [id (:id test-service-data)
                                            path (str "/api/config/builds/" id)]
                                        (:status (utils/http-delete path)) => 200
                                        (provided
                                          (stop-job-execution irrelevant id) => irrelevant
                                          (delete-identifiable-data irrelevant :build-status id) => irrelevant
                                          (delete-identifiable-data irrelevant :build-items id) => irrelevant)
                                        ))
                                ))
