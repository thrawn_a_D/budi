(ns budi.remote-tests.api-tests.routes.config.http-service-test
  (:use midje.sweet)
  (:require
    [budi.dao.generic-crud :refer [get-data
                                   delete-identifiable-data
                                   store-identifiable-data]]
    [budi.remote-tests.api-tests.common.http-utils :as utils]
    [budi.status.gathering.coordinator :refer [restart-job-execution
                                               stop-job-execution]]
    [budi.remote-tests.common.server-utils :refer [start-server
                                                   stop-server]]
    [budi.schemas.config.http-service :refer [HttpService]]
    [budi.schemas.http-schema :as http-schema]))

(def test-service-data {:type :http-service-job
                        :url "http://google.de"
                        :name "GOOGLE"})

(against-background [(before :contents (start-server) :after (stop-server))]
                    (fact-group "Test budi http job configuration"
                                (fact :api-test "Create an http job entry and get the result"
                                      (let [path (str "/api/config/http-services/" (utils/encode-base64 "http://google.de"))
                                            post-response (utils/http-post-coerce path test-service-data HttpService)
                                            get-response (utils/http-get-coerce "/api/config/http-services/" [HttpService])
                                            post-response-status (:status post-response)
                                            post-response-body (:body post-response)
                                            get-response-status (:status get-response)
                                            get-response-body (:body get-response)]
                                        post-response-status => 200
                                        get-response-status => 200
                                        post-response-body => test-service-data
                                        get-response-body => [test-service-data]))
                                (fact :api-test "Update an existing job"
                                      (let [service-url "http://google.de"
                                            path (str "/api/config/http-services/" (utils/encode-base64 service-url))
                                            modified-test-data (assoc test-service-data :name "giigle")]
                                        (:body (utils/http-put-coerce path modified-test-data HttpService)) => modified-test-data
                                        (provided
                                          (restart-job-execution irrelevant :url :service-status irrelevant modified-test-data irrelevant) => irrelevant)
                                        (:status (utils/http-put-coerce path modified-test-data HttpService)) => 200
                                        (:body (utils/http-get-coerce "/api/config/http-services/" [HttpService])) => [modified-test-data]
                                        ))
                                (fact :api-test "Delete an existing http job"
                                      (let [url (utils/encode-base64 (:url test-service-data))
                                            path (str "/api/config/http-services/" url)]
                                        (:status (utils/http-delete path)) => 200
                                        (provided
                                          (stop-job-execution irrelevant url) => irrelevant
                                          (delete-identifiable-data irrelevant :http-service-items url) => irrelevant
                                          (delete-identifiable-data irrelevant :service-status url) => irrelevant)
                                        ))
                                ))
