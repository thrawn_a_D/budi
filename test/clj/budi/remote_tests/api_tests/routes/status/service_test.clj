(ns budi.remote-tests.api-tests.routes.status.service-test
  (:use midje.sweet)
  (:require
    [budi.dao.generic-crud :refer [get-data
                                   delete-identifiable-data
                                   store-identifiable-data]]
    [budi.remote-tests.api-tests.common.http-utils :as utils]
    [budi.remote-tests.common.server-utils :refer [start-server
                                                   stop-server]]
    [budi.schemas.budi :refer [service-status
                               service-status]]
    [budi.remote-tests.api-tests.common.job-utils :refer [get-job-status-data
                                                          store-data]]
    [budi.schemas.http-schema :as http-schema]))

(def test-service-data {:type :http-service-job
                        :url "http://google.de"
                        :service-alive? true
                        :http-response-code 200
                        :name "my-job"
                        :execution-time "23.11.2012-13:22"
                        :estimated-next-execution-time  "23.11.2012-33:22"})

(against-background [(before :contents (start-server) :after (stop-server))]
                    (fact-group "Test service status receivement"
                                (fact :api-test "Get service status of specific service jobs"
                                      (let [url (:url test-service-data)
                                            path "/api/status/http-services"]
                                        (store-data test-service-data url :service-status)
                                        (:status (utils/http-get-coerce path [service-status])) => 200
                                        (:body (utils/http-get-coerce path [service-status])) => (contains [test-service-data] :in-any-order)))
                                (fact :api-test "Get service status of a particular service job"
                                      (let [encoded-url (utils/encode-base64 (:url test-service-data))]
                                        (store-data test-service-data encoded-url :service-status)
                                        (let [path (str "/api/status/http-services/" encoded-url)]
                                          (:status (utils/http-get-coerce path service-status)) => 200
                                          (:body (utils/http-get-coerce path service-status)) => test-service-data)))
                                (fact :api-test "Return a 404 when no service is found"
                                      (let [path "/api/status/http-services/bla"
                                            get-response (utils/http-get-coerce path service-status)
                                            get-response-status (:status get-response)]
                                        get-response-status => 404))
                                (fact :api-test "Delete a service status"
                                      (let [url (:url test-service-data)
                                            encoded-url (utils/encode-base64 url)
                                            path (str "/api/status/http-services/" encoded-url)]
                                        (store-data test-service-data encoded-url :service-status)
                                        (:status (utils/http-delete path)) => 200
                                        (provided
                                          (delete-identifiable-data irrelevant :service-status encoded-url) => irrelevant)))
                                ))
