(ns budi.remote-tests.api-tests.routes.status.sonar-test
  (:use midje.sweet)
  (:require
    [budi.remote-tests.api-tests.common.http-utils :as utils]
    [budi.dao.generic-crud :refer [get-data
                                   delete-identifiable-data
                                   store-identifiable-data]]
    [budi.remote-tests.api-tests.common.http-utils :as utils]
    [budi.remote-tests.common.server-utils :refer [start-server
                                                   stop-server]]
    [budi.schemas.budi :refer [sonar-status]]
    [budi.remote-tests.api-tests.common.job-utils :refer [get-job-status-data
                                                          store-data]]
    [budi.schemas.http-schema :as http-schema]))

(def group-id "bla")
(def artefact-id "blub")

(def test-sonar-data {:type :sonar-job
                      :resource (str group-id ":" artefact-id)
                      :version "1.02"
                      :metrics {:test-coverage 123
                                :violations {:blocker 1
                                             :criticle 2
                                             :major 4
                                             :minor 3
                                             :info 5}}
                      :execution-time "23.11.2012-13:22"
                      :estimated-next-execution-time  "23.11.2012-33:22"})

(against-background [(before :contents (start-server) :after (stop-server))]
                    (fact-group "Test sonar status receivement"
                                (fact :api-test "Get sonar status of specific sonar jobs"
                                      (store-data test-sonar-data (:resource test-sonar-data) :sonar-status)
                                      (let [path "/api/status/sonar"
                                            resource (:resource test-sonar-data)]
                                        (:status (utils/http-get-coerce path [sonar-status])) => 200
                                        (:body (utils/http-get-coerce path [sonar-status])) => (contains [test-sonar-data] :in-any-order)))
                                (fact :api-test "Get sonar status of a particular sonar job"
                                      (store-data test-sonar-data (str group-id ":" artefact-id) :sonar-status)
                                      (let [path (str "/api/status/sonar/" group-id "/" artefact-id)
                                            resource (:resource test-sonar-data)]
                                        (:status (utils/http-get-coerce path sonar-status)) => 200
                                        (:body (utils/http-get-coerce path sonar-status)) => test-sonar-data))
                                (fact :api-test "No item found, return a 404"
                                      (let [path (str "/api/status/sonar/unknown-gr/unknown-art")
                                            get-response (utils/http-get-coerce path sonar-status)
                                            get-response-status (:status get-response)]
                                        get-response-status => 404))
                                (fact :api-test "Delete a sonar status"
                                      (let [resource (:resource test-sonar-data)
                                            path (str "/api/status/sonar/" resource)]
                                        (store-data test-sonar-data resource :sonar-status)
                                        (:status (utils/http-delete path)) => 200
                                        (provided
                                          (delete-identifiable-data irrelevant :sonar-status resource) => irrelevant)))
                                ))
