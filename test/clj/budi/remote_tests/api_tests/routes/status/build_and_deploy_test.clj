(ns budi.remote-tests.api-tests.routes.status.build-and-deploy-test
  (:use midje.sweet)
  (:require
    [budi.dao.generic-crud :refer [get-data
                                   delete-identifiable-data
                                   store-identifiable-data]]
    [budi.remote-tests.api-tests.common.http-utils :as utils]
    [budi.configuration.display.display-config-provider :refer [merge-display-config-with-build-config
                                                                default-display-config]]
    [budi.remote-tests.common.server-utils :refer [start-server
                                                   stop-server]]
    [budi.schemas.budi :refer [build-status
                               service-status]]
    [budi.remote-tests.api-tests.common.job-utils :refer [get-job-status-data
                                                          store-data]]
    [budi.schemas.http-schema :as http-schema]))

(def test-job-data {:type :bamboo-build-job
                    :id "988065878906"
                    :name "my-job"
                    :build-by "My name"
                    :build-completed-at 70897822
                    :state :success
                    :lifeCycleState :running
                    :execution-time "23.11.2012-13:22"
                    :estimated-next-execution-time  "23.11.2012-33:22"})

(against-background [(before :contents (start-server) :after (stop-server))]
                    (fact-group "Test budi build status receivement"
                                (fact :api-test "Get build status of specific build jobs"
                                      (let [path "/api/status/build-and-deploy"
                                            id (:id test-job-data)]
                                        (store-data test-job-data id :build-status)
                                        (:status (utils/http-get-coerce path [build-status])) => 200
                                        (:body (utils/http-get-coerce path [build-status])) => (contains [test-job-data] :in-any-order)))
                                (fact :api-test "Get build status of a particular build job"
                                      (store-data test-job-data (:id test-job-data) :build-status)
                                      (let [id (:id test-job-data)
                                            path (str "/api/status/build-and-deploy/" id)]
                                        (:status (utils/http-get-coerce path build-status)) => 200
                                        (:body (utils/http-get-coerce path build-status)) => test-job-data))
                                (fact :api-test "Return a 404 when no build/deploy job is found"
                                      (let [path "/api/status/build-and-deploy/bla"
                                            get-response (utils/http-get-coerce path service-status)
                                            get-response-status (:status get-response)]
                                        get-response-status => 404))
                                (fact :api-test "Delete a build status"
                                      (store-data test-job-data (:id test-job-data) :build-status)
                                      (let [id (:id test-job-data)
                                            path (str "/api/status/build-and-deploy/" id)]
                                        (:status (utils/http-delete path)) => 200
                                        (provided
                                          (delete-identifiable-data irrelevant :build-status id) => irrelevant)))
                                (fact :api-test "Build status contains display config data"
                                      (store-data test-job-data (:id test-job-data) :build-status)
                                      (let [id (:id test-job-data)
                                            path "/api/status/build-and-deploy?with-display-config=true"]
                                        (:status (utils/http-get-coerce path [build-status])) => 200
                                        (:body (utils/http-get-coerce path [build-status])) => (contains [(contains {:display-config irrelevant})])
                                        ))
                                ))
