(ns budi.remote-tests.api-tests.common.job-utils
  (:require [budi.dao.generic-crud :refer [get-data
                                           store-identifiable-data]]))

(defn get-job-status-data
  [job-type id job-identifier-key]
  (let [database (:database (:database budi.components.budi-system/system))
        jobs-statuses (get-data database job-type)]
    (first (filter #(= id (get % job-identifier-key)) jobs-statuses))))

(defn store-data
  [data id identifier-key]
  (let [database (:database (:database budi.components.budi-system/system))]
    (store-identifiable-data database identifier-key id data)))

(defn retry
  [tries f]
  (let [res (f)]
    (if (and (> tries 0) (not res))
      (do
        (Thread/sleep 100)
        (recur (dec tries) f))
      res)))

(defn get-job
  [id]
  (let [running-jobs (:status-refresh-schedule (:status-retreiver (:status-retreiver budi.components.budi-system/system)))]
    (first (filter #(= (:id %) id) @running-jobs))))

(defn get-job-state
  [identifier]
  (if (retry 20 #(get-job identifier))
    :running
    :not-running))
