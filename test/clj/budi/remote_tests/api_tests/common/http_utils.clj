(ns budi.remote-tests.api-tests.common.http-utils
  (:use plumbing.core clojure.test budi.core)
  (:import (java.util Base64))
  (:require
    [budi.remote-tests.common.test-configuration :refer [get-webserver-host
                                                         get-webserver-port]]
    [ring.swagger.schema :as ss]
    [slingshot.slingshot :refer [try+]]
    [clj-http.client :as client]
    [cheshire.core :as cheshire]))

(defn encode-base64
  [string]
  (->> string
      (.getBytes)
      (.encode (Base64/getEncoder))
      (new java.lang.String)))

(defn get-url
  ([port path]
   (format (str "http://"(get-webserver-host)":%s/%s") port path))
  ([resource-name port operation]
   (format (str "http://"(get-webserver-host)":%s/%s%s") port resource-name operation)))

(defn- call-and-coerce-body [call-fn schema]
  (try+
    (let [{:keys [body status] :as response} (call-fn)]
     (if (and (= status 200) (not (nil? body)))
       (as-> body b
         (if (string? b) (cheshire/parse-string b true) b)
         (ss/coerce schema b)
         (assoc-in response [:body] b))
       response))
    (catch Object ex
      (if (= clojure.lang.ExceptionInfo (type (:throwable &throw-context)))
        (.getData (:throwable &throw-context))
        (:throwable &throw-context)))))

(defn http-post [resource-path body]
  (client/post
   (get-url (get-webserver-port) resource-path)
   {:content-type :json
    :as :json
    :throw-exceptions false
    :body (cheshire/generate-string body)}))

(defn http-put [resource-path body]
  (client/put
    (get-url (get-webserver-port) resource-path)
    {:content-type :json
     :as :json
     :throw-exceptions false
     :body (cheshire/generate-string body)}))

(defn http-post-coerce [resource-path body schema]
  (call-and-coerce-body #(http-post resource-path body) schema))

(defn http-put-coerce [resource-path body schema]
    (call-and-coerce-body #(http-put resource-path body) schema))

(defn http-get [resource-name operation & [qps]]
  (client/get
   (get-url resource-name (get-webserver-port) operation)
   (assoc-when
    {:as :json
     :throw-exceptions false}
    :query-params qps)))

(defn http-get-coerce [path schema]
  (call-and-coerce-body #(client/get (get-url (get-webserver-port) path)) schema))

(defn http-delete [resource-name]
  (client/delete
   (get-url (get-webserver-port) resource-name)
   {:as :json
    :throw-exceptions false}))
