(ns budi.remote-tests.common.server-utils
  (:require [clojure.java.io :as io]
            [budi.remote-tests.common.test-configuration :refer [get-test-configuration]]
            [budi.components.budi-system :refer [start-system
                                                       stop-system]]))

(defn is-local-test? []
  (= :local (:execution (get-test-configuration))))

(defn start-server []
  (when (is-local-test?) (start-system (.getPath (io/resource "./budi_test_conf.edn")))))

(defn stop-server []
  (when (is-local-test?) (stop-system)))
