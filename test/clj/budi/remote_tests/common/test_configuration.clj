(ns budi.remote-tests.common.test-configuration
  (:require [budi.components.budi-system :as sys]
            [environ.core :refer [env]]))

(defn get-test-configuration []
  (if (env :conf)
    (env :conf)
    {:db {:type :nosql}
     :host "localhost"
     :execution :local
     :port 3083}))

(defn get-webserver-host []
  (:host (get-test-configuration)))

(defn get-webserver-port []
  (:port (get-test-configuration)))

(defn get-database-object []
  (if (= :local (:execution (get-test-configuration)))
    (do
      (println "Use local database storage")
      (:database (:database sys/system)))
    (let [db-hosts (:hosts (:db (get-test-configuration)))]
      (println "Use a remote database located in " db-hosts))))
