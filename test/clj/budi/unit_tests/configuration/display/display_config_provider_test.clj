(ns budi.unit-tests.configuration.display.display-config-provider-test
  (:use midje.sweet)
  (:require [budi.configuration.display.display-config-provider :as dcp]
            [budi.dao.generic-crud :refer [get-data]]))

(def test-job-data {:type :bamboo-build-job
                    :id "988065878906"
                    :name "my-job"
                    :build-by "My name"
                    :build-completed-at 70897822
                    :state :success
                    :lifeCycleState :running
                    :execution-time "23.11.2012-13:22"
                    :estimated-next-execution-time  "23.11.2012-33:22"})

(def list-of-test-data (->
                         (repeat 20 test-job-data)
                         (conj (->  test-job-data (assoc :build-completed-at 70897923) (assoc :state :failed))
                               (->  test-job-data (assoc :build-completed-at 71897923) (assoc :state :success))
                               (->  test-job-data (assoc :build-completed-at 71897933) (assoc :state :failed)))))

(fact-group "Test configuration provider"
            (fact :backend-test "Get display configuration even if there is nothing in database"
                  (dcp/get-display-config-data ..database..) => dcp/default-display-config
                  (provided
                    (get-data ..database.. :display-config) => nil))
            (fact :backend-test "Get display configuration containing previously saved data"
                  (dcp/get-display-config-data ..database..) => ..some-display-data..
                  (provided
                    (get-data ..database.. :display-config) => [..some-display-data..]))
  )

(fact-group "Test configuration provider "
            (fact :backend-test "merging with build-tiles"
                  (let [build-jobs list-of-test-data
                        display-config (:newest-build-status dcp/default-display-config)]
                    (dcp/merge-display-config-with-build-config ..database.. build-jobs) => (contains [(contains {:display-config display-config})])
                    (provided
                      (dcp/get-display-config-data ..database..) => dcp/default-display-config
                      (dcp/categorize-build-data build-jobs) => [(assoc-in test-job-data [:display-config :size] :full)]
                      )))
  )

(fact-group "Test build-tiles processor"
            (fact :backend-test "categorizes the build result list and sets tiles size"
                  (dcp/categorize-build-data list-of-test-data) => (contains [(contains {:display-config {:size :full}})])
                  (dcp/categorize-build-data list-of-test-data) => (contains [(contains {:display-config {:size :half}})])
                  (dcp/categorize-build-data list-of-test-data) => (contains [(contains {:display-config {:size :third}})])
                  )
  )
