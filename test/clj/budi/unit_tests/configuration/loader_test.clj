(ns budi.unit-tests.configuration.loader-test
  (:use midje.sweet)
  (:require [budi.configuration.loader :as loader]))

#_(fact-group "Test configuration loader"
  (fact :backend-test "Load an internal configuration using a classpath resource"
    (let [internal-resource (clojure.java.io/resource "internal-backend-config.edn")]
      (loader/load-config internal-resource) => #(contains? % :db)))

  (fact :backend-test "Load an external configuration using an absolute path"
    (let [temporary-external-config (java.io.File/createTempFile "temp-budi-conf" ".edn")]
      (.deleteOnExit temporary-external-config)
      (clojure.java.io/copy "{:db {:type :mongodb}}" temporary-external-config)
      (loader/load-config (.getAbsolutePath temporary-external-config)) => #(= (:type (:db %)) :mongodb)))

  (fact :backend-test "Load internal configuration and overwrite those values with external"
    (let [internal-resource (clojure.java.io/resource "internal-backend-config.edn")
          temporary-external-config (java.io.File/createTempFile "temp-budi-conf" ".edn")]
      (.deleteOnExit temporary-external-config)
      (clojure.java.io/copy "{:db {:type :mongodb}}" temporary-external-config)
      (loader/load-config internal-resource (.getAbsolutePath temporary-external-config)) => (contains {:db irrelevant, :port 3080})))

  (fact :backend-test "Don't throw any error when external resource does not exist"
    (let [internal-resource (clojure.java.io/resource "internal-backend-config.edn")]
      (loader/load-config internal-resource "/some/path.edn") => (contains {:db irrelevant, :port 3080}))))
