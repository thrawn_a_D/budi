(ns budi.unit-tests.status.gathering.call-timer-test
  (:use midje.sweet)
  (:require [clojure.core.async :as async :refer :all]
            [budi.status.gathering.call-timer :refer [continuously-execute]]))

(defn wait [] (Thread/sleep 10))

(fact :backend-test "Execute a function every given period"
    (let [verification-atom (atom {:was-run false})
          time-period 0.0000001
          result-channel (chan)
          my-fn (fn [] (reset! verification-atom {:was-run true}))
          stop-fn (continuously-execute my-fn time-period result-channel)]
       (wait)                                                   ; Give some time to execute
       @verification-atom => (contains {:was-run true})
       (stop-fn)))

(fact :backend-test "Stop execution of a function"
      (let [verification-atom (atom 0)
            time-period 0.00000001
            result-channel (chan)
            my-fn (fn [] (swap! verification-atom inc))
            stop-fn (continuously-execute my-fn time-period result-channel)]
        (wait)                                                  ; Give some time to execute
        (stop-fn)                                               ; We stop execution
        (wait)                                                  ; Wait again some time
        (let [result-1 @verification-atom]                      ; We get the result
          result-1 => #(> % 0)                                  ; Function was executed
          (wait)                                                ; Wait again some time
          @verification-atom => result-1)))                     ; Function was not executed in the meantime (it's stopped)
