# budi - Wall display tool

Budi is a monitoring tool that allows to keep track of several, mandatory services or jobs. The purpose of a monitoring tool, is to provide fast feedback in order to increase reaction time, needed to fix a related problem. Budi is a web application based on the JVM.

Budi currently supports monitoring of:

* Bamboo build job status
* Bamboo deployment job status
* HTTP-Based service status
* Sonar projects

![Budi display wall]
(https://bitbucket.org/thrawn_a_D/budi/downloads/budi-screen.png)

## Usage

### Get a copy of budi
There are two valid ways to get a copy of budi. The easiest way is to get a pre compiled jar file. If you want the newest version of budi, you might have to compile it yourself.

#### Download a pre compiled jar file
Get a copy of a pre compiled jar file by following the link: https://bitbucket.org/thrawn_a_D/budi/downloads/budi-main.zip

#### Build budi
* Install leiningen by following instructions on http://leiningen.org
* Download a copy of budi: git clone https://thrawn_a_D@bitbucket.org/thrawn_a_D/budi.git
* Run: lein uberjar
* Run the budi.sh script file

### Configure budi
Budi is delivered with a minimalistic configuration file ```budi-conf.edn```, which also defines the service port and polling time. Usually you won't need to touch it.

The configuration of jobs and credentials (wallets) is done using the budi user interface (typically under http://localhost:3081/api/pages/config/wallet and http://localhost:3081/api/pages/config/build).

### Start budi
1. You have to install JAVA in order to use budi.
2. Decompress the downloaded budi-main.zip into a budi-main/ folder.
3. Go to budi folder ```cd budi-main/```
4. Run ./budi.sh
5. Configure your credentials under: http://localhost:3081/api/pages/config/wallet
6. Configure your jobs under: http://localhost:3081/api/pages/config/build
7. Now you should be able to see the results on http://localhost:3081/api/pages/budi

It may take a few seconds to gather the status information.

## Development
### HTTP-API documentation
Based on prismatic schema and compojure-api, budi provides an interface documentation based on swagger. Simply open up the "/index.html":
![ Swagger api documentation ]
(https://bitbucket.org/thrawn_a_D/budi/downloads/budi-swagger.png)

## License

Copyright � 2015

Distributed under the GNU General Public License (GPL).

Credits to:
http://clojure.org
https://github.com/clojure/clojurescript
http://isotope.metafizzy.co